﻿
using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Text;
using Common.DBUtility;
using Joz.Model;
namespace Joz.DAL
{
    /// <summary>
    /// 类DDStars_Dal
    /// 此文件由CoderGoCodeBuilder v1.0自动生成，请不要手动修改此文件，以便日后自动覆盖
    /// 2018-12-26 16:15:22
    /// </summary>
    [Serializable]
    public partial class DDStars_Dal : SQL_DAL_Base
    {
    private const string SQL_INSERT_DDSTARS = @" 
  (Name)
    VALUES (@Name)";
      private const string PARM_ID = "@Id";
      private const string PARM_NAME = "@Name";
    public static readonly DDStars_Dal Instance;
    static DDStars_Dal()
    {
        Instance = new DDStars_Dal();
        Instance.InstanceInited();
    }
    private DDStars_Dal()
    {
        tableOrViewName = "DDStars";
        primeryKeyNames = "id"; 
    }
    /// <summary>
    /// 将model实体以一条数据记录插入数据表
    /// 此方法为自动生成，请不要手动修改
    /// </summary>
    /// <param name="model">要以记录插入数据库的对象</param>
    /// <returns>返回自增的Id的新增值</returns>
    public int Add(DDStars_Model model)
    {
        return Add(model,null);
    }
    /// <summary>
    /// 将model实体以一条数据记录插入数据表, 使用事务
    /// 此方法为自动生成，请不要手动修改
    /// </summary>
    /// <param name="model">要以记录插入数据库的对象</param>
    /// <param name="model">使用的事务对象,可为null </param>
    /// <returns>Id</returns>
    public int Add(DDStars_Model model, SqlTransaction trans)
    {
    SqlParameter[] parameters = GetDDStarsParameters();
      if(model.Id == null) parameters[0].Value=0; //有可能出现与业务逻辑有关的问题
      else parameters[0].Value = model.Id;
      if(model.Name == null) parameters[1].Value=""; //有可能出现与业务逻辑有关的问题
      else parameters[1].Value = model.Name;
        if(trans==null)
        {
            return int.Parse(SqlHelperWebDAL.ExecuteScalar(CommandType.Text,"INSERT INTO " + tableOrViewName + " " + SQL_INSERT_DDSTARS+";select @@IDENTITY", parameters).ToString());
        }
        else
        {
            return int.Parse(SqlHelperWebDAL.ExecuteScalar(trans, CommandType.Text,"INSERT INTO " + tableOrViewName + " " + SQL_INSERT_DDSTARS+";select @@IDENTITY", parameters).ToString());
        }
    }
    /// <summary>
    /// 根据model更新该实体对应的数据表记录的所有字段，返回受影响的行数
    /// 此方法为自动生成，请不要手动修改
    /// </summary>
    /// <param name="model">要更新的实体对象</param>
    /// <returns>受影响的行数</returns>
    public int Update(DDStars_Model model)
    {
         return Update("Name=@Name", model);
    }
    /// <summary>
    /// 使用事务，根据model更新该实体对应的数据表记录的所有字段，返回受影响的行数
    /// 此方法为自动生成，请不要手动修改
    /// </summary>
    /// <param name="trans">使用的事务,可以为null</param>
    /// <param name="model">要更新的记录对象</param>
    /// <returns>受影响的行数</returns>
    public int Update(SqlTransaction trans, DDStars_Model model)
    {
         return Update(trans,"Name=@Name", model);
    }
    /// <summary>
    /// 根据model更新该实体对应的数据表记录，可指定部分行，返回受影响的行数
    /// 此方法为自动生成，请不要手动修改
    /// </summary>
    /// <param name="UpdateSql">指定更新SQL(不含set)</param>
    /// <param name="model">要更新的记录对象</param>
    /// <returns>受影响的行数</returns>
    public int Update(string UpdateSql, DDStars_Model model)
    {
        return Update(null,UpdateSql,model);
    }
    /// <summary>
    /// 使用事务，根据model更新该实体对应的数据表记录，可指定部分行，返回受影响的行数
    /// 此方法为自动生成，请不要手动修改
    /// </summary>
    /// <param name="trans">使用的事务,可以为null</param>
    /// <param name="UpdateSql">指定更新SQL(不含set)</param>
    /// <param name="model">要更新的记录对象</param>
    /// <returns>受影响的行数</returns>
    public int Update(SqlTransaction trans, string UpdateSql, DDStars_Model model)
    {
    if(UpdateSql.Trim()=="") throw new Exception("请指定更新细节");
    SqlParameter[] parameters = GetDDStarsParameters();
      if(model.Id == null) parameters[0].Value=0; //有可能出现与业务逻辑有关的问题
      else parameters[0].Value = model.Id;
      if(model.Name == null) parameters[1].Value=""; //有可能出现与业务逻辑有关的问题
      else parameters[1].Value = model.Name;
    if(trans==null) return SqlHelperWebDAL.ExecuteNonQuery(CommandType.Text, "update " + tableOrViewName + " set " + UpdateSql + " where Id=@Id", parameters);
    else return SqlHelperWebDAL.ExecuteNonQuery(trans, CommandType.Text, "update " + tableOrViewName + " set " + UpdateSql + " where Id=@Id", parameters);
    }
    /// <summary>
    /// 根据主键删除该实体对应的数据表记录，返回受影响的行数
    /// 此方法为自动生成，请不要手动修改
    /// </summary>
    /// <returns>受影响的行数</returns>
    public int Delete(int id)
    {
         return Delete(null,id);
    }
    /// <summary>
    /// 使用事务，根据主键删除该实体对应的数据表记录，返回受影响的行数
    /// 此方法为自动生成，请不要手动修改
    /// </summary>
    /// <param name="trans">使用的事务,可以为null</param>
    /// <returns>受影响的行数</returns>
    public int Delete(SqlTransaction trans, int id)
    {
    if(trans==null) return SqlHelperWebDAL.ExecuteNonQuery(CommandType.Text,"delete " + tableOrViewName + " where Id=@Id",new SqlParameter("@Id",id));
	else return SqlHelperWebDAL.ExecuteNonQuery(trans,CommandType.Text,"delete " + tableOrViewName + " where Id=@Id",new SqlParameter("@Id",id));
    }
    /// <summary>
    /// 根据主键返回实体对象
    /// </summary>
    /// <returns></returns>
    public DDStars_Model GetModelByID(int id)
    {
    return GetModel("Id=@Id", new SqlParameter("@Id",id)
    );
    }
    /// <summary>
    /// 根据主键返回实体对象
    /// </summary>
    /// <returns></returns>
    public DDStars_Model GetModelByID(int id,SqlTransaction trans)
    {
        return GetModel("Id=@Id", trans, new SqlParameter("@Id",id)
              );
    }
    /// <summary>
    /// 根据主键返回实体对象
    /// </summary>
    /// <returns></returns>
    public DDStars_Model GetModelByID(int id,string selectFeilds,SqlTransaction trans)
    {
        return GetModel("Id=@Id",selectFeilds ,trans, new SqlParameter("@Id",id)
        );
    }
    /// <summary>
    /// 将记录转换成实体对象
    /// </summary>
    /// <param name="dr"></param>
    /// <returns></returns>
    public DDStars_Model GetModel(SqlDataReader dr)
    {
        DDStars_Model model = new DDStars_Model();
        for (int i = 0; i < dr.FieldCount; i++)
    {
          switch(dr.GetName(i).ToLower())
          {
                case "id":
                    model.Id=SqlHelper.GetInt(dr["Id"]);
                    break;
                case "name":
                    model.Name=SqlHelper.GetString(dr["Name"]);
                    break;
                default:break;
           }
       }
       return model;
    }
    /// <summary>
    /// 将记录转换成实体对象
    /// </summary>
    /// <param name="dr"></param>
    /// <returns></returns>
    public DDStars_Model GetModel(DataRow dr)
    {
          DDStars_Model model = new DDStars_Model();
        for (int i = 0; i < dr.Table.Columns.Count; i++)
        {
          switch(dr.Table.Columns[i].ColumnName.ToLower())
          {
            case "id":
            model.Id=SqlHelper.GetInt(dr["Id"]);
            break;
            case "name":
            model.Name=SqlHelper.GetString(dr["Name"]);
            break;
          default:break;
          }
        }
        return model;
  }
    /// <summary>
    /// 返回符合条件的第一个对象
    /// </summary>
    /// <param name="conditionSql">条件SQL(不含where)</param>
    /// <param name="parms">SQL参数</param>
    /// <returns>返回符合条件的第一个对象</returns>
    public DDStars_Model GetModel(string conditionSql, params SqlParameter[] parms)
    {
        return GetModel(conditionSql,"*", parms);
    }
    /// <summary>
    /// 返回符合条件的第一个对象
    /// </summary>
    /// <param name="conditionSql">条件SQL(不含where)</param>
    /// <param name="selectFields">返回属性（列），为空或null则返回所有属性（列）</param>
    /// <param name="parms">SQL参数</param>
    /// <returns>返回符合条件的第一个对象</returns>
    public DDStars_Model GetModel(string conditionSql, string selectFields, params SqlParameter[] parms)
    {
        DDStars_Model model = null;
    using (SqlDataReader dr = SqlHelperWebDAL.ExecuteReader(CommandType.Text, "select top 1 "+(string.IsNullOrEmpty(selectFields)?"*":selectFields)+" from " + tableOrViewName + " " + ((conditionSql.Trim() == "") ? "" : ("where " + conditionSql)), parms))
    {
    if (dr.Read()) model = GetModel(dr);
    }
    return model;
    }
    /// <summary>
    /// 返回符合条件的第一个对象
    /// </summary>
    /// <param name="conditionSql">条件SQL(不含where)</param>
    /// <param name="trans">事务(可为null)</param>
    /// <param name="parms">SQL参数</param>
    /// <returns>返回符合条件的第一个对象</returns>
    public DDStars_Model GetModel(string conditionSql, SqlTransaction trans, params SqlParameter[] parms)
    {
        return GetModel(conditionSql,"*",trans,parms);
    }
    /// <summary>
    /// 返回符合条件的第一个对象
    /// </summary>
    /// <param name="conditionSql">条件SQL(不含where)</param>
    /// <param name="selectFields">返回属性（列），为空或null则返回所有属性（列）</param>
    /// <param name="trans">事务(可为null)</param>
    /// <param name="parms">SQL参数</param>
    /// <returns>返回符合条件的第一个对象</returns>
    public DDStars_Model GetModel(string conditionSql, string selectFields, SqlTransaction trans, params SqlParameter[] parms)
    {
        if (trans == null) return GetModel(conditionSql,selectFields, parms);
        DDStars_Model model = null;
    DataTable dt = SqlHelperWebDAL.GetDataSet(trans, CommandType.Text, "select top 1 "+(string.IsNullOrEmpty(selectFields)?"*":selectFields)+" from " + tableOrViewName + " " + ((conditionSql.Trim() == "") ? "" : ("where " + conditionSql)), parms).Tables[0];
    if (dt.Rows.Count > 0) model = GetModel(dt.Rows[0]);
    return model;
    }
    /// <summary>
    ///
    /// </summary>
    /// <param name="sql">完整的SQL</param>
    /// <param name="parms"></param>
    /// <returns></returns>
    public IList<DDStars_Model> GetModelList(string sql, params SqlParameter[] parms)
    {
        IList<DDStars_Model>
        modelList = new List<DDStars_Model>();
        using (SqlDataReader dr = SqlHelperWebDAL.ExecuteReader(CommandType.Text, sql, parms))
        {
        while (dr.Read()) modelList.Add(GetModel(dr));
        }
        return modelList;
    }
    /// <summary>
    ///
    /// </summary>
    /// <param name="conditionSql">指定条件(不含Where)</param>
    /// <param name="selectFields">要返回的字段，为空则返回所有字段</param>
    /// <param name="orderBy">排序方式(不包含order by)</param>
    /// <param name="parms">执行SQL语句使用的参数</param>
    /// <returns></returns>
    public IList<DDStars_Model> GetModelList(string conditionSql, string selectFields, string orderBy, params SqlParameter[] parms)
    {
        return GetModelList(conditionSql, selectFields, orderBy, null, parms);
    }
    /// <summary>
    ///
    /// </summary>
    /// <param name="conditionSql">指定条件(不含Where)</param>
    /// <param name="selectFields">要返回的字段，为空则返回所有字段</param>
    /// <param name="orderBy">排序方式(不包含order by)</param>
    /// <param name="trans">事务(可为null)</param>
    /// <param name="parms">执行SQL语句使用的参数</param>
    /// <returns></returns>
    public IList<DDStars_Model> GetModelList(string conditionSql, string selectFields, string orderBy, SqlTransaction trans, params SqlParameter[] parms)
    {
        IList<DDStars_Model>
        modelList = new List<DDStars_Model>();
        foreach(DataRow dr in GetList(conditionSql,selectFields,orderBy, trans,parms).Rows)
        {
            modelList.Add(GetModel(dr));
        }
        return modelList;
    }
    /// <summary>
    /// 分页返回符合条件的记录，可指定返回字段
    /// MSSQL2005及以上可用
    /// </summary>
    /// <param name="totalCount">返回所有记录数,如果输入大于0,则不重新计算</param>
    /// <param name="conditionSql">指定条件(不含Where)</param>
    /// <param name="selectFields">要返回的字段，为空则返回所有字段</param>
    /// <param name="orderBy">排序方式(不包含order by)</param>
    /// <param name="pageSize">分页大小--每页包含的记录数</param>
    /// <param name="pageIndex">页码--要获取第几页的记录</param>
    /// <param name="parms">执行SQL语句使用的参数</param>
    /// <returns>按分页大小返回指定页码的记录的指定字段</returns>
    public IList<DDStars_Model> GetModelList(ref int totalCount, string conditionSql, string selectFields, string orderBy, int pageSize, int pageIndex, params SqlParameter[] parms)
    {
         return GetModelListMSSQL2005(ref totalCount, conditionSql, selectFields, orderBy, pageSize, pageIndex, parms);
    }
    /// <summary>
    /// 分页返回符合条件的记录
    /// MSSQL2005及以上可用
    /// </summary>
    /// <param name="conditionSql">指定条件(不含Where)</param>
    /// <param name="selectFields">要返回的字段，为空则返回所有字段</param>
    /// <param name="orderBy">排序方式(不包含order by)</param>
    /// <param name="pageSize">分页大小--每页包含的记录数</param>
    /// <param name="pageIndex">页码--要获取第几页的记录</param>
    /// <param name="parms">执行SQL语句使用的参数</param>
    /// <returns>按分页大小返回指定页码的记录的指定字段</returns>
    public IList<DDStars_Model> GetModelList(string conditionSql, string orderBy, int pageSize, int pageIndex, params SqlParameter[] parms)
    {
        return GetModelListMSSQL2005(conditionSql, orderBy, pageSize, pageIndex, parms);
    }
    /// <summary>
    /// 分页返回符合条件的记录，可指定返回字段
    /// MSSQL2005及以上可用
    /// </summary>
    /// <param name="totalCount">返回所有记录数,如果输入大于0,则不重新计算</param>
    /// <param name="conditionSql">指定条件(不含Where)</param>
    /// <param name="selectFields">要返回的字段，为空则返回所有字段</param>
    /// <param name="orderBy">排序方式(不包含order by)</param>
    /// <param name="pageSize">分页大小--每页包含的记录数</param>
    /// <param name="pageIndex">页码--要获取第几页的记录</param>
    /// <param name="pageRecordOffset">分页时的偏移值（前面pageRecordOffset条使用其他方式或其他条件取出，小于0则表示最前面pageRecordOffset条记录被忽略）</param>
    /// <param name="parms">执行SQL语句使用的参数</param>
    /// <returns>按分页大小返回指定页码的记录的指定字段</returns>
    public IList<DDStars_Model> GetModelList(ref int totalCount, string conditionSql, string selectFields, string orderBy, int pageSize, int pageIndex, int pageRecordOffset, params SqlParameter[] parms)
    {
        return GetModelListMSSQL2005(ref totalCount, conditionSql, selectFields, orderBy, pageSize, pageIndex, pageRecordOffset, parms);
    }
    /// <summary>
    /// 分页返回符合条件的记录
    /// MSSQL2005及以上可用
    /// </summary>
    /// <param name="conditionSql">指定条件(不含Where)</param>
    /// <param name="selectFields">要返回的字段，为空则返回所有字段</param>
    /// <param name="orderBy">排序方式(不包含order by)</param>
    /// <param name="pageSize">分页大小--每页包含的记录数</param>
    /// <param name="pageIndex">页码--要获取第几页的记录</param>
    /// <param name="pageRecordOffset">分页时的偏移值（前面pageRecordOffset条使用其他方式或其他条件取出，小于0则表示最前面pageRecordOffset条记录被忽略）</param>
    /// <param name="parms">执行SQL语句使用的参数</param>
    /// <returns>按分页大小返回指定页码的记录的指定字段</returns>
    public IList<DDStars_Model> GetModelList(string conditionSql, string orderBy, int pageSize, int pageIndex, int pageRecordOffset, params SqlParameter[] parms)
    {
        return GetModelListMSSQL2005(conditionSql, orderBy, pageSize, pageIndex, pageRecordOffset, parms);
    }
    /// <summary>
    /// 分页返回符合条件的记录，可指定返回字段
    /// MSSQL2005及以上可用
    /// </summary>
    /// <param name="joinSql">join语句，如inner Join B on DDStars.id=B.id</param>
    /// <param name="throwSql">将SQL语句作为异常抛出，用于检测语句正确性</param>
    /// <param name="totalCount">返回所有记录数,如果输入大于0,则不重新计算</param>
    /// <param name="conditionSql">指定条件(不含Where)</param>
    /// <param name="selectFields">要返回的字段，为空则返回所有字段</param>
    /// <param name="orderBy">排序方式(不包含order by)</param>
    /// <param name="pageSize">分页大小--每页包含的记录数</param>
    /// <param name="pageIndex">页码--要获取第几页的记录</param>
    /// <param name="parms">执行SQL语句使用的参数</param>
    /// <returns>按分页大小返回指定页码的记录的指定字段</returns>
    public IList<DDStars_Model> GetModelList_JOIN(string joinSql,bool throwSql,ref int totalCount, string conditionSql, string selectFields, string orderBy, int pageSize, int pageIndex, params SqlParameter[] parms)
    {
        return GetModelList_JOIN(joinSql,throwSql,ref totalCount, conditionSql, selectFields, orderBy, pageSize, pageIndex, 0, parms);
    }
    /// <summary>
    /// 分页返回符合条件的记录，可指定返回字段
    /// MSSQL2005及以上可用
    /// </summary>
    /// <param name="joinSql">join语句，如inner Join B on DDStars.id=B.id</param>
    /// <param name="throwSql">将SQL语句作为异常抛出，用于检测语句正确性</param>
    /// <param name="totalCount">返回所有记录数,如果输入大于0,则不重新计算</param>
    /// <param name="conditionSql">指定条件(不含Where)</param>
    /// <param name="selectFields">要返回的字段，为空则返回所有字段</param>
    /// <param name="orderBy">排序方式(不包含order by)</param>
    /// <param name="pageSize">分页大小--每页包含的记录数</param>
    /// <param name="pageIndex">页码--要获取第几页的记录</param>
    /// <param name="pageRecordOffset">分页时的偏移值（前面pageRecordOffset条使用其他方式或其他条件取出）</param>
    /// <param name="parms">执行SQL语句使用的参数</param>
    /// <returns>按分页大小返回指定页码的记录的指定字段</returns>
    public IList<DDStars_Model> GetModelList_JOIN(string joinSql,bool throwSql,ref int totalCount, string conditionSql, string selectFields, string orderBy, int pageSize, int pageIndex,int pageRecordOffset, params SqlParameter[] parms)
    {
        return GetModelListMSSQL2005_JOIN(joinSql,throwSql,ref totalCount, conditionSql, selectFields, orderBy, pageSize, pageIndex,pageRecordOffset, parms);
    }
    /// <summary>
    /// 分页返回符合条件的记录，可指定返回字段
    /// 注意：仅MSSQL2005及以上可用
    /// </summary>
    /// <param name="totalCount">返回所有记录数,如果输入大于0,则不重新计算</param>
    /// <param name="conditionSql">指定条件(不含Where)</param>
    /// <param name="selectFields">要返回的字段，为空则返回所有字段</param>
    /// <param name="orderBy">排序方式(不包含order by)</param>
    /// <param name="pageSize">分页大小--每页包含的记录数</param>
    /// <param name="pageIndex">页码--要获取第几页的记录</param>
    /// <param name="parms">执行SQL语句使用的参数</param>
    /// <returns>按分页大小返回指定页码的记录的指定字段</returns>
    public IList<DDStars_Model> GetModelListMSSQL2005(ref int totalCount, string conditionSql, string selectFields, string orderBy, int pageSize, int pageIndex, params SqlParameter[] parms)
    {
        return GetModelListMSSQL2005(ref totalCount, conditionSql, selectFields, orderBy, pageSize, pageIndex, 0, parms);
    }
    /// <summary>
    /// 分页返回符合条件的记录，可指定返回字段
    /// 注意：仅MSSQL2005及以上可用
    /// </summary>
    /// <param name="totalCount">返回所有记录数,如果输入大于0,则不重新计算</param>
    /// <param name="conditionSql">指定条件(不含Where)</param>
    /// <param name="selectFields">要返回的字段，为空则返回所有字段</param>
    /// <param name="orderBy">排序方式(不包含order by)</param>
    /// <param name="pageSize">分页大小--每页包含的记录数</param>
    /// <param name="pageIndex">页码--要获取第几页的记录</param>
    /// <param name="pageRecordOffset">分页时的偏移值（前面pageRecordOffset条使用其他方式或其他条件取出）</param>
    /// <param name="parms">执行SQL语句使用的参数</param>
    /// <returns>按分页大小返回指定页码的记录的指定字段</returns>
    public IList<DDStars_Model> GetModelListMSSQL2005(ref int totalCount, string conditionSql, string selectFields, string orderBy, int pageSize, int pageIndex, int pageRecordOffset, params SqlParameter[] parms)
    {
        DataTable dt = GetListMSSQL2005(ref totalCount, conditionSql, selectFields, orderBy, pageSize, pageIndex, pageRecordOffset, parms);
        IList<DDStars_Model>
        modelList = new List<DDStars_Model>();
        foreach(DataRow dr in dt.Rows) modelList.Add(GetModel(dr));
        return modelList;
    }
    /// <summary>
    /// 分页返回符合条件的记录
    /// 注意：仅MSSQL2005及以上可用
    /// </summary>
    /// <param name="conditionSql">指定条件(不含Where)</param>
    /// <param name="selectFields">要返回的字段，为空则返回所有字段</param>
    /// <param name="orderBy">排序方式(不包含order by)</param>
    /// <param name="pageSize">分页大小--每页包含的记录数</param>
    /// <param name="pageIndex">页码--要获取第几页的记录</param>
    /// <param name="parms">执行SQL语句使用的参数</param>
    /// <returns>按分页大小返回指定页码的记录的指定字段</returns>
    public IList<DDStars_Model> GetModelListMSSQL2005(string conditionSql, string orderBy, int pageSize, int pageIndex, params SqlParameter[] parms)
    {
        return GetModelListMSSQL2005(conditionSql, orderBy, pageSize, pageIndex, 0, parms);
    }
    /// <summary>
    /// 分页返回符合条件的记录
    /// 注意：仅MSSQL2005及以上可用
    /// </summary>
    /// <param name="conditionSql">指定条件(不含Where)</param>
    /// <param name="selectFields">要返回的字段，为空则返回所有字段</param>
    /// <param name="orderBy">排序方式(不包含order by)</param>
    /// <param name="pageSize">分页大小--每页包含的记录数</param>
    /// <param name="pageIndex">页码--要获取第几页的记录</param>
    /// <param name="pageRecordOffset">分页时的偏移值（前面pageRecordOffset条使用其他方式或其他条件取出）</param>
    /// <param name="parms">执行SQL语句使用的参数</param>
    /// <returns>按分页大小返回指定页码的记录的指定字段</returns>
    public IList<DDStars_Model> GetModelListMSSQL2005(string conditionSql, string orderBy, int pageSize, int pageIndex, int pageRecordOffset, params SqlParameter[] parms)
    {
        string sql = SqlHelper.GetPageSQL_MSSQL2005(TableOrViewName, PrimeryKeyNames, conditionSql, "*", orderBy, pageSize, pageIndex, pageRecordOffset);
        return GetModelList(sql, parms);
    }
    /// <summary>
    /// 分页返回符合条件的记录，可指定返回字段
    /// 注意：仅MSSQL2005及以上可用
    /// </summary>
    /// <param name="joinSql">join语句，如inner Join B on DDStars.id=B.id</param>
    /// <param name="throwSql">将SQL语句作为异常抛出，用于检测语句正确性</param>
    /// <param name="totalCount">返回所有记录数,如果输入大于0,则不重新计算</param>
    /// <param name="conditionSql">指定条件(不含Where)</param>
    /// <param name="selectFields">要返回的字段，为空则返回所有字段</param>
    /// <param name="orderBy">排序方式(不包含order by)</param>
    /// <param name="pageSize">分页大小--每页包含的记录数</param>
    /// <param name="pageIndex">页码--要获取第几页的记录</param>
    /// <param name="parms">执行SQL语句使用的参数</param>
    /// <returns>按分页大小返回指定页码的记录的指定字段</returns>
    public IList<DDStars_Model> GetModelListMSSQL2005_JOIN(string joinSql,bool throwSql,ref int totalCount, string conditionSql, string selectFields, string orderBy, int pageSize, int pageIndex, params SqlParameter[] parms)
    {
        return GetModelListMSSQL2005_JOIN(joinSql,throwSql,ref totalCount, conditionSql, selectFields, orderBy, pageSize, pageIndex, 0, parms);
    }
    /// <summary>
    /// 分页返回符合条件的记录，可指定返回字段
    /// 注意：仅MSSQL2005及以上可用
    /// </summary>
    /// <param name="joinSql">join语句，如inner Join B on DDStars.id=B.id</param>
    /// <param name="throwSql">将SQL语句作为异常抛出，用于检测语句正确性</param>
    /// <param name="totalCount">返回所有记录数,如果输入大于0,则不重新计算</param>
    /// <param name="conditionSql">指定条件(不含Where)</param>
    /// <param name="selectFields">要返回的字段，为空则返回所有字段</param>
    /// <param name="orderBy">排序方式(不包含order by)</param>
    /// <param name="pageSize">分页大小--每页包含的记录数</param>
    /// <param name="pageIndex">页码--要获取第几页的记录</param>
    /// <param name="pageRecordOffset">分页时的偏移值（前面pageRecordOffset条使用其他方式或其他条件取出）</param>
    /// <param name="parms">执行SQL语句使用的参数</param>
    /// <returns>按分页大小返回指定页码的记录的指定字段</returns>
    public IList<DDStars_Model> GetModelListMSSQL2005_JOIN(string joinSql,bool throwSql,ref int totalCount, string conditionSql, string selectFields, string orderBy, int pageSize, int pageIndex, int pageRecordOffset, params SqlParameter[] parms)
    {
        DataTable dt = GetListMSSQL2005_JOIN(joinSql,throwSql,ref totalCount, conditionSql, selectFields, orderBy, pageSize, pageIndex, pageRecordOffset, parms);
        IList<DDStars_Model>
        modelList = new List<DDStars_Model>();
        foreach(DataRow dr in dt.Rows) modelList.Add(GetModel(dr));
        return modelList;
    }
    /// <summary>
    /// 分页返回符合条件的记录，可指定返回字段
    /// MSSQL2000及以上可用
    /// </summary>
    /// <param name="totalCount">返回所有记录数,如果输入大于0,则不重新计算</param>
    /// <param name="conditionSql">指定条件(不含Where)</param>
    /// <param name="selectFields">要返回的字段，为空则返回所有字段</param>
    /// <param name="orderBy">排序方式(不包含order by)</param>
    /// <param name="pageSize">分页大小--每页包含的记录数</param>
    /// <param name="pageIndex">页码--要获取第几页的记录</param>
    /// <param name="parms">执行SQL语句使用的参数</param>
    /// <returns>按分页大小返回指定页码的记录的指定字段</returns>
    public IList<DDStars_Model> GetModelListMSSQL2000(ref int totalCount, string conditionSql, string selectFields, string orderBy, int pageSize, int pageIndex, params SqlParameter[] parms)
    {
        DataTable dt = GetList(ref totalCount, conditionSql, selectFields, orderBy, pageSize, pageIndex, parms);
        IList<DDStars_Model>
        modelList = new List<DDStars_Model>();
        foreach(DataRow dr in dt.Rows) modelList.Add(GetModel(dr));
        return modelList;
    }
    /// <summary>
    /// 分页返回符合条件的记录
    /// MSSQL2000及以上可用
    /// </summary>
    /// <param name="conditionSql">指定条件(不含Where)</param>
    /// <param name="selectFields">要返回的字段，为空则返回所有字段</param>
    /// <param name="orderBy">排序方式(不包含order by)</param>
    /// <param name="pageSize">分页大小--每页包含的记录数</param>
    /// <param name="pageIndex">页码--要获取第几页的记录</param>
    /// <param name="parms">执行SQL语句使用的参数</param>
    /// <returns>按分页大小返回指定页码的记录的指定字段</returns>
    public IList<DDStars_Model> GetModelListMSSQL2000(string conditionSql, string orderBy, int pageSize, int pageIndex, params SqlParameter[] parms)
    {
      string sql = SqlHelper.GetPageSQL(TableOrViewName, PrimeryKeyNames, conditionSql, "*", orderBy, pageSize, pageIndex);
      return GetModelList(sql, parms);
    }
    /// <summary>
    /// 分页返回符合条件的记录，可指定返回字段
    /// MSSQL2000及以上可用
    /// </summary>
    /// <param name="totalCount">返回所有记录数,如果输入大于0,则不重新计算</param>
    /// <param name="conditionSql">指定条件(不含Where)</param>
    /// <param name="selectFields">要返回的字段，为空则返回所有字段</param>
    /// <param name="orderBy">排序方式(不包含order by)</param>
    /// <param name="pageSize">分页大小--每页包含的记录数</param>
    /// <param name="pageIndex">页码--要获取第几页的记录</param>
    /// <param name="pageRecordOffset">分页时的偏移值（前面pageRecordOffset条使用其他方式或其他条件取出，小于0则表示最前面pageRecordOffset条记录被忽略）</param>
    /// <param name="parms">执行SQL语句使用的参数</param>
    /// <returns>按分页大小返回指定页码的记录的指定字段</returns>
    public IList<DDStars_Model> GetModelListMSSQL2000(ref int totalCount, string conditionSql, string selectFields, string orderBy, int pageSize, int pageIndex, int pageRecordOffset, params SqlParameter[] parms)
    {
        DataTable dt = GetList(ref totalCount, conditionSql, selectFields, orderBy, pageSize, pageIndex, pageRecordOffset, parms);
        IList<DDStars_Model>
        modelList = new List<DDStars_Model>();
        foreach(DataRow dr in dt.Rows) modelList.Add(GetModel(dr));
        return modelList;
    }
    /// <summary>
    /// 分页返回符合条件的记录
    /// MSSQL2000及以上可用
    /// </summary>
    /// <param name="conditionSql">指定条件(不含Where)</param>
    /// <param name="selectFields">要返回的字段，为空则返回所有字段</param>
    /// <param name="orderBy">排序方式(不包含order by)</param>
    /// <param name="pageSize">分页大小--每页包含的记录数</param>
    /// <param name="pageIndex">页码--要获取第几页的记录</param>
    /// <param name="pageRecordOffset">分页时的偏移值（前面pageRecordOffset条使用其他方式或其他条件取出，小于0则表示最前面pageRecordOffset条记录被忽略）</param>
    /// <param name="parms">执行SQL语句使用的参数</param>
    /// <returns>按分页大小返回指定页码的记录的指定字段</returns>
    public IList<DDStars_Model> GetModelListMSSQL2000(string conditionSql, string orderBy, int pageSize, int pageIndex, int pageRecordOffset, params SqlParameter[] parms)
    {
        string sql = SqlHelper.GetPageSQL(TableOrViewName, PrimeryKeyNames, conditionSql, "*", orderBy, pageSize, pageIndex, pageRecordOffset);
        return GetModelList(sql, parms);
    }
    /// <summary>
    /// 分页返回符合条件的记录，可指定返回字段
    /// MSSQL2000及以上可用
    /// </summary>
    /// <param name="joinSql">join语句，如inner Join B on DDStars.id=B.id</param>
    /// <param name="throwSql">将SQL语句作为异常抛出，用于检测语句正确性</param>
    /// <param name="totalCount">返回所有记录数,如果输入大于0,则不重新计算</param>
    /// <param name="conditionSql">指定条件(不含Where)</param>
    /// <param name="selectFields">要返回的字段，为空则返回所有字段</param>
    /// <param name="orderBy">排序方式(不包含order by)</param>
    /// <param name="pageSize">分页大小--每页包含的记录数</param>
    /// <param name="pageIndex">页码--要获取第几页的记录</param>
    /// <param name="parms">执行SQL语句使用的参数</param>
    /// <returns>按分页大小返回指定页码的记录的指定字段</returns>
    public IList<DDStars_Model> GetModelListMSSQL2000_JOIN(string joinSql,bool throwSql,ref int totalCount, string conditionSql, string selectFields, string orderBy, int pageSize, int pageIndex, params SqlParameter[] parms)
    {
        return GetModelListMSSQL2000_JOIN(joinSql,throwSql,ref totalCount, conditionSql, selectFields, orderBy, pageSize, pageIndex, 0, parms);
    }
    /// <summary>
    /// 分页返回符合条件的记录，可指定返回字段
    /// MSSQL2000及以上可用
    /// </summary>
    /// <param name="joinSql">join语句，如inner Join B on DDStars.id=B.id</param>
    /// <param name="throwSql">将SQL语句作为异常抛出，用于检测语句正确性</param>
    /// <param name="totalCount">返回所有记录数,如果输入大于0,则不重新计算</param>
    /// <param name="conditionSql">指定条件(不含Where)</param>
    /// <param name="selectFields">要返回的字段，为空则返回所有字段</param>
    /// <param name="orderBy">排序方式(不包含order by)</param>
    /// <param name="pageSize">分页大小--每页包含的记录数</param>
    /// <param name="pageIndex">页码--要获取第几页的记录</param>
    /// <param name="pageRecordOffset">分页时的偏移值（前面pageRecordOffset条使用其他方式或其他条件取出）</param>
    /// <param name="parms">执行SQL语句使用的参数</param>
    /// <returns>按分页大小返回指定页码的记录的指定字段</returns>
    public IList<DDStars_Model> GetModelListMSSQL2000_JOIN(string joinSql,bool throwSql,ref int totalCount, string conditionSql, string selectFields, string orderBy, int pageSize, int pageIndex,int pageRecordOffset, params SqlParameter[] parms)
    {
        DataTable dt = GetListMSSQL2000_JOIN(joinSql,throwSql,ref totalCount, conditionSql, selectFields, orderBy, pageSize, pageIndex, pageRecordOffset, parms);
        IList<DDStars_Model>
        modelList = new List<DDStars_Model>();
        foreach(DataRow dr in dt.Rows) modelList.Add(GetModel(dr));
        return modelList;
    }
    /// <summary>
    /// Internal function to get cached parameters
    /// </summary>
    /// <returns></returns>
    private static SqlParameter[] GetDDStarsParameters() {
    SqlParameter[] parms = SqlHelper.GetCachedParameters(SQL_INSERT_DDSTARS);
    if (parms == null) {
    parms = new SqlParameter[] {
    new SqlParameter(PARM_ID , SqlDbType.Int)
    ,new SqlParameter(PARM_NAME , SqlDbType.NVarChar , 100)
    };
    SqlHelper.CacheParameters(SQL_INSERT_DDSTARS, parms);
    }
    return parms;
    }
    }
 }
  
