﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ivony.Html;
using Ivony.Html.Parser;
using System.Text.RegularExpressions;
using Joz.BLL;
using Joz.Model;
using Joz.Test.BasicModel;
using Common.Unit;
using System.Collections;

namespace Joz.Test.Jobs
{
    public static class ZizaiTask
    {
        public static void RunPage()
        {
            DataGrab();
        }
        public static void RunDetail()
        {
            GetTaskManager();
        }
        /// <summary>
        /// 存取静态的访问地址
        /// </summary>
        public static List<string> httpurls = new List<string>()
        {
            "http://www.zzyo.cc/vod-list-id-{0}-pg-{1}-order--by--class--year-0-letter--area--lang-.html",
            "http://www.zzyo.cc/vod-list-id-{0}-pg-{1}-order--by--class--year-0-letter--area--lang-.html",
            "http://www.zzyo.cc/vod-list-id-{0}-pg-{1}-order--by--class--year-0-letter--area--lang-.html",
            "http://www.zzyo.cc/vod-list-id-{0}-pg-{1}-order--by--class--year-0-letter--area--lang-.html"
        };

        /// <summary>
        /// 网页数据抓取，下载
        /// </summary>
        static void DataGrab()
        {
            for (int i = 1; i < 5; i++)
            {
                //组装路径
                string homeUrl = string.Format(httpurls[i - 1], i, 1);

                var htmlSource = GetPageHtml(homeUrl);
                if (htmlSource == null) continue;

                //解析出总页数，遍历解析数据
                var navHtml = htmlSource.FindFirst(".hy-switch-tabs");
                int talPageCount = int.Parse(navHtml.FindFirst(".text-color").InnerText());
                int page = int.Parse(navHtml.FindLast(".text-color").InnerText().Split('/')[1]);
                GetPageHtml(page, homeUrl);
            }
        }

        /// <summary>
        /// 获取所有页数以及详情页路径
        /// </summary>
        /// <param name="talPageCount"></param>
        /// <param name="page"></param>
        /// <param name="htmlSource"></param>
        static void GetPageHtml(int page, string homeUrl)
        {
            //解析页数，获取需
            for (int i = 1; i <= page; i++)
            {
                string pageUrl = homeUrl.Replace("pg-1", "pg-" + i);
                var pageHtml = GetPageHtml(pageUrl);

                foreach (var item in pageHtml.Find(".item .col-md-2"))
                {
                    var findA = item.FindFirst("a");
                    string detailUrl = findA.Attribute("href").Value();
                    string videoName = findA.Attribute("title").Value();
                    //建立任务，存储留作以后更新对比  /movie/13925.html
                    var checkRecord = DDRecords_Bll.Instance.GetModel("soureid=@soureid and httpUrl=@httpUrl",
                          new System.Data.SqlClient.SqlParameter("@soureid", (int)EnumSoure.自在电影网),
                          new System.Data.SqlClient.SqlParameter("@httpUrl", detailUrl));
                    if (checkRecord == null)
                    {
                        Console.WriteLine("视频名：" + videoName);
                        DDRecords_Bll.Instance.Add(new DDRecords_Model()
                        {
                            AddTime = DateTime.Now,
                            HttpUrl = detailUrl,
                            IsUpdate = true,
                            Soureid = (int)EnumSoure.自在电影网,
                            VideoName = videoName
                        });
                    }
                }
            }
        }

        /// <summary>
        /// 抓取网页HTML
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        static IHtmlDocument GetPageHtml(string url)
        {
            try
            {
                return new JumonyParser().LoadDocument(url, Encoding.UTF8, true);
            }
            catch (Exception)
            {
                return null;
            }
        }


        static void GetTaskManager()
        {

            int talCount = DDRecords_Bll.Instance.GetListCount("Soureid=@Soureid and IsUpdate=1", new System.Data.SqlClient.SqlParameter("@Soureid", (int)EnumSoure.自在电影网));
            int avgPage = talCount / 20;
            if ((talCount % 20) > 0)
                avgPage++;

            for (int pid = 1; pid <= avgPage; pid++)
            {
                var allRecords = DDRecords_Bll.Instance.GetModelList("Soureid=@Soureid and IsUpdate=1", "", 20, pid, new System.Data.SqlClient.SqlParameter("@Soureid", (int)EnumSoure.自在电影网));
                if (allRecords == null || allRecords.Count <= 0) return;

                foreach (var item in allRecords)
                {
                    var checkVideo = DDVideoInfos_Bll.Instance.GetModel("videoName=@videoName", new System.Data.SqlClient.SqlParameter("@videoName", item.VideoName));
                    if (checkVideo != null) continue;

                    DataAnalysis("http://www.zzyo.cc" + item.HttpUrl, item.HttpUrl);
                    Console.WriteLine("处理了：" + item.VideoName);
                }
            }
        }


        /// <summary>
        /// 详情页数据分析
        /// </summary>
        static void DataAnalysis(string detailUrl, string OriginalUrl)
        {
            var detailHtml = GetPageHtml(detailUrl);
            if (string.IsNullOrEmpty(detailHtml.InnerHtml())) return;

            DDVideoInfos_Model infoModel = new DDVideoInfos_Model();
            infoModel.OriginalUrl = OriginalUrl;
            //视频详情
            var detailInfo = detailHtml.FindFirst(".hy-video-details");

            infoModel.HttpImages = detailInfo.FindFirst("dt a").Attribute("style").Value();
            Regex reg = new Regex(@"\s*url(.*)  no");
            Match match = reg.Match(infoModel.HttpImages);
            infoModel.HttpImages = match.Value.Replace("url(", "").Replace(")  no", "");

            infoModel.VideoName = detailInfo.FindFirst(".head h3").InnerText();
            infoModel.Scord = detailInfo.FindFirst(".score .branch").InnerText().ToDecimalOrZero();

            int i = 0;
            foreach (var ulHtml in detailInfo.Find("ul li"))
            {
                string texts = ulHtml.InnerText();
                if (i == 0)
                {
                    infoModel.StarNames = texts.Replace("主演：", "");
                    if (infoModel.StarNames.IsNotEmptyNotNull())
                    {
                        foreach (var star in infoModel.StarNames.Split(' '))
                        {
                            if (star.IsNotEmptyNotNull())
                            {
                                var checkStar = DDStars_Bll.Instance.GetModel("name=@name", new System.Data.SqlClient.SqlParameter("@name", star));
                                if (checkStar == null)
                                {
                                    DDStars_Bll.Instance.Add(new DDStars_Model() { Name = star });
                                }
                            }
                        }
                    }

                }
                else if (i == 1)
                    infoModel.Director = texts.Replace("导演：", "");
                else if (i == 2)
                {
                    if (texts.Replace("地区：", "").IsNotEmptyNotNull())
                    {
                        try
                        {
                            EnumArea getType = (EnumArea)Enum.Parse(typeof(EnumArea), texts.Replace("地区：", ""));
                            infoModel.AreaId = getType.GetHashCode();
                        }
                        catch (Exception)
                        {
                            infoModel.AreaId = 0;
                        }
                    }
                    else
                    {
                        infoModel.AreaId = 0;
                    }
                }
                else if (i == 3)
                {
                    if (texts.Replace("类型：", "").IsNotEmptyNotNull())
                    {
                        MovieStyle getType = (MovieStyle)Enum.Parse(typeof(MovieStyle), texts.Replace("类型：", ""));
                        infoModel.MovieType = getType.GetHashCode();
                    }
                    else
                    {
                        infoModel.MovieType = 0;
                    }
                }
                else if (i == 4)
                    infoModel.AddTime = texts.Replace("更新时间：", "").ToDateTimeOrNow();
                else if (i == 5)
                    infoModel.ShowYear = (texts.Replace("年份：", "") + "-01-01").ToDateTimeOrNow();
                else
                    infoModel.Plots = texts.Replace("剧情：", "");

                i++;
            }
            infoModel.Items = new List<DDVideoEpisode_Model>();
            //视频播放地址以及视频介绍
            var decSoure = detailHtml.FindFirst(".hy-layout .tab-content");
            //解析播放地址
            int index = 0;
            foreach (var area in decSoure.Find(".hy-play-list .panel"))
            {
                area.FindFirst("a span").Remove();
                string styleName = area.FindFirst("a").InnerText();

                var play = area.FindFirst("ul li");
                string playUrl = play.FindFirst("a").Attribute("href").Value();
                GetVideoAnalysis("http://www.zzyo.cc" + playUrl, infoModel.Items);

                if (infoModel.Items == null || infoModel.Items.Count <= 0) continue;
                var CurItems = infoModel.Items.Where(m => m.EpisodeType == index.ToString());
                if (CurItems != null && CurItems.Count() > 0)
                {
                    foreach (var item in CurItems)
                    {
                        item.EpisodeType = styleName;
                    }
                }
                index++;
            }
            //视频介绍
            infoModel.Dec = decSoure.FindLast(".hy-play-list .plot").InnerText();

            //入库
            infoModel.Id = DDVideoInfos_Bll.Instance.Add(infoModel);
            if (infoModel.Id > 0)
            {
                foreach (var item in infoModel.Items)
                {
                    item.InfoId = infoModel.Id;
                    DDVideoEpisode_Bll.Instance.Add(item);
                }
            }
        }
        /// <summary>
        /// 视频地址解析,组成数组返回
        /// </summary>
        /// <param name="videoUrl"></param>
        static void GetVideoAnalysis(string playUrl, IList<DDVideoEpisode_Model> items)
        {
            var playHtml = GetPageHtml(playUrl);
            if (string.IsNullOrEmpty(playHtml.InnerHtml())) return;
            var scripts = playHtml.FindFirst(".hy-player script").InnerText();

            //获取到隐藏的JS文件
            Regex reg = new Regex(@"\s*mac_from=.*,mac_s");
            Match match = reg.Match(scripts);
            string payJs = match.Value.Replace("mac_from='", "").Replace("',mac_s", "");
            string[] pays = payJs.Replace("$$$", "|").Split('|');

            //获取到播放的路径文件
            reg = new Regex(@"\s*unescape.*;");
            match = reg.Match(scripts);
            string payUrls = match.Value.Replace("unescape('", "").Replace("');", "");
            payUrls = Microsoft.JScript.GlobalObject.unescape(payUrls);

            string[] setNum = payUrls.Replace("$$$", "|").Split('|');
            int i = 0;
            foreach (var str in pays)
            {
                //请求JS文件，获取首位路径,补接播放路径
                var jsText = GetPageHtml("http://www.zzyo.cc/player/" + str + ".js");
                if (jsText == null || string.IsNullOrEmpty(jsText.InnerHtml())) return;
                string jsTexta = jsText.InnerHtml();

                reg = new Regex(@"\s*src=.*url=");
                match = reg.Match(jsTexta);
                jsTexta = match.Value.Replace("src=\"", "");

                foreach (var son in setNum[i].Split('#'))
                {
                    var item = new DDVideoEpisode_Model();
                    item.EpisodeTranUrl = jsTexta;
                    item.EpisodeType = i.ToString();

                    if (son.Contains("$"))
                    {
                        item.EpisodeUrl = son.Split('$')[1];
                        item.EpisodeCount = son.Split('$')[0];
                    }
                    else
                    {
                        item.EpisodeUrl = son;
                        item.EpisodeCount = "";
                    }
                    items.Add(item);
                }
                i++;
            }
        }
    }
}
