﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Joz.Test.BasicModel
{
    /// <summary>
    /// 影视剧基本信息
    /// </summary>
    public class MovieInfo
    {
        /// <summary>
        /// 影视剧名称
        /// </summary>
        public string MovieName { get; set; }
        /// <summary>
        /// 剧照（存虚拟路径）
        /// </summary>
        public string Imgs { get; set; }
        /// <summary>
        /// 参与演员名称组合
        /// </summary>
        public string StartNames { get; set; }
        /// <summary>
        /// 影视评分（来自豆瓣）
        /// </summary>
        public decimal Score { get; set; }
        /// <summary>
        /// 导演名称
        /// </summary>
        public string Director { get; set; }

        /// <summary>
        /// 影视剧类型（看枚举类型）电影，电视剧，综艺
        /// </summary>
        public string MovieType { get; set; }

        /// <summary>
        /// 语言类型（看枚举） 普通话，粤语，英语等
        /// </summary>
        public string Language { get; set; }
        /// <summary>
        /// 上映时间（具体时间去豆瓣抓取）
        /// </summary>
        public DateTime ShowDate { get; set; }
        /// <summary>
        /// 具体的年份，用于搜索
        /// </summary>
        public int ShowYear { get; set; }
        /// <summary>
        /// 剧情名称组合（建立枚举）神话,剧情,历史等
        /// </summary>
        public string Plots { get; set; }
        /// <summary>
        /// 添加记录时间
        /// </summary>
        public DateTime addTime { get; set; }
        /// <summary>
        /// 剧情简介
        /// </summary>
        public string Dec { get; set; }
        /// <summary>
        /// 原网址的访问路径
        /// </summary>
        public string OriginalUrl { get; set; }
        /// <summary>
        /// 区域ID，看枚举
        /// </summary>
        public int AreaId { get; set; }
    }
    /// <summary>
    /// 集数
    /// </summary>
    public class MovieEpisode
    {
        /// <summary>
        /// 数据来源名称（需要分组，依据名称）
        /// </summary>
        public string EpisodeType { get; set; }
        /// <summary>
        /// 集数 1，2，或者 BD,HD等
        /// </summary>
        public string EpisodeCount { get; set; }
        /// <summary>
        /// 跳转播放路径
        /// </summary>
        public string EpisodeUrl { get; set; }
        /// <summary>
        /// 集数介绍
        /// </summary>
        public string EpisodeDec { get; set; }
        /// <summary>
        /// 关联主表
        /// </summary>
        public int InfoId { get; set; }
    }


    /// <summary>
    /// 地区枚举
    /// </summary>
    public enum EnumArea
    {
        大陆 = 1,
        香港 = 2,
        台湾 = 3,
        美国 = 4,
        韩国 = 5,
        日本 = 6,
        泰国 = 7,
        新加坡 = 8,
        马来西亚 = 9,
        印度 = 10,
        英国 = 11,
        法国 = 12,
        加拿大 = 13,
        西班牙 = 14,
        俄罗斯 = 15,
        其它 = 16,
        德国 = 17,
        中国大陆 = 18
    }
    /// <summary>
    /// 语言类型
    /// </summary>
    public enum EnumLang
    {
        普通话 = 1,
        粤语 = 2,
        英语 = 3,
        法语 = 4,
        德语 = 5
    }
    /// <summary>
    /// 影视剧枚举
    /// </summary>
    public enum MovieStyle
    {
        电影 = 1,
        电视剧 = 2,
        综艺 = 3,
        动画片 = 4
    }
    /// <summary>
    /// 电视剧剧情枚举
    /// </summary>
    public enum EnumTvPlots
    {
        言情 = 1,
        伦理 = 2,
        喜剧 = 3,
        悬疑 = 4,
        都市 = 5,
        偶像 = 6,
        古装 = 7,
        军事 = 8,
        警匪 = 9,
        历史 = 10,
        武侠 = 11,
        科幻 = 12,
        宫廷 = 13,
        抗日 = 14,
        动作 = 15,
        神话 = 16,
        励志 = 17,
        谍战 = 18,
        农村 = 19,
        恐怖 = 20,
        犯罪 = 21,
        剧情 = 22
    }
    /// <summary>
    /// 电影剧情类型枚举
    /// </summary>
    public enum EnumMoviePlots
    {
        喜剧 = 1,
        爱情 = 2,
        动作 = 3,
        恐怖 = 4,
        科幻 = 5,
        剧情 = 6,
        犯罪 = 7,
        奇幻 = 8,
        战争 = 9,
        记录 = 10,
        伦理 = 11,
        文艺 = 12,
        动画 = 13,
        悬疑 = 14,
        传记 = 15,
        歌舞 = 16,
        古装 = 17,
        历史 = 18,
        惊悚 = 19,
        冒险20
    }
    /// <summary>
    /// 动画剧情类型
    /// </summary>
    public enum EnumAnimation
    {
        热血 = 1,
        美少女 = 2,
        恋爱 = 3,
        运动 = 4,
        校园 = 5,
        搞笑 = 6,
        剧情 = 7,
        冒险 = 8,
        推理 = 9,
        魔幻 = 10,
        幻想 = 11,
        少儿 = 12,
        亲子 = 13,
        机战 = 14,
        科幻 = 15,
        动物 = 16,
        神话 = 17,
        历史 = 18,
        战争 = 19,
        经典 = 20,
        耽美 = 21,
        男性向 = 22,
        女性向 = 23
    }
    /// <summary>
    /// 综艺剧情类型
    /// </summary>
    public enum EnumVariety
    {
        八卦 = 1,
        美妆 = 2,
        大陆 = 3,
        求职 = 4,
        选秀 = 5,
        访谈 = 6,
        情感 = 7,
        生活 = 8,
        晚会 = 9,
        搞笑 = 10,
        音乐 = 11,
        时尚 = 12,
        游戏 = 13,
        少儿 = 14,
        体育 = 15,
        科教 = 16,
        曲艺 = 17,
        歌舞 = 18,
        财经 = 19,
        汽车 = 20,
        真人秀 = 21
    }

}
