﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Joz.Test.BasicModel
{
    public class Records
    {
        /// <summary>
        /// 电影名
        /// </summary>
        public string MovieName { get; set; }
        /// <summary>
        /// 访问路径
        /// </summary>
        public string HttpUrl { get; set; }
        /// <summary>
        /// 来源ID
        /// </summary>
        public int SourceId { get; set; }
        /// <summary>
        /// 添加时间
        /// </summary>
        public DateTime AddTime { get; set; }
        /// <summary>
        /// 是否更新抓取
        /// </summary>
        public bool IsUpdate { get; set; }

    }

    public enum EnumSoure
    {
        自在电影网 = 1
    }
}
