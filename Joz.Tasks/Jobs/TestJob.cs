﻿using Quartz;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Joz.Tasks.Jobs
{
    public class TestJob : IJob
    {
        public void Execute(IJobExecutionContext context)
        {
            JobDataMap data = context.JobDetail.JobDataMap;
            Console.WriteLine("quartz --{0}", data.GetString("TaskParam"));
        }
    }
}
