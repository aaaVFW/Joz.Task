﻿
using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Text;
using Joz.Model;
using Joz.DAL;
namespace Joz.BLL
{
    /// <summary>
    /// 类DDStars_Bll
    /// 此文件由CoderGoCodeBuilder v1.0自动生成，请不要手动修改此文件，以便日后自动覆盖
    /// 2018-12-26 16:15:22
    /// </summary>
    [Serializable]
    public partial class DDStars_Bll
    {
    private readonly DDStars_Dal dal = DDStars_Dal.Instance;
    public static readonly DDStars_Bll Instance;
    static DDStars_Bll()
    {
        Instance = new DDStars_Bll();
    }
    private DDStars_Bll()
    { }
    public int Add(DDStars_Model model)
    {
        return dal.Add(model);
    }
    /// <summary>
    /// 将model实体以一条数据记录插入数据表, 使用事务
    /// 此方法为自动生成，请不要手动修改
    /// </summary>
    /// <param name="model">要以记录插入数据库的对象</param>
    /// <param name="model">使用的事务对象,可为null </param>
    /// <returns>Id</returns>
    public int Add(DDStars_Model model, SqlTransaction trans)
    {
    return dal.Add(model,trans);
    }
    /// <summary>
    /// 根据model更新该实体对应的数据表记录的所有字段，返回受影响的行数
    /// 此方法为自动生成，请不要手动修改
    /// </summary>
    /// <param name="model">要更新的实体对象</param>
    /// <returns>受影响的行数</returns>
    public int Update(DDStars_Model model)
    {
    return dal.Update(model);
    }
    /// <summary>
    /// 根据model更新该实体对应的数据表记录，可指定部分行，返回受影响的行数
    /// 此方法为自动生成，请不要手动修改
    /// </summary>
    /// <param name="UpdateSql">指定更新SQL(不含set)</param>
    /// <param name="model">要更新的记录对象</param>
    /// <returns>受影响的行数</returns>
    public int Update(string UpdateSql, DDStars_Model model)
    {
    return dal.Update(UpdateSql,model);
    }
    /// <summary>
    /// 使用事务，根据model更新该实体对应的数据表记录，可指定部分行，返回受影响的行数
    /// 此方法为自动生成，请不要手动修改
    /// </summary>
    /// <param name="trans">使用的事务,可以为null</param>
    /// <param name="UpdateSql">指定更新SQL(不含set)</param>
    /// <param name="model">要更新的记录对象</param>
    /// <returns>受影响的行数</returns>
    public int Update(SqlTransaction trans, string UpdateSql, DDStars_Model model)
    {
      return dal.Update(trans,UpdateSql,model);
    }
    /// <summary>
    /// 使用事务（或不使用），根据条件更新对应的数据表记录，可指定部分行，返回受影响的行数
    /// 此方法为自动生成，请不要手动修改
    /// </summary>
    /// <param name="trans">使用的事务,可以为null</param>
    /// <param name="updateSql">指定更新SQL(不含set)，必须指明更新细节</param>
    /// <param name="conditionSql">指定更新条件(不含where)，不接受空条件</param>
    /// <param name="parms">参数</param>
    /// <returns>受影响的行数</returns>
    public int Update(SqlTransaction trans, string updateSql, string conditionSql, params SqlParameter[] parms)
    {
      return dal.Update(trans,updateSql,conditionSql,parms);
    }
    /// <summary>
    /// 根据条件更新对应的数据表记录，可指定部分行，返回受影响的行数
    /// 此方法为自动生成，请不要手动修改
    /// </summary>
    /// <param name="updateSql">指定更新SQL(不含set)，必须指明更新细节</param>
    /// <param name="conditionSql">指定更新条件(不含where)，不接受空条件</param>
    /// <param name="parms">参数</param>
    /// <returns>受影响的行数</returns>
    public int Update(string updateSql, string conditionSql, params SqlParameter[] parms)
    {
        return dal.Update(null,updateSql,conditionSql,parms);
    }
    /// <summary>
    /// 根据主键返回实体对象
    /// </summary>
    /// <returns></returns>
    public DDStars_Model GetModelByID(int id)
    {
        return dal.GetModelByID(id);
    }
    /// <summary>
    /// 根据主键返回实体对象
    /// </summary>
    /// <returns></returns>
    public DDStars_Model GetModelByID(int id, SqlTransaction trans)
    {
        return dal.GetModelByID(id,trans);
    }
    /// <summary>
    /// 根据主键返回实体对象
    /// </summary>
    /// <returns></returns>
    public DDStars_Model GetModelByID(int id,string selectFields)
    {
        return dal.GetModelByID(id,selectFields,null);
    }
    /// <summary>
    /// 根据主键返回实体对象
    /// </summary>
    /// <returns></returns>
    public DDStars_Model GetModelByID(int id,string selectFields, SqlTransaction trans)
    {
        return dal.GetModelByID(id,selectFields,trans);
    }
    /// <summary>
    /// 返回符合条件的第一个对象
    /// </summary>
    /// <param name="conditionSql">条件SQL(不含where)</param>
    /// <param name="parms">SQL参数</param>
    /// <returns>返回符合条件的第一个对象</returns>
    public DDStars_Model GetModel(string conditionSql, params SqlParameter[] parms)
    {
        return dal.GetModel(conditionSql,parms);
    }
    /// <summary>
    /// 返回符合条件的第一个对象
    /// </summary>
    /// <param name="conditionSql">条件SQL(不含where)</param>
    /// <param name="parms">SQL参数</param>
    /// <returns>返回符合条件的第一个对象</returns>
    public DDStars_Model GetModel(string conditionSql, string selectFields, params SqlParameter[] parms)
    {
        return dal.GetModel(conditionSql,selectFields,parms);
    }
    /// <summary>
    /// 返回符合条件的第一个对象
    /// </summary>
    /// <param name="conditionSql">条件SQL(不含where)</param>
    /// <param name="trans">事务，可为null</param>
    /// <param name="parms">SQL参数</param>
    /// <returns>返回符合条件的第一个对象</returns>
    public DDStars_Model GetModel(string conditionSql, SqlTransaction trans, params SqlParameter[] parms)
    {
        return dal.GetModel(conditionSql,trans, parms);
    }
    /// <summary>
    /// 返回符合条件的第一个对象
    /// </summary>
    /// <param name="conditionSql">条件SQL(不含where)</param>
    /// <param name="trans">事务，可为null</param>
    /// <param name="parms">SQL参数</param>
    /// <returns>返回符合条件的第一个对象</returns>
    public DDStars_Model GetModel(string conditionSql, string selectFields, SqlTransaction trans, params SqlParameter[] parms)
    {
        return dal.GetModel(conditionSql,selectFields,trans, parms);
    }
    /// <summary>
    /// 统计符合条件的记录数
    /// </summary>
    /// <param name="conditionSql">条件SQL(不含where)</param>
    /// <param name="parms">SQL参数</param>
    /// <returns></returns>
    public int GetListCount(string conditionSql, params SqlParameter[] parms)
    {
      return dal.GetListCount(conditionSql,parms);
    }
    /// <summary>
    /// 统计符合条件的记录数
    /// </summary>
    /// <param name="conditionSql">条件SQL(不含where)</param>
    /// <param name="countCol">指定统计列（可用Distinct关键字）</param>
    /// <param name="parms">SQL参数</param>
    /// <returns></returns>
    public int GetListCount(string conditionSql, string countCol, params SqlParameter[] parms)
    {
      return dal.GetListCount(conditionSql,countCol,parms);
    }
    /// <summary>
    /// 统计符合条件的记录数，使用事务
    /// </summary>
    /// <param name="trans">事务(可为空)</param>
    /// <param name="conditionSql">条件SQL(不含where)</param>
    /// <param name="parms">SQL参数</param>
    /// <returns></returns>
    public int GetListCount(SqlTransaction trans, string conditionSql, params SqlParameter[] parms)
    {
        return dal.GetListCount(trans,conditionSql,parms);
    }
    /// <summary>
    /// 统计符合条件的记录数，使用事务
    /// </summary>
    /// <param name="trans">事务(可为空)</param>
    /// <param name="conditionSql">条件SQL(不含where)</param>
    /// <param name="countCol">指定统计列（可用Distinct关键字）</param>
    /// <param name="parms">SQL参数</param>
    /// <returns></returns>
    public int GetListCount(SqlTransaction trans, string conditionSql,string countCol, params SqlParameter[] parms)
    {
        return dal.GetListCount(trans,conditionSql,countCol,parms);
    }
    /// <summary>
    ///
    /// </summary>
    /// <param name="sql">完整的SQL</param>
    /// <param name="parms"></param>
    /// <returns></returns>
    public IList<DDStars_Model> GetModelList(string sql, params SqlParameter[] parms)
    {
        return dal.GetModelList(sql, parms);
    }
    /// <summary>
    ///
    /// </summary>
    /// <param name="conditionSql">指定条件(不含Where)</param>
    /// <param name="selectFields">要返回的字段，为空则返回所有字段</param>
    /// <param name="orderBy">排序方式(不包含order by)</param>
    /// <param name="parms">执行SQL语句使用的参数</param>
    /// <returns></returns>
    public IList<DDStars_Model> GetModelList(string conditionSql, string selectFields, string orderBy, params SqlParameter[] parms)
    {
        return dal.GetModelList(conditionSql,selectFields,orderBy, parms);
    }
    /// <summary>
    ///
    /// </summary>
    /// <param name="conditionSql">指定条件(不含Where)</param>
    /// <param name="selectFields">要返回的字段，为空则返回所有字段</param>
    /// <param name="orderBy">排序方式(不包含order by)</param>
    /// <param name="trans">事务(可为null)</param>
    /// <param name="parms">执行SQL语句使用的参数</param>
    /// <returns></returns>
    public IList<DDStars_Model> GetModelList(string conditionSql, string selectFields, string orderBy, SqlTransaction trans, params SqlParameter[] parms)
    {
        return dal.GetModelList(conditionSql,selectFields,orderBy,trans, parms);
    }
    /// <summary>
    /// 返回符合条件的所有记录，可指定返回字段
    /// </summary>
    /// <param name="conditionSql">指定条件(不含Where)</param>
    /// <param name="selectFields">要返回的字段，为空则返回所有字段</param>
    /// <param name="orderBy">排序方式(不包含order by)</param>
    /// <param name="parms">执行SQL语句使用的参数</param>
    /// <returns>按分页大小返回指定页码的记录的指定字段</returns>
    public DataTable GetList(string conditionSql, string selectFields, string orderBy, params SqlParameter[] parms)
    {
        return dal.GetList(conditionSql,selectFields,orderBy, parms);
    }
    /// <summary>
    /// 返回符合条件的所有记录，可指定返回字段
    /// </summary>
    /// <param name="conditionSql">指定条件(不含Where)</param>
    /// <param name="selectFields">要返回的字段，为空则返回所有字段</param>
    /// <param name="orderBy">排序方式(不包含order by)</param>
    /// <param name="trans">事务（可为null）</param>
    /// <param name="parms">执行SQL语句使用的参数</param>
    /// <returns>按分页大小返回指定页码的记录的指定字段</returns>
    public DataTable GetList(string conditionSql, string selectFields, string orderBy, SqlTransaction trans, params SqlParameter[] parms)
    {
        return dal.GetList(conditionSql,selectFields,orderBy,trans, parms);
    }
    /// <summary>
    /// 分页返回符合条件的记录，可指定返回字段
    /// MSSQL2005及以上可用
    /// </summary>
    /// <param name="totalCount">返回所有记录数,如果输入大于0,则不重新计算</param>
    /// <param name="conditionSql">指定条件(不含Where)</param>
    /// <param name="selectFields">要返回的字段，为空则返回所有字段</param>
    /// <param name="orderBy">排序方式(不包含order by)</param>
    /// <param name="pageSize">分页大小--每页包含的记录数</param>
    /// <param name="pageIndex">页码--要获取第几页的记录</param>
    /// <param name="parms">执行SQL语句使用的参数</param>
    /// <returns>按分页大小返回指定页码的记录的指定字段</returns>
    public DataTable GetList(ref int totalCount, string conditionSql, string selectFields, string orderBy, int pageSize, int pageIndex, params SqlParameter[] parms)
    {
    return dal.GetList(ref totalCount, conditionSql, selectFields, orderBy, pageSize, pageIndex, parms);
    }
    /// <summary>
    /// 分页返回符合条件的记录，可指定返回字段
    /// 注意：仅MSSQL2005及以上可用
    /// </summary>
    /// <param name="totalCount">返回所有记录数,如果输入大于0,则不重新计算</param>
    /// <param name="conditionSql">指定条件(不含Where)</param>
    /// <param name="selectFields">要返回的字段，为空则返回所有字段</param>
    /// <param name="orderBy">排序方式(不包含order by)</param>
    /// <param name="pageSize">分页大小--每页包含的记录数</param>
    /// <param name="pageIndex">页码--要获取第几页的记录</param>
    /// <param name="parms">执行SQL语句使用的参数</param>
    /// <returns>按分页大小返回指定页码的记录的指定字段</returns>
    public IList<DDStars_Model> GetModelList(ref int totalCount, string conditionSql, string selectFields, string orderBy, int pageSize, int pageIndex, params SqlParameter[] parms)
    {
    return dal.GetModelList(ref totalCount, conditionSql, selectFields, orderBy, pageSize, pageIndex, parms);
    }
    /// <summary>
    /// 分页返回符合条件的记录，可指定返回字段
    /// MSSQL2005及以上可用
    /// </summary>
    /// <param name="conditionSql">指定条件(不含Where)</param>
    /// <param name="selectFields">要返回的字段，为空则返回所有字段</param>
    /// <param name="orderBy">排序方式(不包含order by)</param>
    /// <param name="pageSize">分页大小--每页包含的记录数</param>
    /// <param name="pageIndex">页码--要获取第几页的记录</param>
    /// <param name="parms">执行SQL语句使用的参数</param>
    /// <returns>按分页大小返回指定页码的记录的指定字段</returns>
    public SqlDataReader GetList(string conditionSql, string selectFields, string orderBy, int pageSize, int pageIndex, params SqlParameter[] parms)
    {
    return dal.GetList(conditionSql, selectFields, orderBy, pageSize, pageIndex, parms);
    }
    /// <summary>
    /// 分页返回符合条件的记录
    /// MSSQL2005及以上可用
    /// </summary>
    /// <param name="conditionSql">指定条件(不含Where)</param>
    /// <param name="selectFields">要返回的字段，为空则返回所有字段</param>
    /// <param name="orderBy">排序方式(不包含order by)</param>
    /// <param name="pageSize">分页大小--每页包含的记录数</param>
    /// <param name="pageIndex">页码--要获取第几页的记录</param>
    /// <param name="parms">执行SQL语句使用的参数</param>
    /// <returns>按分页大小返回指定页码的记录的指定字段</returns>
    public IList<DDStars_Model> GetModelList(string conditionSql, string orderBy, int pageSize, int pageIndex, params SqlParameter[] parms)
    {
    return dal.GetModelList(conditionSql, orderBy, pageSize, pageIndex, parms);
    }
    /// <summary>
    /// 分页返回符合条件的记录，可指定返回字段
    /// MSSQL2005及以上可用
    /// </summary>
    /// <param name="joinSql">join语句，如inner Join B on GP_Mails.fdMailID=B.fdMailID</param>
    /// <param name="throwSql">将SQL语句作为异常抛出，用于检测语句正确性</param>
    /// <param name="totalCount">返回所有记录数,如果输入大于0,则不重新计算</param>
    /// <param name="conditionSql">指定条件(不含Where)</param>
    /// <param name="selectFields">要返回的字段，为空则返回所有字段</param>
    /// <param name="orderBy">排序方式(不包含order by)</param>
    /// <param name="pageSize">分页大小--每页包含的记录数</param>
    /// <param name="pageIndex">页码--要获取第几页的记录</param>
    /// <param name="parms">执行SQL语句使用的参数</param>
    /// <returns>按分页大小返回指定页码的记录的指定字段</returns>
    public DataTable GetList_JOIN(string joinSql,bool throwSql,ref int totalCount, string conditionSql, string selectFields, string orderBy, int pageSize, int pageIndex, params SqlParameter[] parms)
    {
    return dal.GetList_JOIN(joinSql,throwSql,ref totalCount, conditionSql, selectFields, orderBy, pageSize, pageIndex, parms);
    }
    /// <summary>
    /// 分页返回符合条件的记录，可指定返回字段
    /// MSSQL2005及以上可用
    /// </summary>
    /// <param name="joinSql">join语句，如inner Join B on GP_Mails.fdMailID=B.fdMailID</param>
    /// <param name="throwSql">将SQL语句作为异常抛出，用于检测语句正确性</param>
    /// <param name="totalCount">返回所有记录数,如果输入大于0,则不重新计算</param>
    /// <param name="conditionSql">指定条件(不含Where)</param>
    /// <param name="selectFields">要返回的字段，为空则返回所有字段</param>
    /// <param name="orderBy">排序方式(不包含order by)</param>
    /// <param name="pageSize">分页大小--每页包含的记录数</param>
    /// <param name="pageIndex">页码--要获取第几页的记录</param>
    /// <param name="parms">执行SQL语句使用的参数</param>
    /// <returns>按分页大小返回指定页码的记录的指定字段</returns>
    public IList<DDStars_Model> GetModelList_JOIN(string joinSql,bool throwSql,ref int totalCount, string conditionSql, string selectFields, string orderBy, int pageSize, int pageIndex, params SqlParameter[] parms)
    {
    return dal.GetModelList_JOIN(joinSql,throwSql,ref totalCount, conditionSql, selectFields, orderBy, pageSize, pageIndex, parms);
    }
    /// <summary>
    /// 分页返回符合条件的记录，可指定返回字段
    /// MSSQL2005及以上可用
    /// </summary>
    /// <param name="totalCount">返回所有记录数,如果输入大于0,则不重新计算</param>
    /// <param name="conditionSql">指定条件(不含Where)</param>
    /// <param name="selectFields">要返回的字段，为空则返回所有字段</param>
    /// <param name="orderBy">排序方式(不包含order by)</param>
    /// <param name="pageSize">分页大小--每页包含的记录数</param>
    /// <param name="pageIndex">页码--要获取第几页的记录</param>
    /// <param name="pageRecordOffset">分页时的偏移值（前面pageRecordOffset条使用其他方式或其他条件取出）</param>
    /// <param name="parms">执行SQL语句使用的参数</param>
    /// <returns>按分页大小返回指定页码的记录的指定字段</returns>
    public DataTable GetList(ref int totalCount, string conditionSql, string selectFields, string orderBy, int pageSize, int pageIndex, int pageRecordOffset, params SqlParameter[] parms)
    {
        return dal.GetList(ref totalCount, conditionSql, selectFields, orderBy, pageSize, pageIndex, pageRecordOffset, parms);
    }
    /// <summary>
    /// 分页返回符合条件的记录，可指定返回字段
    /// 注意：仅MSSQL2005及以上可用
    /// </summary>
    /// <param name="totalCount">返回所有记录数,如果输入大于0,则不重新计算</param>
    /// <param name="conditionSql">指定条件(不含Where)</param>
    /// <param name="selectFields">要返回的字段，为空则返回所有字段</param>
    /// <param name="orderBy">排序方式(不包含order by)</param>
    /// <param name="pageSize">分页大小--每页包含的记录数</param>
    /// <param name="pageIndex">页码--要获取第几页的记录</param>
    /// <param name="pageRecordOffset">分页时的偏移值（前面pageRecordOffset条使用其他方式或其他条件取出，小于0则表示最前面pageRecordOffset条记录被忽略）</param>
    /// <param name="parms">执行SQL语句使用的参数</param>
    /// <returns>按分页大小返回指定页码的记录的指定字段</returns>
    public IList<DDStars_Model> GetModelList(ref int totalCount, string conditionSql, string selectFields, string orderBy, int pageSize, int pageIndex, int pageRecordOffset, params SqlParameter[] parms)
    {
        return dal.GetModelList(ref totalCount, conditionSql, selectFields, orderBy, pageSize, pageIndex, pageRecordOffset, parms);
    }
    /// <summary>
    /// 分页返回符合条件的记录，可指定返回字段
    /// MSSQL2005及以上可用
    /// </summary>
    /// <param name="conditionSql">指定条件(不含Where)</param>
    /// <param name="selectFields">要返回的字段，为空则返回所有字段</param>
    /// <param name="orderBy">排序方式(不包含order by)</param>
    /// <param name="pageSize">分页大小--每页包含的记录数</param>
    /// <param name="pageIndex">页码--要获取第几页的记录</param>
    /// <param name="pageRecordOffset">分页时的偏移值（前面pageRecordOffset条使用其他方式或其他条件取出，小于0则表示最前面pageRecordOffset条记录被忽略）</param>
    /// <param name="parms">执行SQL语句使用的参数</param>
    /// <returns>按分页大小返回指定页码的记录的指定字段</returns>
    public SqlDataReader GetList(string conditionSql, string selectFields, string orderBy, int pageSize, int pageIndex, int pageRecordOffset, params SqlParameter[] parms)
    {
        return dal.GetList(conditionSql, selectFields, orderBy, pageSize, pageIndex, pageRecordOffset, parms);
    }
    /// <summary>
    /// 分页返回符合条件的记录
    /// MSSQL2005及以上可用
    /// </summary>
    /// <param name="conditionSql">指定条件(不含Where)</param>
    /// <param name="selectFields">要返回的字段，为空则返回所有字段</param>
    /// <param name="orderBy">排序方式(不包含order by)</param>
    /// <param name="pageSize">分页大小--每页包含的记录数</param>
    /// <param name="pageIndex">页码--要获取第几页的记录</param>
    /// <param name="pageRecordOffset">分页时的偏移值（前面pageRecordOffset条使用其他方式或其他条件取出，小于0则表示最前面pageRecordOffset条记录被忽略）</param>
    /// <param name="parms">执行SQL语句使用的参数</param>
    /// <returns>按分页大小返回指定页码的记录的指定字段</returns>
    public IList<DDStars_Model> GetModelList(string conditionSql, string orderBy, int pageSize, int pageIndex, int pageRecordOffset, params SqlParameter[] parms)
    {
        return dal.GetModelList(conditionSql, orderBy, pageSize, pageIndex, pageRecordOffset, parms);
    }
    /// <summary>
    /// 分页返回符合条件的记录，可指定返回字段
    /// MSSQL2005及以上可用
    /// </summary>
    /// <param name="joinSql">join语句，如inner Join B on GP_Mails.fdMailID=B.fdMailID</param>
    /// <param name="throwSql">将SQL语句作为异常抛出，用于检测语句正确性</param>
    /// <param name="totalCount">返回所有记录数,如果输入大于0,则不重新计算</param>
    /// <param name="conditionSql">指定条件(不含Where)</param>
    /// <param name="selectFields">要返回的字段，为空则返回所有字段</param>
    /// <param name="orderBy">排序方式(不包含order by)</param>
    /// <param name="pageSize">分页大小--每页包含的记录数</param>
    /// <param name="pageIndex">页码--要获取第几页的记录</param>
    /// <param name="pageRecordOffset">分页时的偏移值（前面pageRecordOffset条使用其他方式或其他条件取出，小于0则表示最前面pageRecordOffset条记录被忽略）</param>
    /// <param name="parms">执行SQL语句使用的参数</param>
    /// <returns>按分页大小返回指定页码的记录的指定字段</returns>
    public DataTable GetList_JOIN(string joinSql,bool throwSql,ref int totalCount, string conditionSql, string selectFields, string orderBy, int pageSize, int pageIndex, int pageRecordOffset, params SqlParameter[] parms)
    {
        return dal.GetList_JOIN(joinSql,throwSql,ref totalCount, conditionSql, selectFields, orderBy, pageSize, pageIndex, pageRecordOffset, parms);
    }
    /// <summary>
    /// 分页返回符合条件的记录，可指定返回字段
    /// MSSQL2005及以上可用
    /// </summary>
    /// <param name="joinSql">join语句，如inner Join B on GP_Mails.fdMailID=B.fdMailID</param>
    /// <param name="throwSql">将SQL语句作为异常抛出，用于检测语句正确性</param>
    /// <param name="totalCount">返回所有记录数,如果输入大于0,则不重新计算</param>
    /// <param name="conditionSql">指定条件(不含Where)</param>
    /// <param name="selectFields">要返回的字段，为空则返回所有字段</param>
    /// <param name="orderBy">排序方式(不包含order by)</param>
    /// <param name="pageSize">分页大小--每页包含的记录数</param>
    /// <param name="pageIndex">页码--要获取第几页的记录</param>
    /// <param name="pageRecordOffset">分页时的偏移值（前面pageRecordOffset条使用其他方式或其他条件取出，小于0则表示最前面pageRecordOffset条记录被忽略）</param>
    /// <param name="parms">执行SQL语句使用的参数</param>
    /// <returns>按分页大小返回指定页码的记录的指定字段</returns>
    public IList<DDStars_Model> GetModelList_JOIN(string joinSql,bool throwSql,ref int totalCount, string conditionSql, string selectFields, string orderBy, int pageSize, int pageIndex, int pageRecordOffset, params SqlParameter[] parms)
    {
        return dal.GetModelList_JOIN(joinSql,throwSql,ref totalCount, conditionSql, selectFields, orderBy, pageSize, pageIndex, pageRecordOffset, parms);
    }
    /// <summary>
    /// 分页返回符合条件的记录，可指定返回字段
    /// 注意：仅MSSQL2005及以上可用
    /// </summary>
    /// <param name="totalCount">返回所有记录数,如果输入大于0,则不重新计算</param>
    /// <param name="conditionSql">指定条件(不含Where)</param>
    /// <param name="selectFields">要返回的字段，为空则返回所有字段</param>
    /// <param name="orderBy">排序方式(不包含order by)</param>
    /// <param name="pageSize">分页大小--每页包含的记录数</param>
    /// <param name="pageIndex">页码--要获取第几页的记录</param>
    /// <param name="parms">执行SQL语句使用的参数</param>
    /// <returns>按分页大小返回指定页码的记录的指定字段</returns>
    public DataTable GetListMSSQL2005(ref int totalCount, string conditionSql, string selectFields, string orderBy, int pageSize, int pageIndex, params SqlParameter[] parms)
    {
      return dal.GetListMSSQL2005(ref totalCount, conditionSql, selectFields, orderBy, pageSize, pageIndex, parms);
    }
    /// <summary>
    /// 分页返回符合条件的记录，可指定返回字段
    /// MSSQL2005及以上可用
    /// </summary>
    /// <param name="conditionSql">指定条件(不含Where)</param>
    /// <param name="selectFields">要返回的字段，为空则返回所有字段</param>
    /// <param name="orderBy">排序方式(不包含order by)</param>
    /// <param name="pageSize">分页大小--每页包含的记录数</param>
    /// <param name="pageIndex">页码--要获取第几页的记录</param>
    /// <param name="parms">执行SQL语句使用的参数</param>
    /// <returns>按分页大小返回指定页码的记录的指定字段</returns>
    public SqlDataReader GetListMSSQL2005(string conditionSql, string selectFields, string orderBy, int pageSize, int pageIndex, params SqlParameter[] parms)
    {
    return dal.GetListMSSQL2005(conditionSql, selectFields, orderBy, pageSize, pageIndex,  parms);
    }
    /// <summary>
    /// 分页返回符合条件的记录，可指定返回字段
    /// 注意：仅MSSQL2005及以上可用
    /// </summary>
    /// <param name="totalCount">返回所有记录数,如果输入大于0,则不重新计算</param>
    /// <param name="conditionSql">指定条件(不含Where)</param>
    /// <param name="selectFields">要返回的字段，为空则返回所有字段</param>
    /// <param name="orderBy">排序方式(不包含order by)</param>
    /// <param name="pageSize">分页大小--每页包含的记录数</param>
    /// <param name="pageIndex">页码--要获取第几页的记录</param>
    /// <param name="parms">执行SQL语句使用的参数</param>
    /// <returns>按分页大小返回指定页码的记录的指定字段</returns>
    public IList<DDStars_Model> GetModelListMSSQL2005(ref int totalCount, string conditionSql, string selectFields, string orderBy, int pageSize, int pageIndex, params SqlParameter[] parms)
    {
    return dal.GetModelListMSSQL2005(ref totalCount, conditionSql, selectFields, orderBy, pageSize, pageIndex, parms);
    }
    /// <summary>
    /// 分页返回符合条件的记录
    /// 注意：仅MSSQL2005及以上可用
    /// </summary>
    /// <param name="conditionSql">指定条件(不含Where)</param>
    /// <param name="selectFields">要返回的字段，为空则返回所有字段</param>
    /// <param name="orderBy">排序方式(不包含order by)</param>
    /// <param name="pageSize">分页大小--每页包含的记录数</param>
    /// <param name="pageIndex">页码--要获取第几页的记录</param>
    /// <param name="parms">执行SQL语句使用的参数</param>
    /// <returns>按分页大小返回指定页码的记录的指定字段</returns>
    public IList<DDStars_Model> GetModelListMSSQL2005(string conditionSql, string orderBy, int pageSize, int pageIndex, params SqlParameter[] parms)
    {
    return dal.GetModelListMSSQL2005(conditionSql, orderBy, pageSize, pageIndex, parms);
    }
    /// <summary>
    /// 分页返回符合条件的记录，可指定返回字段
    /// 注意：仅MSSQL2005及以上可用
    /// </summary>
    /// <param name="joinSql">join语句，如inner Join B on GP_Mails.fdMailID=B.fdMailID</param>
    /// <param name="throwSql">将SQL语句作为异常抛出，用于检测语句正确性</param>
    /// <param name="totalCount">返回所有记录数,如果输入大于0,则不重新计算</param>
    /// <param name="conditionSql">指定条件(不含Where)</param>
    /// <param name="selectFields">要返回的字段，为空则返回所有字段</param>
    /// <param name="orderBy">排序方式(不包含order by)</param>
    /// <param name="pageSize">分页大小--每页包含的记录数</param>
    /// <param name="pageIndex">页码--要获取第几页的记录</param>
    /// <param name="parms">执行SQL语句使用的参数</param>
    /// <returns>按分页大小返回指定页码的记录的指定字段</returns>
    public DataTable GetListMSSQL2005_JOIN(string joinSql,bool throwSql,ref int totalCount, string conditionSql, string selectFields, string orderBy, int pageSize, int pageIndex, params SqlParameter[] parms)
    {
    return dal.GetListMSSQL2005_JOIN(joinSql,throwSql,ref totalCount, conditionSql, selectFields, orderBy, pageSize, pageIndex, parms);
    }
    /// <summary>
    /// 分页返回符合条件的记录，可指定返回字段
    /// 注意：仅MSSQL2000及以上可用
    /// </summary>
    /// <param name="joinSql">join语句，如inner Join B on GP_Mails.fdMailID=B.fdMailID</param>
    /// <param name="throwSql">将SQL语句作为异常抛出，用于检测语句正确性</param>
    /// <param name="totalCount">返回所有记录数,如果输入大于0,则不重新计算</param>
    /// <param name="conditionSql">指定条件(不含Where)</param>
    /// <param name="selectFields">要返回的字段，为空则返回所有字段</param>
    /// <param name="orderBy">排序方式(不包含order by)</param>
    /// <param name="pageSize">分页大小--每页包含的记录数</param>
    /// <param name="pageIndex">页码--要获取第几页的记录</param>
    /// <param name="parms">执行SQL语句使用的参数</param>
    /// <returns>按分页大小返回指定页码的记录的指定字段</returns>
    public IList<DDStars_Model> GetModelListMSSQL2005_JOIN(string joinSql,bool throwSql,ref int totalCount, string conditionSql, string selectFields, string orderBy, int pageSize, int pageIndex, params SqlParameter[] parms)
    {
    return dal.GetModelListMSSQL2005_JOIN(joinSql,throwSql,ref totalCount, conditionSql, selectFields, orderBy, pageSize, pageIndex, parms);
    }
    /// <summary>
    /// 分页返回符合条件的记录，可指定返回字段
    /// 注意：仅MSSQL2005及以上可用
    /// </summary>
    /// <param name="totalCount">返回所有记录数,如果输入大于0,则不重新计算</param>
    /// <param name="conditionSql">指定条件(不含Where)</param>
    /// <param name="selectFields">要返回的字段，为空则返回所有字段</param>
    /// <param name="orderBy">排序方式(不包含order by)</param>
    /// <param name="pageSize">分页大小--每页包含的记录数</param>
    /// <param name="pageIndex">页码--要获取第几页的记录</param>
    /// <param name="pageRecordOffset">分页时的偏移值（前面pageRecordOffset条使用其他方式或其他条件取出，小于0则表示最前面pageRecordOffset条记录被忽略）</param>
    /// <param name="parms">执行SQL语句使用的参数</param>
    /// <returns>按分页大小返回指定页码的记录的指定字段</returns>
    public DataTable GetListMSSQL2005(ref int totalCount, string conditionSql, string selectFields, string orderBy, int pageSize, int pageIndex, int pageRecordOffset, params SqlParameter[] parms)
    {
        return dal.GetListMSSQL2005(ref totalCount, conditionSql, selectFields, orderBy, pageSize, pageIndex, pageRecordOffset, parms);
    }
    /// <summary>
    /// 分页返回符合条件的记录，可指定返回字段
    /// MSSQL2005及以上可用
    /// </summary>
    /// <param name="conditionSql">指定条件(不含Where)</param>
    /// <param name="selectFields">要返回的字段，为空则返回所有字段</param>
    /// <param name="orderBy">排序方式(不包含order by)</param>
    /// <param name="pageSize">分页大小--每页包含的记录数</param>
    /// <param name="pageIndex">页码--要获取第几页的记录</param>
    /// <param name="pageRecordOffset">分页时的偏移值（前面pageRecordOffset条使用其他方式或其他条件取出，小于0则表示最前面pageRecordOffset条记录被忽略）</param>
    /// <param name="parms">执行SQL语句使用的参数</param>
    /// <returns>按分页大小返回指定页码的记录的指定字段</returns>
    public SqlDataReader GetListMSSQL2005(string conditionSql, string selectFields, string orderBy, int pageSize, int pageIndex, int pageRecordOffset, params SqlParameter[] parms)
    {
        return dal.GetListMSSQL2005(conditionSql, selectFields, orderBy, pageSize, pageIndex, pageRecordOffset,  parms);
    }
    /// <summary>
    /// 分页返回符合条件的记录，可指定返回字段
    /// 注意：仅MSSQL2005及以上可用
    /// </summary>
    /// <param name="totalCount">返回所有记录数,如果输入大于0,则不重新计算</param>
    /// <param name="conditionSql">指定条件(不含Where)</param>
    /// <param name="selectFields">要返回的字段，为空则返回所有字段</param>
    /// <param name="orderBy">排序方式(不包含order by)</param>
    /// <param name="pageSize">分页大小--每页包含的记录数</param>
    /// <param name="pageIndex">页码--要获取第几页的记录</param>
    /// <param name="pageRecordOffset">分页时的偏移值（前面pageRecordOffset条使用其他方式或其他条件取出，小于0则表示最前面pageRecordOffset条记录被忽略）</param>
    /// <param name="parms">执行SQL语句使用的参数</param>
    /// <returns>按分页大小返回指定页码的记录的指定字段</returns>
    public IList<DDStars_Model> GetModelListMSSQL2005(ref int totalCount, string conditionSql, string selectFields, string orderBy, int pageSize, int pageIndex, int pageRecordOffset, params SqlParameter[] parms)
    {
        return dal.GetModelListMSSQL2005(ref totalCount, conditionSql, selectFields, orderBy, pageSize, pageIndex, pageRecordOffset, parms);
    }
    /// <summary>
    /// 分页返回符合条件的记录
    /// 注意：仅MSSQL2005及以上可用
    /// </summary>
    /// <param name="conditionSql">指定条件(不含Where)</param>
    /// <param name="selectFields">要返回的字段，为空则返回所有字段</param>
    /// <param name="orderBy">排序方式(不包含order by)</param>
    /// <param name="pageSize">分页大小--每页包含的记录数</param>
    /// <param name="pageIndex">页码--要获取第几页的记录</param>
    /// <param name="pageRecordOffset">分页时的偏移值（前面pageRecordOffset条使用其他方式或其他条件取出，小于0则表示最前面pageRecordOffset条记录被忽略）</param>
    /// <param name="parms">执行SQL语句使用的参数</param>
    /// <returns>按分页大小返回指定页码的记录的指定字段</returns>
    public IList<DDStars_Model> GetModelListMSSQL2005(string conditionSql, string orderBy, int pageSize, int pageIndex, int pageRecordOffset, params SqlParameter[] parms)
    {
        return dal.GetModelListMSSQL2005(conditionSql, orderBy, pageSize, pageIndex, pageRecordOffset, parms);
    }
    /// <summary>
    /// 分页返回符合条件的记录，可指定返回字段
    /// 注意：仅MSSQL2005及以上可用
    /// </summary>
    /// <param name="joinSql">join语句，如inner Join B on GP_Mails.fdMailID=B.fdMailID</param>
    /// <param name="throwSql">将SQL语句作为异常抛出，用于检测语句正确性</param>
    /// <param name="totalCount">返回所有记录数,如果输入大于0,则不重新计算</param>
    /// <param name="conditionSql">指定条件(不含Where)</param>
    /// <param name="selectFields">要返回的字段，为空则返回所有字段</param>
    /// <param name="orderBy">排序方式(不包含order by)</param>
    /// <param name="pageSize">分页大小--每页包含的记录数</param>
    /// <param name="pageIndex">页码--要获取第几页的记录</param>
    /// <param name="pageRecordOffset">分页时的偏移值（前面pageRecordOffset条使用其他方式或其他条件取出，小于0则表示最前面pageRecordOffset条记录被忽略）</param>
    /// <param name="parms">执行SQL语句使用的参数</param>
    /// <returns>按分页大小返回指定页码的记录的指定字段</returns>
    public DataTable GetListMSSQL2005_JOIN(string joinSql,bool throwSql,ref int totalCount, string conditionSql, string selectFields, string orderBy, int pageSize, int pageIndex, int pageRecordOffset, params SqlParameter[] parms)
    {
       return dal.GetListMSSQL2005_JOIN(joinSql,throwSql,ref totalCount, conditionSql, selectFields, orderBy, pageSize, pageIndex, pageRecordOffset, parms);
    }
    /// <summary>
    /// 分页返回符合条件的记录，可指定返回字段
    /// 注意：仅MSSQL2000及以上可用
    /// </summary>
    /// <param name="joinSql">join语句，如inner Join B on GP_Mails.fdMailID=B.fdMailID</param>
    /// <param name="throwSql">将SQL语句作为异常抛出，用于检测语句正确性</param>
    /// <param name="totalCount">返回所有记录数,如果输入大于0,则不重新计算</param>
    /// <param name="conditionSql">指定条件(不含Where)</param>
    /// <param name="selectFields">要返回的字段，为空则返回所有字段</param>
    /// <param name="orderBy">排序方式(不包含order by)</param>
    /// <param name="pageSize">分页大小--每页包含的记录数</param>
    /// <param name="pageIndex">页码--要获取第几页的记录</param>
    /// <param name="pageRecordOffset">分页时的偏移值（前面pageRecordOffset条使用其他方式或其他条件取出，小于0则表示最前面pageRecordOffset条记录被忽略）</param>
    /// <param name="parms">执行SQL语句使用的参数</param>
    /// <returns>按分页大小返回指定页码的记录的指定字段</returns>
    public IList<DDStars_Model> GetModelListMSSQL2005_JOIN(string joinSql,bool throwSql,ref int totalCount, string conditionSql, string selectFields, string orderBy, int pageSize, int pageIndex, int pageRecordOffset, params SqlParameter[] parms)
    {
        return dal.GetModelListMSSQL2005_JOIN(joinSql,throwSql,ref totalCount, conditionSql, selectFields, orderBy, pageSize, pageIndex, pageRecordOffset, parms);
    }
    /// <summary>
    /// 分页返回符合条件的记录，可指定返回字段
    /// MSSQL2000及以上可用
    /// </summary>
    /// <param name="totalCount">返回所有记录数,如果输入大于0,则不重新计算</param>
    /// <param name="conditionSql">指定条件(不含Where)</param>
    /// <param name="selectFields">要返回的字段，为空则返回所有字段</param>
    /// <param name="orderBy">排序方式(不包含order by)</param>
    /// <param name="pageSize">分页大小--每页包含的记录数</param>
    /// <param name="pageIndex">页码--要获取第几页的记录</param>
    /// <param name="parms">执行SQL语句使用的参数</param>
    /// <returns>按分页大小返回指定页码的记录的指定字段</returns>
    public DataTable GetListMSSQL2000(ref int totalCount, string conditionSql, string selectFields, string orderBy, int pageSize, int pageIndex, params SqlParameter[] parms)
    {
    return dal.GetListMSSQL2000(ref totalCount, conditionSql, selectFields, orderBy, pageSize, pageIndex, parms);
    }
    /// <summary>
    /// 分页返回符合条件的记录，可指定返回字段
    /// 注意：仅MSSQL2005及以上可用
    /// </summary>
    /// <param name="totalCount">返回所有记录数,如果输入大于0,则不重新计算</param>
    /// <param name="conditionSql">指定条件(不含Where)</param>
    /// <param name="selectFields">要返回的字段，为空则返回所有字段</param>
    /// <param name="orderBy">排序方式(不包含order by)</param>
    /// <param name="pageSize">分页大小--每页包含的记录数</param>
    /// <param name="pageIndex">页码--要获取第几页的记录</param>
    /// <param name="parms">执行SQL语句使用的参数</param>
    /// <returns>按分页大小返回指定页码的记录的指定字段</returns>
    public IList<DDStars_Model> GetModelListMSSQL2000(ref int totalCount, string conditionSql, string selectFields, string orderBy, int pageSize, int pageIndex, params SqlParameter[] parms)
    {
    return dal.GetModelListMSSQL2000(ref totalCount, conditionSql, selectFields, orderBy, pageSize, pageIndex, parms);
    }
    /// <summary>
    /// 分页返回符合条件的记录，可指定返回字段
    /// MSSQL2000及以上可用
    /// </summary>
    /// <param name="conditionSql">指定条件(不含Where)</param>
    /// <param name="selectFields">要返回的字段，为空则返回所有字段</param>
    /// <param name="orderBy">排序方式(不包含order by)</param>
    /// <param name="pageSize">分页大小--每页包含的记录数</param>
    /// <param name="pageIndex">页码--要获取第几页的记录</param>
    /// <param name="parms">执行SQL语句使用的参数</param>
    /// <returns>按分页大小返回指定页码的记录的指定字段</returns>
    public SqlDataReader GetListMSSQL2000(string conditionSql, string selectFields, string orderBy, int pageSize, int pageIndex, params SqlParameter[] parms)
    {
    return dal.GetListMSSQL2000(conditionSql, selectFields, orderBy, pageSize, pageIndex, parms);
    }
    /// <summary>
    /// 分页返回符合条件的记录
    /// MSSQL2000及以上可用
    /// </summary>
    /// <param name="conditionSql">指定条件(不含Where)</param>
    /// <param name="selectFields">要返回的字段，为空则返回所有字段</param>
    /// <param name="orderBy">排序方式(不包含order by)</param>
    /// <param name="pageSize">分页大小--每页包含的记录数</param>
    /// <param name="pageIndex">页码--要获取第几页的记录</param>
    /// <param name="parms">执行SQL语句使用的参数</param>
    /// <returns>按分页大小返回指定页码的记录的指定字段</returns>
    public IList<DDStars_Model> GetModelListMSSQL2000(string conditionSql, string orderBy, int pageSize, int pageIndex, params SqlParameter[] parms)
    {
    return dal.GetModelListMSSQL2000(conditionSql, orderBy, pageSize, pageIndex, parms);
    }
    /// <summary>
    /// 分页返回符合条件的记录，可指定返回字段
    /// MSSQL2000及以上可用
    /// </summary>
    /// <param name="joinSql">join语句，如inner Join B on GP_Mails.fdMailID=B.fdMailID</param>
    /// <param name="throwSql">将SQL语句作为异常抛出，用于检测语句正确性</param>
    /// <param name="totalCount">返回所有记录数,如果输入大于0,则不重新计算</param>
    /// <param name="conditionSql">指定条件(不含Where)</param>
    /// <param name="selectFields">要返回的字段，为空则返回所有字段</param>
    /// <param name="orderBy">排序方式(不包含order by)</param>
    /// <param name="pageSize">分页大小--每页包含的记录数</param>
    /// <param name="pageIndex">页码--要获取第几页的记录</param>
    /// <param name="parms">执行SQL语句使用的参数</param>
    /// <returns>按分页大小返回指定页码的记录的指定字段</returns>
    public DataTable GetListMSSQL2000_JOIN(string joinSql,bool throwSql,ref int totalCount, string conditionSql, string selectFields, string orderBy, int pageSize, int pageIndex, params SqlParameter[] parms)
    {
    return dal.GetListMSSQL2000_JOIN(joinSql,throwSql,ref totalCount, conditionSql, selectFields, orderBy, pageSize, pageIndex, parms);
    }
    /// <summary>
    /// 分页返回符合条件的记录，可指定返回字段
    /// MSSQL2000及以上可用
    /// </summary>
    /// <param name="joinSql">join语句，如inner Join B on GP_Mails.fdMailID=B.fdMailID</param>
    /// <param name="throwSql">将SQL语句作为异常抛出，用于检测语句正确性</param>
    /// <param name="totalCount">返回所有记录数,如果输入大于0,则不重新计算</param>
    /// <param name="conditionSql">指定条件(不含Where)</param>
    /// <param name="selectFields">要返回的字段，为空则返回所有字段</param>
    /// <param name="orderBy">排序方式(不包含order by)</param>
    /// <param name="pageSize">分页大小--每页包含的记录数</param>
    /// <param name="pageIndex">页码--要获取第几页的记录</param>
    /// <param name="parms">执行SQL语句使用的参数</param>
    /// <returns>按分页大小返回指定页码的记录的指定字段</returns>
    public IList<DDStars_Model> GetModelListMSSQL2000_JOIN(string joinSql,bool throwSql,ref int totalCount, string conditionSql, string selectFields, string orderBy, int pageSize, int pageIndex, params SqlParameter[] parms)
    {
    return dal.GetModelListMSSQL2000_JOIN(joinSql,throwSql,ref totalCount, conditionSql, selectFields, orderBy, pageSize, pageIndex, parms);
    }
    /// <summary>
    /// 分页返回符合条件的记录，可指定返回字段
    /// MSSQL2000及以上可用
    /// </summary>
    /// <param name="totalCount">返回所有记录数,如果输入大于0,则不重新计算</param>
    /// <param name="conditionSql">指定条件(不含Where)</param>
    /// <param name="selectFields">要返回的字段，为空则返回所有字段</param>
    /// <param name="orderBy">排序方式(不包含order by)</param>
    /// <param name="pageSize">分页大小--每页包含的记录数</param>
    /// <param name="pageIndex">页码--要获取第几页的记录</param>
    /// <param name="pageRecordOffset">分页时的偏移值（前面pageRecordOffset条使用其他方式或其他条件取出，小于0则表示最前面pageRecordOffset条记录被忽略）</param>
    /// <param name="parms">执行SQL语句使用的参数</param>
    /// <returns>按分页大小返回指定页码的记录的指定字段</returns>
    public DataTable GetListMSSQL2000(ref int totalCount, string conditionSql, string selectFields, string orderBy, int pageSize, int pageIndex, int pageRecordOffset, params SqlParameter[] parms)
    {
    return dal.GetListMSSQL2000(ref totalCount, conditionSql, selectFields, orderBy, pageSize, pageIndex, pageRecordOffset, parms);
    }
    /// <summary>
    /// 分页返回符合条件的记录，可指定返回字段
    /// 注意：仅MSSQL2005及以上可用
    /// </summary>
    /// <param name="totalCount">返回所有记录数,如果输入大于0,则不重新计算</param>
    /// <param name="conditionSql">指定条件(不含Where)</param>
    /// <param name="selectFields">要返回的字段，为空则返回所有字段</param>
    /// <param name="orderBy">排序方式(不包含order by)</param>
    /// <param name="pageSize">分页大小--每页包含的记录数</param>
    /// <param name="pageIndex">页码--要获取第几页的记录</param>
    /// <param name="pageRecordOffset">分页时的偏移值（前面pageRecordOffset条使用其他方式或其他条件取出，小于0则表示最前面pageRecordOffset条记录被忽略）</param>
    /// <param name="parms">执行SQL语句使用的参数</param>
    /// <returns>按分页大小返回指定页码的记录的指定字段</returns>
    public IList<DDStars_Model> GetModelListMSSQL2000(ref int totalCount, string conditionSql, string selectFields, string orderBy, int pageSize, int pageIndex, int pageRecordOffset, params SqlParameter[] parms)
    {
    return dal.GetModelListMSSQL2000(ref totalCount, conditionSql, selectFields, orderBy, pageSize, pageIndex, pageRecordOffset, parms);
    }
    /// <summary>
    /// 分页返回符合条件的记录，可指定返回字段
    /// MSSQL2000及以上可用
    /// </summary>
    /// <param name="conditionSql">指定条件(不含Where)</param>
    /// <param name="selectFields">要返回的字段，为空则返回所有字段</param>
    /// <param name="orderBy">排序方式(不包含order by)</param>
    /// <param name="pageSize">分页大小--每页包含的记录数</param>
    /// <param name="pageIndex">页码--要获取第几页的记录</param>
    /// <param name="pageRecordOffset">分页时的偏移值（前面pageRecordOffset条使用其他方式或其他条件取出，小于0则表示最前面pageRecordOffset条记录被忽略）</param>
    /// <param name="parms">执行SQL语句使用的参数</param>
    /// <returns>按分页大小返回指定页码的记录的指定字段</returns>
    public SqlDataReader GetListMSSQL2000(string conditionSql, string selectFields, string orderBy, int pageSize, int pageIndex, int pageRecordOffset, params SqlParameter[] parms)
    {
    return dal.GetListMSSQL2000(conditionSql, selectFields, orderBy, pageSize, pageIndex, pageRecordOffset, parms);
    }
    /// <summary>
    /// 分页返回符合条件的记录
    /// MSSQL2000及以上可用
    /// </summary>
    /// <param name="conditionSql">指定条件(不含Where)</param>
    /// <param name="selectFields">要返回的字段，为空则返回所有字段</param>
    /// <param name="orderBy">排序方式(不包含order by)</param>
    /// <param name="pageSize">分页大小--每页包含的记录数</param>
    /// <param name="pageIndex">页码--要获取第几页的记录</param>
    /// <param name="pageRecordOffset">分页时的偏移值（前面pageRecordOffset条使用其他方式或其他条件取出，小于0则表示最前面pageRecordOffset条记录被忽略）</param>
    /// <param name="parms">执行SQL语句使用的参数</param>
    /// <returns>按分页大小返回指定页码的记录的指定字段</returns>
    public IList<DDStars_Model> GetModelListMSSQL2000(string conditionSql, string orderBy, int pageSize, int pageIndex, int pageRecordOffset, params SqlParameter[] parms)
    {
    return dal.GetModelListMSSQL2000(conditionSql, orderBy, pageSize, pageIndex, pageRecordOffset, parms);
    }
    /// <summary>
    /// 分页返回符合条件的记录，可指定返回字段
    /// MSSQL2000及以上可用
    /// </summary>
    /// <param name="joinSql">join语句，如inner Join B on GP_Mails.fdMailID=B.fdMailID</param>
    /// <param name="throwSql">将SQL语句作为异常抛出，用于检测语句正确性</param>
    /// <param name="totalCount">返回所有记录数,如果输入大于0,则不重新计算</param>
    /// <param name="conditionSql">指定条件(不含Where)</param>
    /// <param name="selectFields">要返回的字段，为空则返回所有字段</param>
    /// <param name="orderBy">排序方式(不包含order by)</param>
    /// <param name="pageSize">分页大小--每页包含的记录数</param>
    /// <param name="pageIndex">页码--要获取第几页的记录</param>
    /// <param name="pageRecordOffset">分页时的偏移值（前面pageRecordOffset条使用其他方式或其他条件取出，小于0则表示最前面pageRecordOffset条记录被忽略）</param>
    /// <param name="parms">执行SQL语句使用的参数</param>
    /// <returns>按分页大小返回指定页码的记录的指定字段</returns>
    public DataTable GetListMSSQL2000_JOIN(string joinSql,bool throwSql,ref int totalCount, string conditionSql, string selectFields, string orderBy, int pageSize, int pageIndex, int pageRecordOffset, params SqlParameter[] parms)
    {
    return dal.GetListMSSQL2000_JOIN(joinSql,throwSql,ref totalCount, conditionSql, selectFields, orderBy, pageSize, pageIndex, pageRecordOffset, parms);
    }
    /// <summary>
    /// 分页返回符合条件的记录，可指定返回字段
    /// MSSQL2000及以上可用
    /// </summary>
    /// <param name="joinSql">join语句，如inner Join B on GP_Mails.fdMailID=B.fdMailID</param>
    /// <param name="throwSql">将SQL语句作为异常抛出，用于检测语句正确性</param>
    /// <param name="totalCount">返回所有记录数,如果输入大于0,则不重新计算</param>
    /// <param name="conditionSql">指定条件(不含Where)</param>
    /// <param name="selectFields">要返回的字段，为空则返回所有字段</param>
    /// <param name="orderBy">排序方式(不包含order by)</param>
    /// <param name="pageSize">分页大小--每页包含的记录数</param>
    /// <param name="pageIndex">页码--要获取第几页的记录</param>
    /// <param name="pageRecordOffset">分页时的偏移值（前面pageRecordOffset条使用其他方式或其他条件取出，小于0则表示最前面pageRecordOffset条记录被忽略）</param>
    /// <param name="parms">执行SQL语句使用的参数</param>
    /// <returns>按分页大小返回指定页码的记录的指定字段</returns>
    public IList<DDStars_Model> GetModelListMSSQL2000_JOIN(string joinSql,bool throwSql,ref int totalCount, string conditionSql, string selectFields, string orderBy, int pageSize, int pageIndex, int pageRecordOffset, params SqlParameter[] parms)
    {
    return dal.GetModelListMSSQL2000_JOIN(joinSql,throwSql,ref totalCount, conditionSql, selectFields, orderBy, pageSize, pageIndex, pageRecordOffset, parms);
    }
    }
    }
  
