﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Log
{
    public class LogHelper
    {
        static readonly string logBasePath = string.Format("{0}\\Log\\", System.AppDomain.CurrentDomain.BaseDirectory);
        //错误日志路径
        private static readonly string logErrorPath = string.Format("{0}LogError.log", logBasePath);
        //警告日志路径
        private static readonly string logWarnPath = string.Format("{0}LogWarn.log", logBasePath);
        //记录日常日志
        private static readonly string logRecordPath = string.Format("{0}LogRecord.log", logBasePath);

        static object _lockError = new object(), _lockWarn = new object(), _lockRecord = new object();
        static LogHelper()
        {
            if (!Directory.Exists(logBasePath))
                Directory.CreateDirectory(logBasePath);
            if (!File.Exists(logErrorPath))
                File.Create(logErrorPath).Close();
            if (!File.Exists(logWarnPath))
                File.Create(logWarnPath).Close();
        }

        public static void WriteErrorLog(string content)
        {
            lock (_lockError)
            {
                try
                {
                    content = "时间：" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "\r\n" + content;
                    FileHelper.WriteText(logErrorPath, content + "\r\n\r\n", true);
                }
                catch
                {
                }
            }
        }

        public static void WriteWarnLog(string content)
        {
            lock (_lockWarn)
            {
                try
                {
                    content = "时间：" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "\r\n" + content;
                    FileHelper.WriteText(logWarnPath, content + "\r\n\r\n", true);
                }
                catch
                {
                }
            }
        }
        public static void WriteRecordLog(string content)
        {
            lock (_lockRecord)
            {
                try
                {
                    content = "时间：" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "\r\n" + content;
                    FileHelper.WriteText(logRecordPath, content + "\r\n\r\n", true);
                }
                catch
                {
                }
            }
        }
    }

    public class FileHelper
    {
        /// <summary>
        /// 读取文本文件全部内容
        /// </summary>
        /// <param name="fileName">文件绝对路径</param>
        public static string ReadText(string fileName)
        {
            return ReadText(fileName, Encoding.Default);
        }

        /// <summary>
        /// 读取文本文件全部内容
        /// </summary>
        /// <param name="fileName">文件绝对路径</param>
        /// <param name="coding">编码</param>
        /// <returns></returns>
        public static string ReadText(string fileName, Encoding coding)
        {
            using (StreamReader sr = new StreamReader(fileName, coding))
            {
                return sr.ReadToEnd();
            }
        }

        /// <summary>
        /// 向文本文件写入文本内容
        /// </summary>
        /// <param name="fileName">文件绝对路径</param>
        /// <param name="text">要写入的内容</param>
        /// <param name="append">是否添加文本,false表示重写文件</param>
        public static void WriteText(string fileName, string text, bool append)
        {
            WriteText(fileName, text, append, Encoding.Default);
        }

        /// <summary>
        /// 向文本文件写入文本内容
        /// </summary>
        /// <param name="fileName">文件绝对路径</param>
        /// <param name="text">要写入的内容</param>
        /// <param name="append">是否添加文本,false表示重写文件</param>
        /// <param name="coding">编码</param>
        public static void WriteText(string fileName, string text, bool append, Encoding coding)
        {
            FileInfo info = new FileInfo(fileName);
            if (info.Length > 104857600)
            {
                File.Copy(fileName, string.Format("{0}{1}.log", info.DirectoryName + "\\", Guid.NewGuid()));
                File.Create(fileName).Close();
            }
            using (StreamWriter sw = new StreamWriter(fileName, append, coding))
            {
                sw.WriteLine(text);
            }
        }
    }
}
