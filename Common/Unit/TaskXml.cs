﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Xml;

namespace Common.Unit
{

    /// <summary>
    /// 读取配置文件
    /// </summary>
    public static class TaskXml
    {
        public static IList<QuertzXml> allQuertzXml = null;
        static TaskXml()
        {
            allQuertzXml = WebMenuXMLParse.GetWebMenu("QuertzXml");
        }
    }

    public class QuertzXml
    {
        /// <summary>
        /// 唯一ID
        /// </summary>
        public string TaskID { get; set; }
        /// <summary>
        /// 任务名称
        /// </summary>
        public string TaskName { get; set; }
        /// <summary>
        /// 参数（固定值）
        /// </summary>
        public string TaskParam { get; set; }
        /// <summary>
        /// Quertz定时规则
        /// </summary>
        public string CronExpressionString { get; set; }
        /// <summary>
        /// 开关量（1--开，0--关）
        /// </summary>
        public bool Status { get; set; }
        /// <summary>
        /// 任务创建时间
        /// </summary>
        public string CreatedTime { get; set; }
        /// <summary>
        /// 任务备注
        /// </summary>
        public string CronRemark { get; set; }
        /// <summary>
        /// 命名空间
        /// </summary>
        public string AssemblyName { get; set; }
        /// <summary>
        /// 具体的方法名称
        /// </summary>
        public string ClassName { get; set; }
    }


    #region 解析菜单XML
    static class WebMenuXMLParse
    {
        internal static IList<QuertzXml> GetWebMenu(string menuNames)
        {
            string configPath = System.AppDomain.CurrentDomain.BaseDirectory + "/XmlConfig/{0}.xml";
            configPath = string.Format(configPath, menuNames);
            if (!File.Exists(configPath)) throw new FileNotFoundException("未找到配置文件");
            return Parse(configPath);
        }

        static IList<QuertzXml> Parse(string configPath)
        {
            IList<QuertzXml> result = new List<QuertzXml>();
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(configPath);
            //获取根节点
            XmlNode root = xmlDoc.SelectSingleNode("/Tasks");
            return RecursionGetMenusByXmlNode(root, null);
        }

        static IList<QuertzXml> RecursionGetMenusByXmlNode(XmlNode node, QuertzXml parent)
        {
            IList<QuertzXml> result = new List<QuertzXml>();
            foreach (XmlNode menuNode in node.SelectNodes("./Task"))
            {
                QuertzXml menu = GetMenuByNode(menuNode);
                result.Add(menu);
            }
            return result;
        }

        static QuertzXml GetMenuByNode(XmlNode node)
        {
            try
            {
                QuertzXml menu = new QuertzXml();
                menu.TaskID = node.Attributes["TaskID"]?.Value;
                menu.TaskName = node.Attributes["TaskName"]?.Value;
                menu.TaskParam = node.Attributes["TaskParam"]?.Value ?? "Index";
                menu.CronExpressionString = node.Attributes["CronExpressionString"]?.Value;
                menu.AssemblyName = node.Attributes["AssemblyName"]?.Value;
                menu.ClassName = node.Attributes["ClassName"]?.Value;
                menu.Status = bool.Parse(node.Attributes["Status"]?.Value);
                menu.CreatedTime = node.Attributes["CreatedTime"]?.Value;
                menu.CronRemark = node.Attributes["CronRemark"]?.Value;
                return menu;
            }
            catch (Exception)
            {
                return null;
            }
        }
    }

    #endregion
}
