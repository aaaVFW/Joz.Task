﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace Common.Unit
{
    public static class StringHelper
    {
        /// <summary>
        /// 特殊符号遮盖
        /// </summary>
        /// <param name="str">拟遮盖字符全字段</param>
        /// <param name="program_starBegin">开始位</param>
        /// <param name="program_starEnd">结束位</param>
        /// <param name="program_StarCharShow">遮盖组合符号</param>
        /// <returns></returns>
        public static string StrReplaceByStar(string str, int program_starBegin, int program_starEnd, string program_StarChar)
        {
            string NewStr = "";
            string a = "";
            string b = "";
            int starLen = program_starEnd - program_starBegin + 1;
            if (starLen <= 0)
            {
                return str;
            }
            string program_StarCharShow = StarCharShow(starLen, program_StarChar);
            if (str.Length <= program_starEnd)
            {
                if (str.Length < program_starBegin)
                {
                    a = str;
                }
                else
                {
                    a = str.Substring(0, program_starBegin - 1);
                }
            }
            else
            {
                a = str.Substring(0, program_starBegin - 1);
                b = str.Substring(program_starEnd, str.Length - program_starEnd);
            }
            NewStr = a + program_StarCharShow + b;
            return NewStr;
        }
        /// <summary>
        /// 通过长度生成遮盖字段如****
        /// </summary>
        /// <param name="program_StarLen">长度如4</param>
        /// <param name="program_StarChar">遮盖符号如*</param>
        /// <returns></returns>
        public static string StarCharShow(int program_StarLen, string program_StarChar)
        {
            string program_StarCharShow = "";
            for (int i = 0; i < program_StarLen; i++)
            {
                program_StarCharShow = program_StarCharShow + program_StarChar;
            }
            return program_StarCharShow;
        }
        /// <summary>
        /// 将整型数组拼接为字符串
        /// </summary>
        /// <param name="separator"></param>
        /// <param name="values"></param>
        /// <returns></returns>
        public static string Join(string separator, int[] values)
        {
            if (values.Length == 0) return "";
            StringBuilder str = new StringBuilder();
            foreach (int item in values)
                str.AppendFormat("{0}{1}", item, separator);
            str.Remove(str.Length - separator.Length, separator.Length);
            return str.ToString();
        }

        #region  截短字串的函数，分区中英文
        /// <summary>
        /// 截短字串的函数,区分中英文，一个汉字相当于两个英文字母
        /// 如截取40，表示截取40个英文字母或20个汉字
        /// 超出部分用...替换
        /// </summary>
        /// <param name="value">要加工的字串</param>
        /// <param name="length">长度</param>
        /// <returns>被加工过的字串</returns>
        public static string CutString(string value, int length)
        {
            return CutString(value, length, true, CutType.Varchar);
        }
        #endregion

        #region  截短字串的函数，不区分中英文
        /// <summary>
        /// 截短字串的函数,不区分中英文
        /// 如截取40，表示截取40个字符不分中英文
        /// 超出部分用...替换
        /// </summary>
        /// <param name="value">要加工的字串</param>
        /// <param name="length">长度</param>
        /// <returns>被加工过的字串</returns>
        public static string CutStringN(string value, int length)
        {
            return CutString(value, length, true, CutType.NVarchar);
        }
        #endregion

        #region 截取字符串私有方法
        /// <summary>
        /// 截取字符枚举值,Varchar--英文一个字节，中文两个字节，NVarchar--无论中英文都是两个字节
        /// </summary>
        private enum CutType
        {
            Varchar,
            NVarchar
        }
        /// <summary>
        /// 要截取的字节数
        /// </summary>
        /// <param name="value">输入的字符串</param>
        /// <param name="length">限定长度</param>
        /// <param name="ellipsis">是否需要省略号,true--需要，false--不需要</param>
        /// <param name="cuttype">截取类型</param>
        /// <returns>截取后的字符串，如果是NVarchar--则20个字节就会有10个字符，Varchar--20个字节会有>=10个字符</returns>
        private static string CutString(string value, int length, bool ellipsis, CutType cuttype)
        {
            if (string.IsNullOrEmpty(value))
                return string.Empty;
            value = value.Trim();
            if (value.Length == 0)
                return string.Empty;
            if (cuttype == CutType.NVarchar)
            {
                if (value.Length > length / 2)
                {
                    value = value.Substring(0, length / 2);
                    if (ellipsis)
                        return value + "...";
                }
            }
            else
            {
                string resultString = string.Empty;
                byte[] myByte = System.Text.Encoding.GetEncoding("gbk").GetBytes(value);
                if (myByte.Length > length)
                {
                    resultString = Encoding.GetEncoding("gbk").GetString(myByte, 0, length);
                    string lastChar = resultString.Substring(resultString.Length - 1, 1);
                    if (lastChar.Equals(value.Substring(resultString.Length - 1, 1)))
                    { value = resultString; }//如果截取后最后一个字符与原始输入字符串中同一位置的字符相等，则表示截取完成
                    else//如果不相等，则减去一个字节再截取
                    {
                        value = Encoding.GetEncoding("gbk").GetString(myByte, 0, length - 1);
                    }
                    if (ellipsis)
                        return value + "...";
                    return value;
                }
            }
            return value;
        }
        #endregion



        /// <summary>
        /// 将字符串转换成适合呈现的HTML或JS(对换行符和单双引号进行编码，避免语法错误)
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static string StringToHtml(string str)
        {
            return str.Replace("\"", "&quot;").Replace("'", "&#039;").Replace("\t", "").Replace("\r\n", "<br />").Replace("\r", "<br />").Replace("\n", "<br />");
        }

    }
}
