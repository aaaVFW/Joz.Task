﻿using System;
using System.Linq;
using System.Text.RegularExpressions;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace Common.Unit
{
    /// <summary>
    /// RequestHelper 的摘要说明
    /// </summary>
    public class RequestHelper
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="collection"></param>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <param name="acceptEmpty"> </param>
        /// <param name="htmlDecode"></param>
        /// <param name="pts"></param>
        /// <returns></returns>
        static bool TryGet(System.Collections.Specialized.NameValueCollection collection, string key, ref string value, bool acceptEmpty, bool htmlDecode, params string[] pts)
        {
            bool empty = false;
            string[] vs = collection.GetValues(key);
            if (vs == null) return false;
            foreach (string s in vs)
            {
                if (s.IsEmptyOrNull()) continue;
                string v = s;
                if (htmlDecode) v = HttpContext.Current.Server.HtmlDecode(v);
                if (isMatchAll(v, pts))
                {
                    value = v;
                    return true;
                }
                else if (v.IsEmptyOrNull() && acceptEmpty)
                {
                    value = string.Empty;
                    empty = true;
                }
            }
            return empty;
        }


        public static string[] TryGetQueryValues(HttpContext context, string key)
        {
            string[] vs = context.Request.QueryString.GetValues(key);
            if (vs == null) return new string[] { };
            return vs;
        }
        public static string[] TryGetFormValues(HttpContext context, string key)
        {
            string[] vs = context.Request.Form.GetValues(key);
            if (vs == null) return new string[] { };
            return vs;
        }

        public static string[] TryGetValues(HttpContext context, string key)
        {
            string[] vs = TryGetFormValues(context, key);
            string[] vs2 = TryGetQueryValues(context, key);
            if (vs == null && vs2 == null) return new string[] { };
            if (vs != null && vs2 != null) return vs.Concat(vs2).ToArray();
            return (vs != null) ? vs : vs2;
        }

        #region  String NotEmpty
        #region 返回String值
        /// <summary>
        /// 从Request.QueryString参数中检索具有指定名称的非Null或者Empty的字符串参数，如果没有符合的参数则返回指定的默认值
        /// </summary>
        /// <param name="key">要查找的参数名称</param>
        /// <param name="defaultValue">默认值，如果没有匹配的值则返回默认值</param>
        /// <returns>返回匹配的参数值，没有匹配则返回默认值</returns>
        public static string TryGetQueryStringNotEmpty(string key, string defaultValue)
        {
            string value = defaultValue;
            TryGetQueryStringNotEmpty(key, ref value);
            return value;
        }
        /// <summary>
        /// 从Request.QueryString参数中检索具有指定名称的非Null或者Empty的字符串参数，如果没有符合的参数则返回指定的默认值
        /// </summary>
        /// <param name="key">要查找的参数名称</param>
        /// <param name="defaultValue">默认值，如果没有匹配的值则返回默认值</param>
        /// <param name="htmlDecode">是否需要使用HtmlDecode方法解码</param>
        /// <returns>返回匹配的参数值，没有匹配则返回默认值</returns>
        public static string TryGetQueryStringNotEmpty(string key, string defaultValue, bool htmlDecode)
        {
            string value = defaultValue;
            TryGetQueryStringNotEmpty(key, ref value, htmlDecode);
            return value;
        }
        /// <summary>
        /// 从Request.Form参数中检索具有指定名称的非Null或者Empty的字符串参数，如果没有符合的参数则返回指定的默认值
        /// </summary>
        /// <param name="key">要查找的参数名称</param>
        /// <param name="defaultValue">默认值，如果没有匹配的值则返回默认值</param>
        /// <returns>返回匹配的参数值，没有匹配则返回默认值</returns>
        public static string TryGetFormStringNotEmpty(string key, string defaultValue)
        {
            string value = defaultValue;
            TryGetFormStringNotEmpty(key, ref value);
            return value;
        }
        /// <summary>
        /// 从Request.Form参数中检索具有指定名称的非Null或者Empty的字符串参数，如果没有符合的参数则返回指定的默认值
        /// </summary>
        /// <param name="key">要查找的参数名称</param>
        /// <param name="defaultValue">默认值，如果没有匹配的值则返回默认值</param>
        /// <param name="htmlDecode">是否需要使用HtmlDecode方法解码</param>
        /// <returns>返回匹配的参数值，没有匹配则返回默认值</returns>
        public static string TryGetFormStringNotEmpty(string key, string defaultValue, bool htmlDecode)
        {
            string value = defaultValue;
            TryGetFormStringNotEmpty(key, ref value, htmlDecode);
            return value;
        }
        /// <summary>
        /// 从Request参数中检索具有指定名称的非Null或者Empty的字符串参数，如果没有符合的参数则返回指定的默认值
        /// </summary>
        /// <param name="keys">要查找的参数名称，提过超过一个参数名则返回第一个匹配的参数</param>
        /// <param name="defaultValue">默认值，如果没有匹配的值则返回默认值</param>
        /// <param name="pts">要匹配的正则表达式（如果提供超过一个的表达式，则需要全部匹配，即多个表达式之间是“与”的关系）</param>
        /// <returns>返回匹配的参数值，没有匹配则返回默认值</returns>
        public static string TryGetRequestStringNotEmpty(string[] keys, string defaultValue, params string[] pts)
        {
            string value = defaultValue;
            TryGetRequestStringNotEmpty(keys, ref value, pts);
            return value;
        }
        /// <summary>
        /// 从Request参数中检索具有指定名称的非Null或者Empty的字符串参数，如果没有符合的参数则返回指定的默认值
        /// </summary>
        /// <param name="key">要查找的参数名称</param>
        /// <param name="defaultValue">默认值，如果没有匹配的值则返回默认值</param>
        /// <param name="pts">要匹配的正则表达式（如果提供超过一个的表达式，则需要全部匹配，即多个表达式之间是“与”的关系）</param>
        /// <returns>返回匹配的参数值，没有匹配则返回默认值</returns>
        public static string TryGetRequestStringNotEmpty(string key, string defaultValue, params string[] pts)
        {
            string value = defaultValue;
            TryGetRequestStringNotEmpty(key, ref value, pts);
            return value;
        }
        /// <summary>
        /// 从Request参数中检索具有指定名称的非Null或者Empty的字符串参数，如果没有符合的参数则返回指定的默认值
        /// </summary>
        /// <param name="key">要查找的参数名称</param>
        /// <param name="defaultValue">默认值，如果没有匹配的值则返回默认值</param>
        /// <param name="htmlDecode">是否需要使用HtmlDecode方法解码</param>
        /// <param name="pts">要匹配的正则表达式（如果提供超过一个的表达式，则需要全部匹配，即多个表达式之间是“与”的关系）</param>
        /// <returns>返回匹配的参数值，没有匹配则返回默认值</returns>
        public static string TryGetRequestStringNotEmpty(string key, string defaultValue, bool htmlDecode, params string[] pts)
        {
            string value = defaultValue;
            TryGetRequestStringNotEmpty(key, ref value, htmlDecode, pts);
            return value;
        }
        /// <summary>
        /// 从Request参数中检索具有指定名称的非Null或者Empty的字符串参数，如果没有符合的参数则返回指定的默认值
        /// </summary>
        /// <param name="keys">要查找的参数名称，提过超过一个参数名则返回第一个匹配的参数</param>
        /// <param name="defaultValue">默认值，如果没有匹配的值则返回默认值</param>
        /// <param name="htmlDecode">是否需要使用HtmlDecode方法解码</param>
        /// <param name="pts">要匹配的正则表达式（如果提供超过一个的表达式，则需要全部匹配，即多个表达式之间是“与”的关系）</param>
        /// <returns>返回匹配的参数值，没有匹配则返回默认值</returns>
        public static string TryGetRequestStringNotEmpty(string[] keys, string defaultValue, bool htmlDecode, params string[] pts)
        {
            string value = defaultValue;
            TryGetRequestStringNotEmpty(keys, ref value, htmlDecode, pts);
            return value;
        }
        #endregion
        #region String 返回Bool 当前 HttpContext
        /// <summary>
        /// 从Request.QueryString参数中检索具有指定名称的非Null或者Empty的字符串参数，保存到指定的引用参数中
        /// </summary>
        /// <param name="key">要查找的参数名称</param>
        /// <param name="value">引用参数，保存匹配的值</param>
        /// <returns>找到匹配的参数返回True，否则返回False</returns>
        public static bool TryGetQueryStringNotEmpty(string key, ref string value)
        {
            return TryGetQueryStringNotEmpty(HttpContext.Current, key, ref value);
        }
        /// <summary>
        /// 从Request.QueryString参数中检索具有指定名称的非Null或者Empty的字符串参数，保存到指定的引用参数中
        /// </summary>
        /// <param name="key">要查找的参数名称</param>
        /// <param name="value">引用参数，保存匹配的值</param>
        /// <param name="htmlDecode">是否需要使用HtmlDecode方法解码</param>
        /// <returns>找到匹配的参数返回True，否则返回False</returns>
        public static bool TryGetQueryStringNotEmpty(string key, ref string value, bool htmlDecode)
        {
            return TryGetQueryStringNotEmpty(HttpContext.Current, key, ref value, htmlDecode);
        }
        /// <summary>
        /// 从Request.Form参数中检索具有指定名称的非Null或者Empty的字符串参数，保存到指定的引用参数中
        /// </summary>
        /// <param name="key">要查找的参数名称</param>
        /// <param name="value">引用参数，保存匹配的值</param>
        /// <returns>找到匹配的参数返回True，否则返回False</returns>
        public static bool TryGetFormStringNotEmpty(string key, ref string value)
        {
            return TryGetFormStringNotEmpty(HttpContext.Current, key, ref value);
        }
        /// <summary>
        /// 从Request.Form参数中检索具有指定名称的非Null或者Empty的字符串参数，保存到指定的引用参数中
        /// </summary>
        /// <param name="key">要查找的参数名称</param>
        /// <param name="value">引用参数，保存匹配的值</param>
        /// <param name="htmlDecode">是否需要使用HtmlDecode方法解码</param>
        /// <returns>找到匹配的参数返回True，否则返回False</returns>
        public static bool TryGetFormStringNotEmpty(string key, ref string value, bool htmlDecode)
        {
            return TryGetFormStringNotEmpty(HttpContext.Current, key, ref value, htmlDecode);
        }
        /// <summary>
        /// 从Request参数中检索具有指定名称的非Null或者Empty的字符串参数，保存到指定的引用参数中
        /// </summary>
        /// <param name="keys">要查找的参数名称，提过超过一个参数名则返回第一个匹配的参数</param>
        /// <param name="value">引用参数，保存匹配的值</param>
        /// <param name="pts">要匹配的正则表达式（如果提供超过一个的表达式，则需要全部匹配，即多个表达式之间是“与”的关系）</param>
        /// <returns>找到匹配的参数返回True，否则返回False</returns>
        public static bool TryGetRequestStringNotEmpty(string[] keys, ref string value, params string[] pts)
        {
            return TryGetRequestStringNotEmpty(HttpContext.Current, keys, ref value, pts);
        }
        /// <summary>
        /// 从Request参数中检索具有指定名称的非Null或者Empty的字符串参数，保存到指定的引用参数中
        /// </summary>
        /// <param name="key">要查找的参数名称</param>
        /// <param name="value">引用参数，保存匹配的值</param>
        /// <param name="pts">要匹配的正则表达式（如果提供超过一个的表达式，则需要全部匹配，即多个表达式之间是“与”的关系）</param>
        /// <returns>找到匹配的参数返回True，否则返回False</returns>
        public static bool TryGetRequestStringNotEmpty(string key, ref string value, params string[] pts)
        {
            return TryGetRequestStringNotEmpty(HttpContext.Current, key, ref value, pts);
        }
        /// <summary>
        /// 从Request参数中检索具有指定名称的非Null或者Empty的字符串参数，保存到指定的引用参数中
        /// </summary>
        /// <param name="key">要查找的参数名称</param>
        /// <param name="value">引用参数，保存匹配的值</param>
        /// <param name="htmlDecode">是否需要使用HtmlDecode方法解码</param>
        /// <param name="pts">要匹配的正则表达式（如果提供超过一个的表达式，则需要全部匹配，即多个表达式之间是“与”的关系）</param>
        /// <returns>找到匹配的参数返回True，否则返回False</returns>
        public static bool TryGetRequestStringNotEmpty(string key, ref string value, bool htmlDecode, params string[] pts)
        {
            return TryGetRequestStringNotEmpty(HttpContext.Current, key, ref value, htmlDecode, pts);
        }
        /// <summary>
        /// 从Request参数中检索具有指定名称的非Null或者Empty的字符串参数，保存到指定的引用参数中
        /// </summary>
        /// <param name="keys">要查找的参数名称，提过超过一个参数名则返回第一个匹配的参数</param>
        /// <param name="value">引用参数，保存匹配的值</param>
        /// <param name="htmlDecode">是否需要使用HtmlDecode方法解码</param>
        /// <param name="pts">要匹配的正则表达式（如果提供超过一个的表达式，则需要全部匹配，即多个表达式之间是“与”的关系）</param>
        /// <returns>找到匹配的参数返回True，否则返回False</returns>
        public static bool TryGetRequestStringNotEmpty(string[] keys, ref string value, bool htmlDecode, params string[] pts)
        {
            return TryGetRequestStringNotEmpty(HttpContext.Current, keys, ref value, htmlDecode, pts);
        }
        #endregion
        #region 返回Bool 指定 HttpContext
        /// <summary>
        /// 从Request.QueryString参数中检索具有指定名称的非Null或者Empty的字符串参数，保存到指定的引用参数中
        /// </summary>
        /// <param name="context">指定HttpContext上下文</param>
        /// <param name="key">要查找的参数名称</param>
        /// <param name="value">引用参数，保存匹配的值</param>
        /// <returns>找到匹配的参数返回True，否则返回False</returns>
        public static bool TryGetQueryStringNotEmpty(HttpContext context, string key, ref string value)
        {
            return TryGetQueryStringNotEmpty(context, key, ref value, true);
        }
        /// <summary>
        /// 从Request.QueryString参数中检索具有指定名称的非Null或者Empty的字符串参数，保存到指定的引用参数中
        /// </summary>
        /// <param name="context">指定HttpContext上下文</param>
        /// <param name="key">要查找的参数名称</param>
        /// <param name="value">引用参数，保存匹配的值</param>
        /// <param name="htmlDecode">是否需要使用HtmlDecode方法解码</param>
        /// <returns>找到匹配的参数返回True，否则返回False</returns>
        public static bool TryGetQueryStringNotEmpty(HttpContext context, string key, ref string value, bool htmlDecode)
        {
            return TryGet(context.Request.QueryString, key, ref value, false, htmlDecode);
        }
        /// <summary>
        /// 从Request.Form参数中检索具有指定名称的非Null或者Empty的字符串参数，保存到指定的引用参数中
        /// </summary>
        /// <param name="context">指定HttpContext上下文</param>
        /// <param name="key">要查找的参数名称</param>
        /// <param name="value">引用参数，保存匹配的值</param>
        /// <returns>找到匹配的参数返回True，否则返回False</returns>
        public static bool TryGetFormStringNotEmpty(HttpContext context, string key, ref string value)
        {
            return TryGetFormStringNotEmpty(context, key, ref value, true);
        }
        /// <summary>
        /// 从Request.Form参数中检索具有指定名称的非Null或者Empty的字符串参数，保存到指定的引用参数中
        /// </summary>
        /// <param name="context">指定HttpContext上下文</param>
        /// <param name="key">要查找的参数名称</param>
        /// <param name="value">引用参数，保存匹配的值</param>
        /// <param name="htmlDecode">是否需要使用HtmlDecode方法解码</param>
        /// <returns>找到匹配的参数返回True，否则返回False</returns>
        public static bool TryGetFormStringNotEmpty(HttpContext context, string key, ref string value, bool htmlDecode)
        {
            return TryGet(context.Request.Form, key, ref value, false, htmlDecode);
        }
        /// <summary>
        /// 从Request参数中检索具有指定名称的非Null或者Empty的字符串参数，保存到指定的引用参数中
        /// </summary>
        /// <param name="context">指定HttpContext上下文</param>
        /// <param name="keys">要查找的参数名称，提过超过一个参数名则返回第一个匹配的参数</param>
        /// <param name="value">引用参数，保存匹配的值</param>
        /// <param name="pts">要匹配的正则表达式（如果提供超过一个的表达式，则需要全部匹配，即多个表达式之间是“与”的关系）</param>
        /// <returns>找到匹配的参数返回True，否则返回False</returns>
        public static bool TryGetRequestStringNotEmpty(HttpContext context, string[] keys, ref string value, params string[] pts)
        {
            return TryGetRequestStringNotEmpty(context, keys, ref value, true, pts);
        }
        /// <summary>
        /// 从Request参数中检索具有指定名称的非Null或者Empty的字符串参数，保存到指定的引用参数中
        /// </summary>
        /// <param name="context">指定HttpContext上下文</param>
        /// <param name="key">要查找的参数名称</param>
        /// <param name="value">引用参数，保存匹配的值</param>
        /// <param name="pts">要匹配的正则表达式（如果提供超过一个的表达式，则需要全部匹配，即多个表达式之间是“与”的关系）</param>
        /// <returns>找到匹配的参数返回True，否则返回False</returns>
        public static bool TryGetRequestStringNotEmpty(HttpContext context, string key, ref string value, params string[] pts)
        {
            return TryGetRequestStringNotEmpty(context, key, ref value, true, pts);
        }
        /// <summary>
        /// 从Request参数中检索具有指定名称的非Null或者Empty的字符串参数，保存到指定的引用参数中
        /// </summary>
        /// <param name="context">指定HttpContext上下文</param>
        /// <param name="key">要查找的参数名称</param>
        /// <param name="value">引用参数，保存匹配的值</param>
        /// <param name="htmlDecode">是否需要使用HtmlDecode方法解码</param>
        /// <param name="pts">要匹配的正则表达式（如果提供超过一个的表达式，则需要全部匹配，即多个表达式之间是“与”的关系）</param>
        /// <returns>找到匹配的参数返回True，否则返回False</returns>
        public static bool TryGetRequestStringNotEmpty(HttpContext context, string key, ref string value, bool htmlDecode, params string[] pts)
        {
            if (TryGet(context.Request.Form, key, ref value, false, htmlDecode, pts)) return true;
            if (TryGet(context.Request.QueryString, key, ref value, false, htmlDecode, pts)) return true;
            return false;
        }
        /// <summary>
        /// 从Request参数中检索具有指定名称的非Null或者Empty的字符串参数，保存到指定的引用参数中
        /// </summary>
        /// <param name="context">指定HttpContext上下文</param>
        /// <param name="keys">要查找的参数名称，提过超过一个参数名则返回第一个匹配的参数</param>
        /// <param name="value">引用参数，保存匹配的值</param>
        /// <param name="htmlDecode">是否需要使用HtmlDecode方法解码</param>
        /// <param name="pts">要匹配的正则表达式（如果提供超过一个的表达式，则需要全部匹配，即多个表达式之间是“与”的关系）</param>
        /// <returns>找到匹配的参数返回True，否则返回False</returns>
        public static bool TryGetRequestStringNotEmpty(HttpContext context, string[] keys, ref string value, bool htmlDecode, params string[] pts)
        {
            foreach (string key in keys)
                if (TryGetRequestStringNotEmpty(context, key, ref value, htmlDecode, pts))
                {
                    return true;
                }
            return false;
        }
        #endregion
        #endregion
        #region  String
        #region 返回String值
        /// <summary>
        /// 从Request.QueryString参数中检索符合要求的参数，如果没有符合的参数则返回指定的默认值
        /// </summary>
        /// <param name="key">要查找的参数名称</param>
        /// <param name="defaultValue">默认值，如果没有匹配的值则返回默认值</param>
        /// <returns>返回匹配的参数或默认值</returns>
        public static string TryGetQueryString(string key, string defaultValue)
        {
            string value = defaultValue;
            TryGetQueryString(key, ref value);
            return value;
        }
        /// <summary>
        /// 从Request.QueryString参数中检索符合要求的参数，如果没有符合的参数则返回指定的默认值
        /// </summary>
        /// <param name="key">要查找的参数名称</param>
        /// <param name="defaultValue">默认值，如果没有匹配的值则返回默认值</param>
        /// <param name="htmlDecode">是否需要使用HtmlDecode方法解码</param>
        /// <returns>返回匹配的参数或默认值</returns>
        public static string TryGetQueryString(string key, string defaultValue, bool htmlDecode)
        {
            string value = defaultValue;
            TryGetQueryString(key, ref value, htmlDecode);
            return value;
        }
        /// <summary>
        /// 从Request.Form参数中检索符合要求的参数，如果没有符合的参数则返回指定的默认值
        /// </summary>
        /// <param name="key">要查找的参数名称</param>
        /// <param name="defaultValue">默认值，如果没有匹配的值则返回默认值</param>
        /// <returns>返回匹配的参数或默认值</returns>
        public static string TryGetFormString(string key, string defaultValue)
        {
            string value = defaultValue;
            TryGetFormString(key, ref value);
            return value;
        }
        /// <summary>
        /// 从Request.Form参数中检索符合要求的参数，如果没有符合的参数则返回指定的默认值
        /// </summary>
        /// <param name="key">要查找的参数名称</param>
        /// <param name="defaultValue">默认值，如果没有匹配的值则返回默认值</param>
        /// <param name="htmlDecode">是否需要使用HtmlDecode方法解码</param>
        /// <returns>返回匹配的参数或默认值</returns>
        public static string TryGetFormString(string key, string defaultValue, bool htmlDecode)
        {
            string value = defaultValue;
            TryGetFormString(key, ref value, htmlDecode);
            return value;
        }
        /// <summary>
        /// 从Request参数中检索符合要求的参数，如果没有符合的参数则返回指定的默认值
        /// </summary>
        /// <param name="keys">要查找的参数名称，提过超过一个参数名则返回第一个匹配的参数</param>
        /// <param name="defaultValue">默认值，如果没有匹配的值则返回默认值</param>
        /// <param name="pts">要匹配的正则表达式（如果提供超过一个的表达式，则需要全部匹配，即多个表达式之间是“与”的关系）</param>
        /// <returns>返回匹配的参数或默认值</returns>
        public static string TryGetRequestString(string[] keys, string defaultValue, params string[] pts)
        {
            string value = defaultValue;
            TryGetRequestString(keys, ref value, pts);
            return value;
        }
        /// <summary>
        /// 从Request参数中检索符合要求的参数，如果没有符合的参数则返回指定的默认值
        /// </summary>
        /// <param name="key">要查找的参数名称</param>
        /// <param name="defaultValue">默认值，如果没有匹配的值则返回默认值</param>
        /// <param name="pts">要匹配的正则表达式（如果提供超过一个的表达式，则需要全部匹配，即多个表达式之间是“与”的关系）</param>
        /// <returns>返回匹配的参数或默认值</returns>
        public static string TryGetRequestString(string key, string defaultValue, params string[] pts)
        {
            string value = defaultValue;
            TryGetRequestString(key, ref value, pts);
            return value;
        }
        /// <summary>
        /// 从Request参数中检索符合要求的参数，如果没有符合的参数则返回指定的默认值
        /// </summary>
        /// <param name="key">要查找的参数名称</param>
        /// <param name="defaultValue">默认值，如果没有匹配的值则返回默认值</param>
        /// <param name="htmlDecode">是否需要使用HtmlDecode方法解码</param>
        /// <param name="pts">要匹配的正则表达式（如果提供超过一个的表达式，则需要全部匹配，即多个表达式之间是“与”的关系）</param>
        /// <returns>返回匹配的参数或默认值</returns>
        public static string TryGetRequestString(string key, string defaultValue, bool htmlDecode, params string[] pts)
        {
            string value = defaultValue;
            TryGetRequestString(key, ref value, htmlDecode, pts);
            return value;
        }
        /// <summary>
        /// 从Request参数中检索符合要求的参数，如果没有符合的参数则返回指定的默认值
        /// </summary>
        /// <param name="keys">要查找的参数名称，提过超过一个参数名则返回第一个匹配的参数</param>
        /// <param name="defaultValue">默认值，如果没有匹配的值则返回默认值</param>
        /// <param name="htmlDecode">是否需要使用HtmlDecode方法解码</param>
        /// <param name="pts">要匹配的正则表达式（如果提供超过一个的表达式，则需要全部匹配，即多个表达式之间是“与”的关系）</param>
        /// <returns>返回匹配的参数或默认值</returns>
        public static string TryGetRequestString(string[] keys, string defaultValue, bool htmlDecode, params string[] pts)
        {
            string value = defaultValue;
            TryGetRequestString(keys, ref value, htmlDecode, pts);
            return value;
        }
        #endregion
        #region String 返回Bool 当前 HttpContext
        /// <summary>
        /// 从Request.QueryString参数中检索符合要求的参数并保存到引用参数中，并返回True或False
        /// </summary>
        /// <param name="key">要查找的参数名称</param>
        /// <param name="value">引用参数，保存匹配的值</param>
        /// <returns>找到匹配的参数返回True，否则返回False</returns>
        public static bool TryGetQueryString(string key, ref string value)
        {
            return TryGetQueryString(HttpContext.Current, key, ref value);
        }
        /// <summary>
        /// 从Request.QueryString参数中检索符合要求的参数并保存到引用参数中，并返回True或False
        /// </summary>
        /// <param name="key">要查找的参数名称</param>
        /// <param name="value">引用参数，保存匹配的值</param>
        /// <param name="htmlDecode">是否需要使用HtmlDecode方法解码</param>
        /// <returns>找到匹配的参数返回True，否则返回False</returns>
        public static bool TryGetQueryString(string key, ref string value, bool htmlDecode)
        {
            return TryGetQueryString(HttpContext.Current, key, ref value, htmlDecode);
        }
        /// <summary>
        /// 从Request.Form参数中检索符合要求的参数并保存到引用参数中，并返回True或False
        /// </summary>
        /// <param name="key">要查找的参数名称</param>
        /// <param name="value">引用参数，保存匹配的值</param>
        /// <returns>找到匹配的参数返回True，否则返回False</returns>
        public static bool TryGetFormString(string key, ref string value)
        {
            return TryGetFormString(HttpContext.Current, key, ref value);
        }
        /// <summary>
        /// 从Request.Form参数中检索符合要求的参数并保存到引用参数中，并返回True或False
        /// </summary>
        /// <param name="key">要查找的参数名称</param>
        /// <param name="value">引用参数，保存匹配的值</param>
        /// <param name="htmlDecode">是否需要使用HtmlDecode方法解码</param>
        /// <returns>找到匹配的参数返回True，否则返回False</returns>
        public static bool TryGetFormString(string key, ref string value, bool htmlDecode)
        {
            return TryGetFormString(HttpContext.Current, key, ref value, htmlDecode);
        }
        /// <summary>
        /// 从Request参数中检索符合要求的参数并保存到引用参数中，并返回True或False
        /// </summary>
        /// <param name="keys">要查找的参数名称，提过超过一个参数名则返回第一个匹配的参数</param>
        /// <param name="value">引用参数，保存匹配的值</param>
        /// <param name="pts">要匹配的正则表达式（如果提供超过一个的表达式，则需要全部匹配，即多个表达式之间是“与”的关系）</param>
        /// <returns>找到匹配的参数返回True，否则返回False</returns>
        public static bool TryGetRequestString(string[] keys, ref string value, params string[] pts)
        {
            return TryGetRequestString(HttpContext.Current, keys, ref value, pts);
        }
        /// <summary>
        /// 从Request参数中检索符合要求的参数并保存到引用参数中，并返回True或False
        /// </summary>
        /// <param name="key">要查找的参数名称</param>
        /// <param name="value">引用参数，保存匹配的值</param>
        /// <param name="pts">要匹配的正则表达式（如果提供超过一个的表达式，则需要全部匹配，即多个表达式之间是“与”的关系）</param>
        /// <returns>找到匹配的参数返回True，否则返回False</returns>
        public static bool TryGetRequestString(string key, ref string value, params string[] pts)
        {
            return TryGetRequestString(HttpContext.Current, key, ref value, pts);
        }
        /// <summary>
        /// 从Request参数中检索符合要求的参数并保存到引用参数中，并返回True或False
        /// </summary>
        /// <param name="key">要查找的参数名称</param>
        /// <param name="value">引用参数，保存匹配的值</param>
        /// <param name="htmlDecode">是否需要使用HtmlDecode方法解码</param>
        /// <param name="pts">要匹配的正则表达式（如果提供超过一个的表达式，则需要全部匹配，即多个表达式之间是“与”的关系）</param>
        /// <returns>找到匹配的参数返回True，否则返回False</returns>
        public static bool TryGetRequestString(string key, ref string value, bool htmlDecode, params string[] pts)
        {
            return TryGetRequestString(HttpContext.Current, key, ref value, htmlDecode, pts);
        }
        /// <summary>
        /// 从Request参数中检索符合要求的参数并保存到引用参数中，并返回True或False
        /// </summary>
        /// <param name="keys">要查找的参数名称，提过超过一个参数名则返回第一个匹配的参数</param>
        /// <param name="value">引用参数，保存匹配的值</param>
        /// <param name="htmlDecode">是否需要使用HtmlDecode方法解码</param>
        /// <param name="pts">要匹配的正则表达式（如果提供超过一个的表达式，则需要全部匹配，即多个表达式之间是“与”的关系）</param>
        /// <returns>找到匹配的参数返回True，否则返回False</returns>
        public static bool TryGetRequestString(string[] keys, ref string value, bool htmlDecode, params string[] pts)
        {
            return TryGetRequestString(HttpContext.Current, keys, ref value, htmlDecode, pts);
        }
        #endregion
        #region String 返回Bool 指定 HttpContext
        /// <summary>
        /// 从Request.QueryString参数中检索符合要求的参数并保存到引用参数中，并返回True或False
        /// </summary>
        /// <param name="context">指定HttpContext上下文</param>
        /// <param name="key">要查找的参数名称</param>
        /// <param name="value">引用参数，保存匹配的值</param>
        /// <returns>找到匹配的参数返回True，否则返回False</returns>
        public static bool TryGetQueryString(HttpContext context, string key, ref string value)
        {
            return TryGetQueryString(context, key, ref value, true);
        }
        /// <summary>
        /// 从Request.QueryString参数中检索符合要求的参数并保存到引用参数中，并返回True或False
        /// </summary>
        /// <param name="context">指定HttpContext上下文</param>
        /// <param name="key">要查找的参数名称</param>
        /// <param name="value">引用参数，保存匹配的值</param>
        /// <param name="htmlDecode">是否需要使用HtmlDecode方法解码</param>
        /// <returns>找到匹配的参数返回True，否则返回False</returns>
        public static bool TryGetQueryString(HttpContext context, string key, ref string value, bool htmlDecode)
        {
            return TryGet(context.Request.QueryString, key, ref value, true, htmlDecode);
        }
        /// <summary>
        /// 从Request.Form参数中检索符合要求的参数并保存到引用参数中，并返回True或False
        /// </summary>
        /// <param name="context">指定HttpContext上下文</param>
        /// <param name="key">要查找的参数名称</param>
        /// <param name="value">引用参数，保存匹配的值</param>
        /// <returns>找到匹配的参数返回True，否则返回False</returns>
        public static bool TryGetFormString(HttpContext context, string key, ref string value)
        {
            return TryGetFormString(context, key, ref value, true);
        }
        /// <summary>
        /// 从Request.Form参数中检索符合要求的参数并保存到引用参数中，并返回True或False
        /// </summary>
        /// <param name="context">指定HttpContext上下文</param>
        /// <param name="key">要查找的参数名称</param>
        /// <param name="value">引用参数，保存匹配的值</param>
        /// <param name="htmlDecode">是否需要使用HtmlDecode方法解码</param>
        /// <returns>找到匹配的参数返回True，否则返回False</returns>
        public static bool TryGetFormString(HttpContext context, string key, ref string value, bool htmlDecode)
        {
            return TryGet(context.Request.Form, key, ref value, true, htmlDecode);
        }
        /// <summary>
        /// 从Request参数中检索符合要求的参数并保存到引用参数中，并返回True或False
        /// </summary>
        /// <param name="context">指定HttpContext上下文</param>
        /// <param name="keys">要查找的参数名称，提过超过一个参数名则返回第一个匹配的参数</param>
        /// <param name="value">引用参数，保存匹配的值</param>
        /// <param name="pts">要匹配的正则表达式（如果提供超过一个的表达式，则需要全部匹配，即多个表达式之间是“与”的关系）</param>
        /// <returns>找到匹配的参数返回True，否则返回False</returns>
        public static bool TryGetRequestString(HttpContext context, string[] keys, ref string value, params string[] pts)
        {
            return TryGetRequestString(context, keys, ref value, true, pts);
        }
        /// <summary>
        /// 从Request参数中检索符合要求的参数并保存到引用参数中，并返回True或False
        /// </summary>
        /// <param name="context">指定HttpContext上下文</param>
        /// <param name="key">要查找的参数名称</param>
        /// <param name="value">引用参数，保存匹配的值</param>
        /// <param name="pts">要匹配的正则表达式（如果提供超过一个的表达式，则需要全部匹配，即多个表达式之间是“与”的关系）</param>
        /// <returns>找到匹配的参数返回True，否则返回False</returns>
        public static bool TryGetRequestString(HttpContext context, string key, ref string value, params string[] pts)
        {
            return TryGetRequestString(context, key, ref value, true, pts);
        }
        /// <summary>
        /// 从Request参数中检索符合要求的参数并保存到引用参数中，并返回True或False
        /// </summary>
        /// <param name="context">指定HttpContext上下文</param>
        /// <param name="key">要查找的参数名称</param>
        /// <param name="value">引用参数，保存匹配的值</param>
        /// <param name="htmlDecode">是否需要使用HtmlDecode方法解码</param>
        /// <param name="pts">要匹配的正则表达式（如果提供超过一个的表达式，则需要全部匹配，即多个表达式之间是“与”的关系）</param>
        /// <returns>找到匹配的参数返回True，否则返回False</returns>
        public static bool TryGetRequestString(HttpContext context, string key, ref string value, bool htmlDecode, params string[] pts)
        {
            return ((TryGetFormString(context, key, ref value, htmlDecode) || TryGetQueryString(context, key, ref value, htmlDecode)) && isMatchAll(value, pts));
        }
        /// <summary>
        /// 从Request参数中检索符合要求的参数并保存到引用参数中，并返回True或False
        /// </summary>
        /// <param name="context">指定HttpContext上下文</param>
        /// <param name="keys">要查找的参数名称，提过超过一个参数名则返回第一个匹配的参数</param>
        /// <param name="value">引用参数，保存匹配的值</param>
        /// <param name="htmlDecode">是否需要使用HtmlDecode方法解码</param>
        /// <param name="pts">要匹配的正则表达式（如果提供超过一个的表达式，则需要全部匹配，即多个表达式之间是“与”的关系）</param>
        /// <returns>找到匹配的参数返回True，否则返回False</returns>
        public static bool TryGetRequestString(HttpContext context, string[] keys, ref string value, bool htmlDecode, params string[] pts)
        {
            foreach (string key in keys) if (TryGetRequestString(context, key, ref value, htmlDecode, pts)) return true;
            return false;
        }
        #endregion
        #endregion
        #region  Int
        #region 返回Int? 当前HttpContext
        public static int? TryGetQueryInt(string key)
        {
            int value = 0;
            if (TryGetQueryInt(key, ref value)) return value;
            return null;
        }

        public static int? TryGetFormInt(string key)
        {
            int value = 0;
            if (TryGetFormInt(key, ref value)) return value;
            return null;
        }

        public static int? TryGetRequestInt(string key, params string[] keys)
        {
            int value = 0;
            if (TryGetRequestInt(key, ref value)) return value;
            return TryGetRequestInt(keys);
        }

        public static int? TryGetRequestInt(string[] keys)
        {
            int value = 0;
            if (TryGetRequestInt(keys, ref value)) return value;
            return null;
        }
        #endregion
        #region 返回Int 当前HttpContext
        public static int TryGetQueryInt(string key, int defaultValue)
        {
            int value = defaultValue;
            TryGetQueryInt(key, ref value);
            return value;
        }

        public static int TryGetFormInt(string key, int defaultValue)
        {
            int value = defaultValue;
            TryGetFormInt(key, ref value);
            return value;
        }

        public static int TryGetRequestInt(string key, int defaultValue)
        {
            int value = defaultValue;
            TryGetRequestInt(key, ref value);
            return value;
        }

        public static int TryGetRequestInt(string[] keys, int defaultValue)
        {
            int value = defaultValue;
            TryGetRequestInt(keys, ref value);
            return value;
        }
        public static int TryGetRequestInt(int defaultValue, params string[] keys)
        {
            int value = defaultValue;
            TryGetRequestInt(keys, ref value);
            return value;
        }
        #endregion
        #region 返回Bool 当前HttpContext
        public static bool TryGetQueryInt(string key, ref int value)
        {
            return TryGetQueryInt(HttpContext.Current, key, ref value);
        }

        public static bool TryGetFormInt(string key, ref int value)
        {
            return TryGetFormInt(HttpContext.Current, key, ref value);
        }

        public static bool TryGetRequestInt(string key, ref int value)
        {
            return TryGetRequestInt(HttpContext.Current, key, ref value);
        }

        public static bool TryGetRequestInt(string[] keys, ref int value)
        {
            return TryGetRequestInt(HttpContext.Current, keys, ref value);
        }
        public static bool TryGetRequestInt(ref int value, params string[] keys)
        {
            return TryGetRequestInt(HttpContext.Current, keys, ref value);
        }
        #endregion
        #region 返回Bool 指定HttpContext
        public static bool TryGetQueryInt(HttpContext context, string key, ref int value)
        {
            foreach (string v in TryGetQueryValues(context, key))
            {
                try
                {
                    value = int.Parse(v);
                    return true;
                }
                catch { }
            }
            return false;
        }

        public static bool TryGetFormInt(HttpContext context, string key, ref int value)
        {
            foreach (string v in TryGetFormValues(context, key))
            {
                try
                {
                    value = int.Parse(v);
                    return true;
                }
                catch { }
            }
            return false;
        }

        public static bool TryGetRequestInt(HttpContext context, string key, ref int value)
        {
            foreach (string v in TryGetValues(context, key))
            {
                try
                {
                    value = int.Parse(v);
                    return true;
                }
                catch { }
            }
            return false;
        }

        public static bool TryGetRequestInt(HttpContext context, string[] keys, ref int value)
        {
            foreach (string key in keys) if (TryGetRequestInt(context, key, ref value)) return true;
            return false;
        }
        public static bool TryGetRequestInt(HttpContext context, ref int value, params string[] keys)
        {
            return TryGetRequestInt(context, keys, ref value);
        }
        #endregion
        #endregion
        #region  Bool
        #region 返回Bool? 当前HttpContext
        public static bool? TryGetQueryBool(string key)
        {
            bool value = false;
            if (TryGetQueryBool(key, ref value)) return value;
            return null;
        }

        public static bool? TryGetFormBool(string key)
        {
            bool value = false;
            if (TryGetFormBool(key, ref value)) return value;
            return null;
        }

        public static bool? TryGetRequestBool(string key, params string[] keys)
        {
            bool value = false;
            if (TryGetRequestBool(key, ref value)) return value;
            return TryGetRequestBool(keys);
        }

        public static bool? TryGetRequestBool(string[] keys)
        {
            bool value = false;
            if (TryGetRequestBool(keys, ref value)) return value;
            return null;
        }
        #endregion
        #region 返回bool值 当前HttpContext
        public static bool TryGetQueryBool(string key, bool defaultValue)
        {
            bool value = defaultValue;
            TryGetQueryBool(key, ref value);
            return value;
        }

        public static bool TryGetFormBool(string key, bool defaultValue)
        {
            bool value = defaultValue;
            TryGetFormBool(key, ref value);
            return value;
        }

        public static bool TryGetRequestBool(string key, bool defaultValue)
        {
            bool value = defaultValue;
            TryGetRequestBool(key, ref value);
            return value;
        }

        public static bool TryGetRequestBool(string[] keys, bool defaultValue)
        {
            bool value = defaultValue;
            TryGetRequestBool(keys, ref value);
            return value;
        }
        #endregion
        #region 返回Bool 当前HttpContext
        /// <summary>
        /// 特别注意：返回值不是请求值，而是指是否有请求值，即使请求值为false返回也是true
        /// </summary>
        /// <param name="key">要查找的参数名称</param>
        /// <param name="value">引用参数，保存匹配的值</param>
        /// <returns>特别注意：返回值不是请求值，而是指是否有请求值，即使请求值为false返回也是true</returns>
        public static bool TryGetQueryBool(string key, ref bool value)
        {
            return TryGetQueryBool(HttpContext.Current, key, ref value);
        }
        /// <summary>
        /// 特别注意：返回值不是请求值，而是指是否有请求值，即使请求值为false返回也是true
        /// </summary>
        /// <param name="key">要查找的参数名称</param>
        /// <param name="value">引用参数，保存匹配的值</param>
        /// <returns>特别注意：返回值不是请求值，而是指是否有请求值，即使请求值为false返回也是true</returns>
        public static bool TryGetFormBool(string key, ref bool value)
        {
            return TryGetFormBool(HttpContext.Current, key, ref value);
        }
        /// <summary>
        /// 特别注意：返回值不是请求值，而是指是否有请求值，即使请求值为false返回也是true
        /// </summary>
        /// <param name="key">要查找的参数名称</param>
        /// <param name="value">引用参数，保存匹配的值</param>
        /// <returns>特别注意：返回值不是请求值，而是指是否有请求值，即使请求值为false返回也是true</returns>
        public static bool TryGetRequestBool(string key, ref bool value)
        {
            return TryGetRequestBool(HttpContext.Current, key, ref value);
        }
        /// <summary>
        /// 特别注意：返回值不是请求值，而是指是否有请求值，即使请求值为false返回也是true
        /// </summary>
        /// <param name="keys">要查找的参数名称，提过超过一个参数名则返回第一个匹配的参数</param>
        /// <param name="value">引用参数，保存匹配的值</param>
        /// <returns>特别注意：返回值不是请求值，而是指是否有请求值，即使请求值为false返回也是true</returns>
        public static bool TryGetRequestBool(string[] keys, ref bool value)
        {
            return TryGetRequestBool(HttpContext.Current, keys, ref value);
        }
        #endregion
        #region 返回Bool 指定HttpContext
        /// <summary>
        /// 特别注意：返回值不是请求值，而是指是否有请求值，即使请求值为false返回也是true
        /// </summary>
        /// <param name="key">要查找的参数名称</param>
        /// <param name="value">引用参数，保存匹配的值</param>
        /// <returns>特别注意：返回值不是请求值，而是指是否有请求值，即使请求值为false返回也是true</returns>
        public static bool TryGetQueryBool(HttpContext context, string key, ref bool value)
        {
            foreach (string v in TryGetQueryValues(context, key))
            {
                try
                {
                    value = bool.Parse(v);
                    return true;
                }
                catch { }
            }
            return false;
        }
        /// <summary>
        /// 特别注意：返回值不是请求值，而是指是否有请求值，即使请求值为false返回也是true
        /// </summary>
        /// <param name="key">要查找的参数名称</param>
        /// <param name="value">引用参数，保存匹配的值</param>
        /// <returns>特别注意：返回值不是请求值，而是指是否有请求值，即使请求值为false返回也是true</returns>
        public static bool TryGetFormBool(HttpContext context, string key, ref bool value)
        {
            foreach (string v in TryGetFormValues(context, key))
            {
                try
                {
                    value = bool.Parse(v);
                    return true;
                }
                catch { }
            }
            return false;
        }
        /// <summary>
        /// 特别注意：返回值不是请求值，而是指是否有请求值，即使请求值为false返回也是true
        /// </summary>
        /// <param name="key">要查找的参数名称</param>
        /// <param name="value">引用参数，保存匹配的值</param>
        /// <returns>特别注意：返回值不是请求值，而是指是否有请求值，即使请求值为false返回也是true</returns>
        public static bool TryGetRequestBool(HttpContext context, string key, ref bool value)
        {
            foreach (string v in TryGetValues(context, key))
            {
                try
                {
                    value = bool.Parse(v);
                    return true;
                }
                catch { }
            }
            return false;
        }
        /// <summary>
        /// 特别注意：返回值不是请求值，而是指是否有请求值，即使请求值为false返回也是true
        /// </summary>
        /// <param name="keys">要查找的参数名称，提过超过一个参数名则返回第一个匹配的参数</param>
        /// <param name="value">引用参数，保存匹配的值</param>
        /// <returns>特别注意：返回值不是请求值，而是指是否有请求值，即使请求值为false返回也是true</returns>
        public static bool TryGetRequestBool(HttpContext context, string[] keys, ref bool value)
        {
            foreach (string key in keys) if (TryGetRequestBool(context, key, ref value)) return true;
            return false;
        }
        #endregion
        #endregion
        #region  Double
        #region 返回double? 当前HttpContext
        public static double? TryGetQueryDouble(string key)
        {
            double value = 0;
            if (TryGetQueryDouble(key, ref value)) return value;
            return null;
        }

        public static double? TryGetFormDouble(string key)
        {
            double value = 0;
            if (TryGetFormDouble(key, ref value)) return value;
            return null;
        }

        public static double? TryGetRequestDouble(string key, params string[] keys)
        {
            double value = 0;
            if (TryGetRequestDouble(key, ref value)) return value;
            return TryGetRequestDouble(keys);
        }

        public static double? TryGetRequestDouble(string[] keys)
        {
            double value = 0;
            if (TryGetRequestDouble(keys, ref value)) return value;
            return null;
        }
        #endregion
        #region 返回double值 当前HttpContext
        public static double TryGetQueryDouble(string key, double defaultValue)
        {
            double value = defaultValue;
            TryGetQueryDouble(key, ref value);
            return value;
        }

        public static double TryGetFormDouble(string key, double defaultValue)
        {
            double value = defaultValue;
            TryGetFormDouble(key, ref value);
            return value;
        }

        public static double TryGetRequestDouble(string key, double defaultValue)
        {
            double value = defaultValue;
            TryGetRequestDouble(key, ref value);
            return value;
        }

        public static double TryGetRequestDouble(string[] keys, double defaultValue)
        {
            double value = defaultValue;
            TryGetRequestDouble(keys, ref value);
            return value;
        }
        #endregion
        #region 返回Bool 当前HttpContext
        public static bool TryGetQueryDouble(string key, ref double value)
        {
            return TryGetQueryDouble(HttpContext.Current, key, ref value);
        }
        public static bool TryGetFormDouble(string key, ref double value)
        {
            return TryGetFormDouble(HttpContext.Current, key, ref value);
        }
        public static bool TryGetRequestDouble(string key, ref double value)
        {
            return TryGetRequestDouble(HttpContext.Current, key, ref value);
        }
        public static bool TryGetRequestDouble(string[] keys, ref double value)
        {
            return TryGetRequestDouble(HttpContext.Current, keys, ref value);
        }
        #endregion
        #region 返回Bool 指定HttpContext

        public static bool TryGetQueryDouble(HttpContext context, string key, ref double value)
        {
            foreach (string v in TryGetQueryValues(context, key))
            {
                try
                {
                    value = double.Parse(v);
                    return true;
                }
                catch { }
            }
            return false;
        }
        public static bool TryGetFormDouble(HttpContext context, string key, ref double value)
        {
            foreach (string v in TryGetFormValues(context, key))
            {
                try
                {
                    value = double.Parse(v);
                    return true;
                }
                catch { }
            }
            return false;
        }
        public static bool TryGetRequestDouble(HttpContext context, string key, ref double value)
        {
            foreach (string v in TryGetFormValues(context, key))
            {
                try
                {
                    value = double.Parse(v);
                    return true;
                }
                catch { }
            }
            return false;
        }
        public static bool TryGetRequestDouble(HttpContext context, string[] keys, ref double value)
        {
            foreach (string key in keys) if (TryGetRequestDouble(context, key, ref value)) return true;
            return false;
        }
        #endregion
        #endregion
        #region  Decimal
        #region 返回decimal? 当前HttpContext
        public static decimal? TryGetQueryDecimal(string key)
        {
            decimal value = 0;
            if (TryGetQueryDecimal(key, ref value)) return value;
            return null;
        }

        public static decimal? TryGetFormDecimal(string key)
        {
            decimal value = 0;
            if (TryGetFormDecimal(key, ref value)) return value;
            return null;
        }

        public static decimal? TryGetRequestDecimal(string key, params string[] keys)
        {
            decimal value = 0;
            if (TryGetRequestDecimal(key, ref value)) return value;
            return TryGetRequestDecimal(keys);
        }

        public static decimal? TryGetRequestDecimal(string[] keys)
        {
            decimal value = 0;
            if (TryGetRequestDecimal(keys, ref value)) return value;
            return null;
        }
        #endregion
        #region 返回decimal值 当前HttpContext
        public static decimal TryGetQueryDecimal(string key, decimal defaultValue)
        {
            decimal value = defaultValue;
            TryGetQueryDecimal(key, ref value);
            return value;
        }

        public static decimal TryGetFormDecimal(string key, decimal defaultValue)
        {
            decimal value = defaultValue;
            TryGetFormDecimal(key, ref value);
            return value;
        }

        public static decimal TryGetRequestDecimal(string key, decimal defaultValue)
        {
            decimal value = defaultValue;
            TryGetRequestDecimal(key, ref value);
            return value;
        }

        public static decimal TryGetRequestDecimal(string[] keys, decimal defaultValue)
        {
            decimal value = defaultValue;
            TryGetRequestDecimal(keys, ref value);
            return value;
        }
        #endregion
        #region 返回Bool 当前HttpContext
        public static bool TryGetQueryDecimal(string key, ref decimal value)
        {
            return TryGetQueryDecimal(HttpContext.Current, key, ref value);
        }
        public static bool TryGetFormDecimal(string key, ref decimal value)
        {
            return TryGetFormDecimal(HttpContext.Current, key, ref value);
        }
        public static bool TryGetRequestDecimal(string key, ref decimal value)
        {
            return TryGetRequestDecimal(HttpContext.Current, key, ref value);
        }
        public static bool TryGetRequestDecimal(string[] keys, ref decimal value)
        {
            return TryGetRequestDecimal(HttpContext.Current, keys, ref value);
        }
        #endregion
        #region 返回Bool 指定HttpContext

        public static bool TryGetQueryDecimal(HttpContext context, string key, ref decimal value)
        {
            foreach (string v in TryGetQueryValues(context, key))
            {
                try
                {
                    value = decimal.Parse(v);
                    return true;
                }
                catch { }
            }
            return false;
        }
        public static bool TryGetFormDecimal(HttpContext context, string key, ref decimal value)
        {
            foreach (string v in TryGetFormValues(context, key))
            {
                try
                {
                    value = decimal.Parse(v);
                    return true;
                }
                catch { }
            }
            return false;
        }
        public static bool TryGetRequestDecimal(HttpContext context, string key, ref decimal value)
        {
            foreach (string v in TryGetFormValues(context, key))
            {
                try
                {
                    value = decimal.Parse(v);
                    return true;
                }
                catch { }
            }
            return false;
        }
        public static bool TryGetRequestDecimal(HttpContext context, string[] keys, ref decimal value)
        {
            foreach (string key in keys) if (TryGetRequestDecimal(context, key, ref value)) return true;
            return false;
        }
        #endregion
        #endregion
        #region DateTime
        #region 返回DateTime? 当前HttpContext
        public static DateTime? TryGetQueryDateTime(string key)
        {
            DateTime value = DateTime.Now;
            if (TryGetQueryDateTime(key, ref value)) return value;
            return null;
        }

        public static DateTime? TryGetFormDateTime(string key)
        {
            DateTime value = DateTime.Now;
            if (TryGetFormDateTime(key, ref value)) return value;
            return null;
        }

        public static DateTime? TryGetRequestDateTime(string key, params string[] keys)
        {
            DateTime value = DateTime.Now;
            if (TryGetRequestDateTime(key, ref value)) return value;
            return TryGetRequestDateTime(keys);
        }

        public static DateTime? TryGetRequestDateTime(string[] keys)
        {
            DateTime value = DateTime.Now;
            if (TryGetRequestDateTime(keys, ref value)) return value;
            return null;
        }
        #endregion
        #region 返回DateTime值 当前HttpContext
        public static DateTime TryGetQueryDateTime(string key, DateTime defaultValue)
        {
            DateTime value = defaultValue;
            TryGetQueryDateTime(key, ref value);
            return value;
        }

        public static DateTime TryGetFormDateTime(string key, DateTime defaultValue)
        {
            DateTime value = defaultValue;
            TryGetFormDateTime(key, ref value);
            return value;
        }

        public static DateTime TryGetRequestDateTime(string key, DateTime defaultValue)
        {
            DateTime value = defaultValue;
            TryGetRequestDateTime(key, ref value);
            return value;
        }

        public static DateTime TryGetRequestDateTime(string[] keys, DateTime defaultValue)
        {
            DateTime value = defaultValue;
            TryGetRequestDateTime(keys, ref value);
            return value;
        }
        public static DateTime TryGetRequestDateTime(DateTime defaultValue, params string[] keys)
        {
            DateTime value = defaultValue;
            TryGetRequestDateTime(keys, ref value);
            return value;
        }
        #endregion
        #region 返回Bool 当前HttpContext
        public static bool TryGetQueryDateTime(string key, ref DateTime value)
        {
            return TryGetQueryDateTime(HttpContext.Current, key, ref value);
        }
        public static bool TryGetFormDateTime(string key, ref DateTime value)
        {
            return TryGetFormDateTime(HttpContext.Current, key, ref value);
        }
        public static bool TryGetRequestDateTime(string key, ref DateTime value)
        {
            return TryGetRequestDateTime(HttpContext.Current, key, ref value);
        }
        public static bool TryGetRequestDateTime(string[] keys, ref DateTime value)
        {
            return TryGetRequestDateTime(HttpContext.Current, keys, ref value);
        }
        public static bool TryGetRequestDateTime(ref DateTime value, params string[] keys)
        {
            return TryGetRequestDateTime(HttpContext.Current, ref value, keys);
        }
        #endregion
        #region 返回Bool 指定HttpContext

        public static bool TryGetQueryDateTime(HttpContext context, string key, ref DateTime value)
        {
            foreach (string v in TryGetQueryValues(context, key))
            {
                try
                {
                    value = DateTime.Parse(v);
                    return true;
                }
                catch { }
            }
            return false;
        }
        public static bool TryGetFormDateTime(HttpContext context, string key, ref DateTime value)
        {
            foreach (string v in TryGetFormValues(context, key))
            {
                try
                {
                    value = DateTime.Parse(v);
                    return true;
                }
                catch { }
            }
            return false;
        }
        public static bool TryGetRequestDateTime(HttpContext context, string key, ref DateTime value)
        {
            foreach (string v in TryGetValues(context, key))
            {
                try
                {
                    value = DateTime.Parse(v);
                    return true;
                }
                catch { }
            }
            return false;
        }
        public static bool TryGetRequestDateTime(HttpContext context, string[] keys, ref DateTime value)
        {
            foreach (string key in keys) if (TryGetRequestDateTime(context, key, ref value)) return true;
            return false;
        }

        public static bool TryGetRequestDateTime(HttpContext context, ref DateTime value, params string[] keys)
        {

            if (keys.Length == 0) return false;
            foreach (string key in keys)
            {
                if (TryGetRequestDateTime(context, key, ref value)) return true;
            }
            return false;
        }

        #endregion
        #endregion

        #region 客户端IP相关
        /// <summary>
        /// 获取客户端IP与代理IP
        /// </summary>
        /// <param name="listProxy">代理IP列表</param>
        /// <returns>客户端IP</returns>
        public static string GetClientIP()
        {
            string listProxy = "";
            return GetClientIP(out listProxy);
        }
        /// <summary>
        /// 获取客户端IP与代理IP
        /// </summary>
        /// <returns>客户端IP</returns>
        public static string GetClientIP(HttpContext context)
        {
            string listProxy = "";
            return GetClientIP(context, out listProxy);
        }
        /// <summary>
        /// 获取客户端IP与代理IP
        /// </summary>
        /// <param name="listProxy">代理IP列表</param>
        /// <returns>客户端IP</returns>
        public static string GetClientIP(out string listProxy)
        {
            return GetClientIP(HttpContext.Current, out listProxy);
        }
        public static string GetClientIP(HttpContext context, out string listProxy)
        {
            string hostIP = context.Request.UserHostAddress;   //代理IP地址（如果不通过代理则为客户IP地址）
            string proxyAddr = context.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];  //包含代理的IP地址列表
            string remoteAddr = context.Request.ServerVariables["REMOTE_ADDR"];  //客户IP地址

            listProxy = "";
            if (proxyAddr != null && proxyAddr != String.Empty)
            {
                listProxy = proxyAddr + "," + ((remoteAddr != null) ? remoteAddr : "") + "," + hostIP;
            }
            string result = proxyAddr;
            if (result == null || result == String.Empty)
            {
                result = remoteAddr;
            }
            if (result == null || result == String.Empty)
            {
                result = hostIP;
            }
            return result;
        }
        #endregion




        /// <summary>
        /// 获取客户端提交的所有数据信息
        /// </summary>
        /// <returns></returns>
        public static string GetAllRequestData(HttpContext context)
        {
            string vv = "";
            foreach (string s in context.Request.Form.Keys) vv += ";  " + s + "=" + context.Request.Form[s].ToString();
            foreach (string s in context.Request.QueryString.Keys) vv += ";  " + s + "=" + context.Request.QueryString[s].ToString();
            return vv;
        }


        static bool isMatchAll(string value, params string[] pts)
        {
            foreach (string s in pts) if (!Regex.IsMatch(value, s)) return false;
            return true;
        }


    }
}
