﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Unit
{
    public static class CompareDataType
    {

        public static bool JudgeDataType(string fileExit)
        {
            fileExit = fileExit.ToLower();
            if (fileExit == ".jpg" || fileExit == ".jpg" || fileExit == ".tiff" || fileExit == ".raw" || fileExit == ".bmp" || fileExit == ".gif" || fileExit == ".png" || fileExit == ".jpeg")
                return true;
            else
                return false;

        }

        public static string ConvertFileSize(int fileSize)
        {
            var kb = fileSize / 1024.0;
            if (kb >= 1024)
                return Math.Round((kb / 1024.0), 2) + "MB";
            return Math.Round(kb, 2) + "KB";
        }
    }
}
