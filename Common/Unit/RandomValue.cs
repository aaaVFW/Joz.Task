﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Unit
{
    public class RandomValue
    {
        private static string vCharChn = "的一是了我不人在他有这个上们来到时大地为子中你说生国年着就那和要她出也得里后自以会家可下而过天去能对小多然于心学么之都好看起发当没成只如事把还用第样道想作种开见明问力理尔点文几定本公特做外孩相西果走将月十实向声车全信重三机工物气每并别真打太新比才便夫再书部水像眼等体却加电主界门利海受听表德少克代员许稜先口由死安写性马光白或住难望教命花结乐色更拉东神记处让母父应直字场平报友关放至张认接告入笑内英军候民岁往何度山觉路带万男边风解叫任金快原吃妈变通师立象数四失满战远格士音轻目条呢病始达深完今提求清王化空业思切怎非找片罗钱紶吗语元喜曾离飞科言干流欢约各即指合反题必该论交终林请医晚制球决窢传画保读运及则房早院量苦火布品近坐产答星精视五连司巴奇管类未朋且婚台夜青北队久乎越观落尽形影红爸百令周吧识步希亚术留市半热送兴造谈容极随演收首根讲整式取照办强石古华諣拿计您装似足双妻尼转诉米称丽客南领节衣站黑刻统断福城故历惊脸选包紧争另建维绝树系伤示愿持千史谁准联妇纪基买志静阿诗独复痛消社算义竟确酒需单治卡幸兰念举仅钟怕共毛句息功官待究跟穿室易游程号居考突皮哪费倒价图具刚脑永歌响商礼细专黄块脚味灵改据般破引食仍存众注笔甚某沉血备习校默务土微娘须试怀料调广蜖苏显赛查密议底列富梦错座参八除跑亮假印设线温虽掉京初养香停际致阳纸李纳验助激够严证帝饭忘趣支春集丈木研班普导顿睡展跳获艺六波察群皇段急庭创区奥器谢弟店否害草排背止组州朝封睛板角况曲馆育忙质河续哥呼若推境遇雨标姐充围案伦护冷警贝著雪索剧啊船险烟依斗值帮汉慢佛肯闻唱沙局伯族低玩资屋击速顾泪洲团圣旁堂兵七露园牛哭旅街劳型烈姑陈莫鱼异抱宝权鲁简态级票怪寻杀律胜份汽右洋范床舞秘午登楼贵吸责例追较职属渐左录丝牙党继托赶章智冲叶胡吉卖坚喝肉遗救修松临藏担戏善卫药悲敢靠伊村戴词森耳差短祖云规窗散迷油旧适乡架恩投弹铁博雷府压超负勒杂醒洗采毫嘴毕九冰既状乱景席珍童顶派素脱农疑练野按犯拍征坏骨余承置臓彩灯巨琴免环姆暗换技翻束增忍餐洛塞缺忆判欧层付阵玛批岛项狗休懂武革良恶恋委拥娜妙探呀营退摇弄桌熟诺宣银势奖宫忽套康供优课鸟喊降夏困刘罪亡鞋健模败伴守挥鲜财孤枪禁恐伙杰迹妹藸遍盖副坦牌江顺秋萨菜划授归浪听凡预奶雄升碃编典袋莱含盛济蒙棋端腿招释介烧误";
        private static string vCharEn = "ABCDEFGHJKMNPQRSTUVWXYZabcdefghjkmnpqrstuvwxyz";
        private static string vCharEnAndNum = "ABCDEFGHJKMNPQRSTUVWXYZabcdefghjkmnpqrstuvwxyz23456789";
        private static string vCharNum = "0123456789";

        private static char[] vCharArray = vCharChn.ToCharArray();
        private static char[] vCharArrayNum = vCharNum.ToCharArray();
        private static char[] vCharArrayEn = vCharEn.ToCharArray();
        private static char[] vCharArrayEnNum = vCharEnAndNum.ToCharArray();

        /// <summary>
        /// 从字符数组中随机选择字符拼接成指定长度的字符串
        /// </summary>
        /// <param name="chars"></param>
        /// <param name="minLen">最小长度</param>
        /// <param name="maxLen">最大长度</param>
        /// <returns></returns>
        public static string RandomStr(char[] chars, int minLen, int maxLen)
        {
            int len = 0;
            if (minLen == maxLen) len = minLen;
            else len = Random.Next(minLen, maxLen);
            string v = "";
            for (int i = 0; i <= len - 1; i++)
            {
                v += chars[Random.Next(chars.Length)];
            }
            return v;
        }
        static Random rand = new Random();
        public static Random Random
        {
            get
            {
                return rand;
            }
        }
        public static int Max(params int[] parms)
        {
            if (parms.Length == 0) throw new Exception();
            int k = parms[0];
            for (int i = 0; i < parms.Length; i++) if (parms[i] > k) k = parms[i];
            return k;
        }
        public static int Min(params int[] parms)
        {
            if (parms.Length == 0) throw new Exception();
            int k = parms[0];
            for (int i = 0; i < parms.Length; i++) if (parms[i] < k) k = parms[i];
            return k;
        }
        public static string Random_English_Text(int len)
        {
            return Random_English_Text(len, len);
        }
        public static string Random_English_Text(int minLen, int maxLen)
        {
            return RandomStr(vCharArrayEn, minLen, maxLen);
        }

        public static string Random_Chinese_Text(int len)
        {
            return Random_Chinese_Text(len, len);
        }
        public static string Random_Chinese_Text(int minLen, int maxLen)
        {
            return RandomStr(vCharArray, minLen, maxLen);
        }
        public static string Account
        {
            get { return Random_English_Text(8, 16); }
        }
        public static string Random_Account()
        {
            return Account;
        }
        static char[] firstName = "赵钱孙李周吴郑王冯陈褚卫蒋沈韩杨朱秦尤许何吕施张孔曹严华金魏陶姜戚谢邹喻柏水窦章云苏潘葛奚范彭郎鲁韦昌马苗凤花方俞任袁柳酆鲍史唐费廉岑薛雷贺倪汤滕殷罗毕郝邬安常乐于时傅皮卞齐康伍余元卜顾孟平黄和穆萧尹姚邵湛汪祁毛禹狄米贝明臧计伏成戴谈宋茅庞熊纪舒屈项祝董梁杜阮蓝闵席季麻强贾路娄危江童颜郭梅盛林刁锺徐邱骆高夏蔡田樊胡凌霍虞万支柯昝管卢".ToCharArray();
        static char[] secondName = "嘉哲俊博妍乐佳涵晨宇怡泽子凡悦思奕依浩泓彤冰媛凯伊淇淳一洁茹清吉源渊和函妤宜云琪菱宣沂健信欣可洋萍荣榕含佑明雄梅芝英义淑卿乾亦芬萱昊芸天岚昕尧鸿棋琳孜娟宸林乔琦丞安毅凌泉坤晴竹娴婕恒渝菁龄弘佩勋宁元栋盈江卓春晋逸沅倩昱绮海圣承民智棠容羚峰钰涓新莉恩羽妮旭维家泰诗谚阳彬书苓汉蔚坚茵耘喆国仑良裕融致富德易虹纲筠奇平蓓真之凰桦玫强村沛汶锋彦延庭霞冠益劭钧薇亭瀚桓东滢恬瑾达群茜先洲溢楠基轩月美心茗丹森学文".ToCharArray();
        /// <summary>
        /// 随机的中文姓名
        /// </summary>
        public static string ChineseName
        {
            get
            {
                return Random_ChineseName();
            }
        }
        /// <summary>
        /// 返回一个随机的中文姓名
        /// </summary>
        /// <returns></returns>
        public static string Random_ChineseName()
        {
            string v = firstName[Random.Next(firstName.Length)].ToString();
            int k = rand.Next(1, 100);
            if (k < 80) k = 2;
            else if (k < 95) k = 1;
            else k = 3;

            for (int i = 0; i < k; i++) v += secondName[rand.Next(0, secondName.Length)].ToString();
            return v;
        }

        /// <summary>
        /// 返回随机的手机号码
        /// </summary>
        public static string Mobile
        {
            get { return Random_Mobile(); }
        }
        /// <summary>
        /// 返回一个随机生成的手机号码
        /// </summary>
        /// <returns></returns>
        public static string Random_Mobile()
        {
            return RandomInArray("130,131,132,133,134,135,136,137,138,139,150,151,152,153,155,156,157,158,159,180,185,186,187,188,189".Split(',')).ToString() + RandomStr(vCharArrayNum, 8, 8);
        }
        /// <summary>
        /// 返回一个随机生成的电子邮箱
        /// </summary>
        public static string Email
        {
            get { return Random_English_Text(6, 16).ToLower() + "@" + Random_English_Text(5, 9).ToLower() + ".com"; }
        }
        /// <summary>
        /// 返回一个随机生成的电子邮箱
        /// </summary>
        /// <returns></returns>
        public static string Random_Email()
        {
            return Email;
        }

        public static string Sex
        {
            get { return Random.Next(10) > 5 ? "女" : "男"; }
        }
        public static string Random_Sex()
        {
            return Sex;
        }
        public static int Sex_Num
        {
            get { return Random.Next(10) > 5 ? 0 : 1; }
        }
        public static int Random_Sex_Num()
        {
            return Sex_Num;
        }
        /// <summary>
        /// 返回一个不带区号也不带分机号的电话号码
        /// </summary>
        /// <returns></returns>
        public static string Telephone
        {
            get { return RandomStr(vCharArrayNum, 6, 8); }
        }
        /// <summary>
        /// 返回一个不带区号也不带分机号的电话号码
        /// </summary>
        /// <returns></returns>
        public static string Telephone_B
        {
            get { return RandomStr(vCharArrayNum, 6, 8); }
        }
        /// <summary>
        /// 返回一个不带区号也不带分机号的电话号码
        /// </summary>
        /// <returns></returns>
        public static string Random_Telephone_B()
        {
            return Telephone_B;
        }
        /// <summary>
        /// 返回一个带区号的电话号码
        /// </summary>
        /// <returns></returns>
        public static string Telephone_A_B
        {
            get { return RandomStr(vCharArrayNum, 3, 3) + "-" + Telephone_B; }
        }
        /// <summary>
        /// 返回一个带区号的电话号码
        /// </summary>
        /// <returns></returns>
        public static string Random_Telephone_A_B()
        {
            return Telephone_A_B;
        }
        /// <summary>
        /// 返回一个带区号和分机号的电话号码
        /// </summary>
        /// <returns></returns>
        public static string Telephone_A_B_C
        {
            get { return Telephone_A_B + "-" + RandomStr(vCharArrayNum, 3, 3); }
        }
        /// <summary>
        /// 返回一个带区号和分机号的电话号码
        /// </summary>
        /// <returns></returns>
        public static string Random_Telephone_A_B_C()
        {
            return Telephone_A_B_C;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="addMaxHoursBefore">小于0则返回当前时间之前的时间，大于0则返回之后的时间</param>
        /// <returns></returns>
        public static DateTime Random_DateTime(int addMaxHoursBefore)
        {
            if (addMaxHoursBefore == 0) return DateTime.Now;
            return DateTime.Now.AddHours(Random.Next(addMaxHoursBefore > 0 ? 0 : addMaxHoursBefore, addMaxHoursBefore > 0 ? addMaxHoursBefore : 0));
        }
        public static DateTime Random_DateTime(DateTime min, DateTime max)
        {
            double hours = ((TimeSpan)(max - min)).TotalHours;
            return min.AddHours(hours * Random_Double());
        }

        public static int Int
        {
            get { return Random.Next(); }
        }
        public static int Random_Int()
        {
            return Random.Next();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="min">最小值（含）</param>
        /// <param name="max">最大值（不含）</param>
        /// <returns></returns>
        public static int Random_Int(int min, int max)
        {
            return Random.Next(min, max);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="min">最小值（含）</param>
        /// <param name="max">最大值（不含）</param>
        /// <param name="defaultValue"></param>
        /// <param name="defaultPercent"></param>
        /// <returns></returns>
        public static int Random_Int(int min, int max, int defaultValue, int defaultPercent)
        {
            if (Random.Next(100) < defaultPercent) return defaultValue;
            return Random.Next(min, max);
        }
        public static double Double
        {
            get { return Random.NextDouble(); }
        }
        public static double Random_Double()
        {
            return Random.NextDouble();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="min">最小值（含）</param>
        /// <param name="max">最大值（不含）</param>
        /// <returns></returns>
        public static double Random_Double(double min, double max)
        {
            return Random.NextDouble() * (max - min) + min;
        }

        /// <summary>
        /// 根据指定的随机分布，从数组或默认值中选择一个对象
        /// </summary>
        /// <param name="objs">数组中的备选对象</param>
        /// <param name="percent">从数组中选择的几率（0-100）</param>
        /// <param name="def">默认值</param>
        /// <returns></returns>
        public static object RandomInPercent(object[] objs, int percent, object def)
        {
            if (Random.Next(101) <= percent) return RandomInArray(objs);
            else return def;
        }
        /// <summary>
        /// 从两个对象中随机选择一个，可以指定遵循一定的概率分布
        /// </summary>
        /// <param name="obj">第一个对象</param>
        /// <param name="percent">选中第一个对象的几率（0-100）</param>
        /// <param name="otherObj">第二个对象</param>
        /// <returns></returns>
        public static object ChooseOne(object obj, int percent, object otherObj)
        {
            if (Random.Next(101) <= percent) return obj;
            else return otherObj;
        }
        /// <summary>
        /// 从数组中随机选择一个值
        /// </summary>
        /// <param name="objects"></param>
        /// <returns></returns>
        public static object RandomInArray(object[] objects)
        {
            return objects[Random.Next(objects.Length)];
        }
        /// <summary>
        /// 从数组中随机选择一个值
        /// </summary>
        /// <param name="chars"></param>
        /// <returns></returns>
        public static char RandomInArray(char[] chars)
        {
            return chars[Random.Next(chars.Length)];
        }
        /// <summary>
        /// 从数组中随机选择一个值
        /// </summary>
        /// <param name="objects"></param>
        /// <returns></returns>
        public static int RandomInArray(int[] objects)
        {
            return objects[Random.Next(objects.Length)];
        }
        /// <summary>
        /// 从数组中随机选择一个值
        /// </summary>
        /// <param name="objects"></param>
        /// <returns></returns>
        public static double RandomInArray(double[] objects)
        {
            return objects[Random.Next(objects.Length)];
        }
        /// <summary>
        /// 从数组中随机选择一个值
        /// </summary>
        /// <param name="objects"></param>
        /// <returns></returns>
        public static object RandomInArray(string[] objects)
        {
            return objects[Random.Next(objects.Length)];
        }
        /// <summary>
        /// 对数组随机排序
        /// </summary>
        /// <param name="nums"></param>
        /// <returns></returns>
        public static int[] Random_Sort(int[] nums)
        {
            for (int i = 0; i < nums.Length; i++)
            {
                int t = nums[i];
                int k = Random.Next(nums.Length);
                nums[i] = nums[k];
                nums[k] = t;
            }
            return nums;
        }
    }
}
