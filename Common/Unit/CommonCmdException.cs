﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Unit
{
    public class CommonCmdException:Exception
    {
        public CommonCmdException() { }

        public CommonCmdException(string message):base(message) { }
        
    }
}
