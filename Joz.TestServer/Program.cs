﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Topshelf;
namespace Joz.TestServer
{
    class Program
    {
        static void Main(string[] args)
        {
            HostFactory.Run(x =>
            {
                x.Service<TestQuartz>(s =>
                {
                    s.ConstructUsing(name => new TestQuartz());
                    s.WhenStarted(tc => tc.Start());
                    s.WhenStopped(tc => tc.Stop());
                });

                //服务使用NETWORK_SERVICE内置帐户运行。身份标识，有好几种方式，如：x.RunAs("username", "password");  x.RunAsPrompt(); x.RunAsNetworkService(); 等
                x.RunAsLocalSystem();

                x.SetDescription("TestServer Host");  //服务的描述
                x.SetDisplayName("TestServer");      //安装显示名称
                x.SetServiceName("TestServer");     //安装服务名称
            });
        }
    }
}
