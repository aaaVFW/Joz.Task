﻿using Common.Unit;
using Joz.TestServer.Quartz;
using Quartz;
using Quartz.Impl;
using Quartz.Impl.Triggers;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using Topshelf;

namespace Joz.TestServer
{
    /// <summary>
    /// Timer定时
    /// </summary>
    public class TaskManagerServiceBus
    {
        readonly Timer _timer;
        public TaskManagerServiceBus()
        {
            _timer = new Timer(1000) { AutoReset = true };
            _timer.Elapsed += (sender, eventArgs) => Console.WriteLine("It is {0} and all is well", DateTime.Now);
        }

        public void Start() { _timer.Start(); }
        public void Stop() { _timer.Stop(); }

    }

    /// <summary>
    /// QuartZ定时
    /// </summary>
    public class TestQuartz
    {
        public void Start()
        {
            QuartzHelper.InitScheduler();
            QuartzHelper.StartScheduler();
        }
        
        public void Stop()
        {
            QuartzHelper.StopSchedule();
            System.Environment.Exit(0);
        }
    }
}

