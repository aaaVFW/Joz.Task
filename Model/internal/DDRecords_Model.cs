﻿
    using System;
    namespace Joz.Model
    {
    /// <summary>
    /// 类DDRecords_Model
    /// </summary>
    [Serializable]
    public partial class DDRecords_Model
    {
    /// <summary>
    /// 
    /// </summary>
    private int? id;
    private string videoName;
    private string httpUrl;
    private int? soureid;
    private DateTime? addTime;
    private bool? isUpdate;
  public DDRecords_Model(){}
  /// <summary>
  /// 带参数的构造函数
  /// </summary>
    /// <param name="id"></param>
    /// <param name="videoName">视频名称</param>
    /// <param name="httpUrl">原路径</param>
    /// <param name="soureid">来源ID</param>
    /// <param name="addTime"></param>
    /// <param name="isUpdate">是否需要更新</param>
  public DDRecords_Model(int? id,string videoName,string httpUrl,int? soureid,DateTime? addTime,bool? isUpdate)
  {
    this.id=id;
    this.videoName=videoName;
    this.httpUrl=httpUrl;
    this.soureid=soureid;
    this.addTime=addTime;
    this.isUpdate=isUpdate;
  }
  /// <summary>
  /// 带参数的构造函数
  ///（只包含数据表对应的属性，对于扩展的属性，应该在此方法或CopySelf方法的基础上扩展一个方法）
  /// </summary>
    /// <param name="m">要浅复制的对象</param>
  public DDRecords_Model(DDRecords_Model m)
  {
    this.id=m.id;
    this.videoName=m.videoName;
    this.httpUrl=m.httpUrl;
    this.soureid=m.soureid;
    this.addTime=m.addTime;
    this.isUpdate=m.isUpdate;
  }
  /// <summary>
  /// 返回一个当前对象的一个拷贝（只包含数据表对应的属性，对于扩展的属性，应该在此方法的基础上扩展一个方法）
  /// </summary>
  public DDRecords_Model CopySelf()
  {
    return new DDRecords_Model(this);
  }
    /// <summary>
    /// 
    /// </summary>
    public int? Id
    {
        get { return this.id; }
        set { this.id=value; }
    }
    /// <summary>
    /// 视频名称
    /// </summary>
    public string VideoName
    {
        get { return this.videoName; }
        set { this.videoName=value; }
    }
    /// <summary>
    /// 原路径
    /// </summary>
    public string HttpUrl
    {
        get { return this.httpUrl; }
        set { this.httpUrl=value; }
    }
    /// <summary>
    /// 来源ID，，看枚举
    /// </summary>
    public int? Soureid
    {
        get { return this.soureid; }
        set { this.soureid=value; }
    }
    /// <summary>
    /// 
    /// </summary>
    public DateTime? AddTime
    {
        get { return this.addTime; }
        set { this.addTime=value; }
    }
    /// <summary>
    /// 是否需要更新
    /// </summary>
    public bool? IsUpdate
    {
        get { return this.isUpdate; }
        set { this.isUpdate=value; }
    }
    }
    }
  
