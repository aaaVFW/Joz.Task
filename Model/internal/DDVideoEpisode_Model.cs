﻿
    using System;
    namespace Joz.Model
    {
    /// <summary>
    /// 类DDVideoEpisode_Model
    /// </summary>
    [Serializable]
    public partial class DDVideoEpisode_Model
    {
    /// <summary>
    /// 
    /// </summary>
    private int? id;
    private int? infoId;
    private string episodeType;
    private string episodeCount;
    private string episodeUrl;
    private string episodeTranUrl;
  public DDVideoEpisode_Model(){}
  /// <summary>
  /// 带参数的构造函数
  /// </summary>
    /// <param name="id"></param>
    /// <param name="infoId"></param>
    /// <param name="episodeType">数据来源名称（需要分组</param>
    /// <param name="episodeCount">集数 1，2，或者 BD</param>
    /// <param name="episodeUrl">实际播放路径</param>
    /// <param name="episodeTranUrl">跳转播放路径</param>
  public DDVideoEpisode_Model(int? id,int? infoId,string episodeType,string episodeCount,string episodeUrl,string episodeTranUrl)
  {
    this.id=id;
    this.infoId=infoId;
    this.episodeType=episodeType;
    this.episodeCount=episodeCount;
    this.episodeUrl=episodeUrl;
    this.episodeTranUrl=episodeTranUrl;
  }
  /// <summary>
  /// 带参数的构造函数
  ///（只包含数据表对应的属性，对于扩展的属性，应该在此方法或CopySelf方法的基础上扩展一个方法）
  /// </summary>
    /// <param name="m">要浅复制的对象</param>
  public DDVideoEpisode_Model(DDVideoEpisode_Model m)
  {
    this.id=m.id;
    this.infoId=m.infoId;
    this.episodeType=m.episodeType;
    this.episodeCount=m.episodeCount;
    this.episodeUrl=m.episodeUrl;
    this.episodeTranUrl=m.episodeTranUrl;
  }
  /// <summary>
  /// 返回一个当前对象的一个拷贝（只包含数据表对应的属性，对于扩展的属性，应该在此方法的基础上扩展一个方法）
  /// </summary>
  public DDVideoEpisode_Model CopySelf()
  {
    return new DDVideoEpisode_Model(this);
  }
    /// <summary>
    /// 
    /// </summary>
    public int? Id
    {
        get { return this.id; }
        set { this.id=value; }
    }
    /// <summary>
    /// 
    /// </summary>
    public int? InfoId
    {
        get { return this.infoId; }
        set { this.infoId=value; }
    }
    /// <summary>
    /// 数据来源名称（需要分组，，依据名称）
    /// </summary>
    public string EpisodeType
    {
        get { return this.episodeType; }
        set { this.episodeType=value; }
    }
    /// <summary>
    /// 集数 1，2，或者 BD，,HD等
    /// </summary>
    public string EpisodeCount
    {
        get { return this.episodeCount; }
        set { this.episodeCount=value; }
    }
    /// <summary>
    /// 实际播放路径
    /// </summary>
    public string EpisodeUrl
    {
        get { return this.episodeUrl; }
        set { this.episodeUrl=value; }
    }
    /// <summary>
    /// 跳转播放路径
    /// </summary>
    public string EpisodeTranUrl
    {
        get { return this.episodeTranUrl; }
        set { this.episodeTranUrl=value; }
    }
    }
    }
  
