﻿
    using System;
    namespace Joz.Model
    {
    /// <summary>
    /// 类DDVideoInfos_Model
    /// </summary>
    [Serializable]
    public partial class DDVideoInfos_Model
    {
    /// <summary>
    /// 
    /// </summary>
    private int? id;
    private string videoName;
    private string images;
    private string httpImages;
    private string starNames;
    private decimal? scord;
    private string director;
    private int? movieType;
    private int? language;
    private DateTime? showDate;
    private DateTime? showYear;
    private string plots;
    private string dec;
    private int? areaId;
    private DateTime? addTime;
    private string originalUrl;
  public DDVideoInfos_Model(){}
  /// <summary>
  /// 带参数的构造函数
  /// </summary>
    /// <param name="id"></param>
    /// <param name="videoName">视频名称</param>
    /// <param name="images">本地文件路径</param>
    /// <param name="httpImages">原站点图片路径</param>
    /// <param name="starNames">演员名称组合</param>
    /// <param name="scord">豆瓣评分</param>
    /// <param name="director">导演名称</param>
    /// <param name="movieType">影视剧类型（看枚举类型）电影</param>
    /// <param name="language">语言类型（看枚举） 普通话</param>
    /// <param name="showDate">上映时间（具体时间去豆瓣抓取）</param>
    /// <param name="showYear">具体的年份</param>
    /// <param name="plots">剧情名称组合（建立枚举）神话</param>
    /// <param name="dec">剧情简介</param>
    /// <param name="areaId">区域ID</param>
    /// <param name="addTime"></param>
    /// <param name="originalUrl">原网址的访问路径</param>
  public DDVideoInfos_Model(int? id,string videoName,string images,string httpImages,string starNames,decimal? scord,string director,int? movieType,int? language,DateTime? showDate,DateTime? showYear,string plots,string dec,int? areaId,DateTime? addTime,string originalUrl)
  {
    this.id=id;
    this.videoName=videoName;
    this.images=images;
    this.httpImages=httpImages;
    this.starNames=starNames;
    this.scord=scord;
    this.director=director;
    this.movieType=movieType;
    this.language=language;
    this.showDate=showDate;
    this.showYear=showYear;
    this.plots=plots;
    this.dec=dec;
    this.areaId=areaId;
    this.addTime=addTime;
    this.originalUrl=originalUrl;
  }
  /// <summary>
  /// 带参数的构造函数
  ///（只包含数据表对应的属性，对于扩展的属性，应该在此方法或CopySelf方法的基础上扩展一个方法）
  /// </summary>
    /// <param name="m">要浅复制的对象</param>
  public DDVideoInfos_Model(DDVideoInfos_Model m)
  {
    this.id=m.id;
    this.videoName=m.videoName;
    this.images=m.images;
    this.httpImages=m.httpImages;
    this.starNames=m.starNames;
    this.scord=m.scord;
    this.director=m.director;
    this.movieType=m.movieType;
    this.language=m.language;
    this.showDate=m.showDate;
    this.showYear=m.showYear;
    this.plots=m.plots;
    this.dec=m.dec;
    this.areaId=m.areaId;
    this.addTime=m.addTime;
    this.originalUrl=m.originalUrl;
  }
  /// <summary>
  /// 返回一个当前对象的一个拷贝（只包含数据表对应的属性，对于扩展的属性，应该在此方法的基础上扩展一个方法）
  /// </summary>
  public DDVideoInfos_Model CopySelf()
  {
    return new DDVideoInfos_Model(this);
  }
    /// <summary>
    /// 
    /// </summary>
    public int? Id
    {
        get { return this.id; }
        set { this.id=value; }
    }
    /// <summary>
    /// 视频名称
    /// </summary>
    public string VideoName
    {
        get { return this.videoName; }
        set { this.videoName=value; }
    }
    /// <summary>
    /// 本地文件路径
    /// </summary>
    public string Images
    {
        get { return this.images; }
        set { this.images=value; }
    }
    /// <summary>
    /// 原站点图片路径
    /// </summary>
    public string HttpImages
    {
        get { return this.httpImages; }
        set { this.httpImages=value; }
    }
    /// <summary>
    /// 演员名称组合
    /// </summary>
    public string StarNames
    {
        get { return this.starNames; }
        set { this.starNames=value; }
    }
    /// <summary>
    /// 豆瓣评分
    /// </summary>
    public decimal? Scord
    {
        get { return this.scord; }
        set { this.scord=value; }
    }
    /// <summary>
    /// 导演名称
    /// </summary>
    public string Director
    {
        get { return this.director; }
        set { this.director=value; }
    }
    /// <summary>
    /// 影视剧类型（看枚举类型）电影，，电视剧，综艺
    /// </summary>
    public int? MovieType
    {
        get { return this.movieType; }
        set { this.movieType=value; }
    }
    /// <summary>
    /// 语言类型（看枚举） 普通话，，粤语，英语等
    /// </summary>
    public int? Language
    {
        get { return this.language; }
        set { this.language=value; }
    }
    /// <summary>
    /// 上映时间（具体时间去豆瓣抓取）
    /// </summary>
    public DateTime? ShowDate
    {
        get { return this.showDate; }
        set { this.showDate=value; }
    }
    /// <summary>
    /// 具体的年份，，用于搜索
    /// </summary>
    public DateTime? ShowYear
    {
        get { return this.showYear; }
        set { this.showYear=value; }
    }
    /// <summary>
    /// 剧情名称组合（建立枚举）神话，,剧情,历史等
    /// </summary>
    public string Plots
    {
        get { return this.plots; }
        set { this.plots=value; }
    }
    /// <summary>
    /// 剧情简介
    /// </summary>
    public string Dec
    {
        get { return this.dec; }
        set { this.dec=value; }
    }
    /// <summary>
    /// 区域ID，，看枚举
    /// </summary>
    public int? AreaId
    {
        get { return this.areaId; }
        set { this.areaId=value; }
    }
    /// <summary>
    /// 
    /// </summary>
    public DateTime? AddTime
    {
        get { return this.addTime; }
        set { this.addTime=value; }
    }
    /// <summary>
    /// 原网址的访问路径
    /// </summary>
    public string OriginalUrl
    {
        get { return this.originalUrl; }
        set { this.originalUrl=value; }
    }
    }
    }
  
