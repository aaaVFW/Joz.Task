﻿
    using System;
    namespace Joz.Model
    {
    /// <summary>
    /// 类DDStars_Model
    /// </summary>
    [Serializable]
    public partial class DDStars_Model
    {
    /// <summary>
    /// 
    /// </summary>
    private int? id;
    private string name;
  public DDStars_Model(){}
  /// <summary>
  /// 带参数的构造函数
  /// </summary>
    /// <param name="id"></param>
    /// <param name="name"></param>
  public DDStars_Model(int? id,string name)
  {
    this.id=id;
    this.name=name;
  }
  /// <summary>
  /// 带参数的构造函数
  ///（只包含数据表对应的属性，对于扩展的属性，应该在此方法或CopySelf方法的基础上扩展一个方法）
  /// </summary>
    /// <param name="m">要浅复制的对象</param>
  public DDStars_Model(DDStars_Model m)
  {
    this.id=m.id;
    this.name=m.name;
  }
  /// <summary>
  /// 返回一个当前对象的一个拷贝（只包含数据表对应的属性，对于扩展的属性，应该在此方法的基础上扩展一个方法）
  /// </summary>
  public DDStars_Model CopySelf()
  {
    return new DDStars_Model(this);
  }
    /// <summary>
    /// 
    /// </summary>
    public int? Id
    {
        get { return this.id; }
        set { this.id=value; }
    }
    /// <summary>
    /// 
    /// </summary>
    public string Name
    {
        get { return this.name; }
        set { this.name=value; }
    }
    }
    }
  
